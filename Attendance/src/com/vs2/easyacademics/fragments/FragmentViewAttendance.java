package com.vs2.easyacademics.fragments;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.HomeActivity;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.adapters.AttendanceListAdapter;
import com.vs2.easyacademics.adapters.FullReportListAdapter;
import com.vs2.easyacademics.adapters.ShowReportListAdapter;
import com.vs2.easyacademics.fragments.FragmentAdminClasses.DeleteClassApi;
import com.vs2.easyacademics.objects.AttendenceMaster;
import com.vs2.easyacademics.objects.FullReport;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.ShowReport;
import com.vs2.easyacademics.objects.ToastMsg;

public class FragmentViewAttendance extends SherlockFragment {

	private View mView;
	private ActionBar mActionbar;
	String classid,classname,subjectid, reportType, previousMonthDate, endMonth,
			startMonth,takeanDate;

	private Button mButtonDateStart, mButtonDateEnd;
	private TextView mTextViewTo,mTextViewTotalLecture;
	static final int DATE_DIALOG_ID_END = 999, DATE_DIALOG_ID_START = 888;
	private DatePickerDialog mDatePickerDialogEndDate,
			mDatePickerDialogStartDate;
	String enddate = null, startdate = null;
	private int yearEnd, monthEnd, dayEnd, yearStart, monthStart, dayStart,
			maxDay;
	private Calendar lastCalender;
	public static String daysStart[],daysEnd[],days[];
	private ScrollView mScrollView;

	public static String ListViewChecker;
	private ListView mListViewDailyList;
	private AttendenceMaster mAttendenceMasterList;
	public static ArrayList<AttendenceMaster> arry_attendenceMaster;
	
	private ArrayList<AttendenceMaster> mArrayList;
	private ArrayList<ArrayList<AttendenceMaster>> mArrayArrayList;
	private ArrayList<Integer> mArrayAId;
	private ArrayList<Integer> mArrayLectureNo;
	private LinearLayout mLinearLayoutRollNo, mLinearLayoutAttend;
	
	private Spinner mSpinnerLectureNo;
	String[] spinnerListLectureNo;
	private int[][] totalPresent,totalAbsent;
	private int counterPresent,counterAbsent;
	
	private int[][] numberOfTotalPresent;
	private int[] counterNumberOfTotalPresent;
	
	private String []strLeft;
    private String []strAdded;
    
	private AttendanceListAdapter mAdapter;
	TextView textAttend,textLectureNo;
	private int lectureNo;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_view_attendance, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		Bundle bundle = new Bundle();
		bundle = getArguments();
		subjectid = bundle.getString("Subject ID");
		classid = bundle.getString("Class ID");
		reportType = bundle.getString("Report");
		initGloble();
		return mView;
	}

	public void initGloble() {
		removeAddView();
		mListViewDailyList=(ListView)mView.findViewById(R.id.listview_daily);
		mSpinnerLectureNo= (Spinner) mView.findViewById(R.id.spinner_lecture_no);
		mButtonDateStart = (Button) mView.findViewById(R.id.button_date_start);
		mButtonDateEnd = (Button) mView.findViewById(R.id.button_date_end);
		mTextViewTo = (TextView) mView.findViewById(R.id.text_to);
		mTextViewTotalLecture = (TextView) mView.findViewById(R.id.text_total_lecture);
		textLectureNo = (TextView) mView.findViewById(R.id.text_lecture_no);
		
		arry_attendenceMaster = new ArrayList<AttendenceMaster>();
		mArrayArrayList= new ArrayList<ArrayList<AttendenceMaster>>();
		mArrayList = new ArrayList<AttendenceMaster>();
		mArrayAId = new ArrayList<Integer>();
		mArrayLectureNo = new ArrayList<Integer>();
		mLinearLayoutRollNo = (LinearLayout) mView
				.findViewById(R.id.layout_roll_no);
		mLinearLayoutAttend = (LinearLayout) mView
				.findViewById(R.id.layout_attend);
		
		addListenerOnButton();
		setEndDateonView();

		if (reportType.equals("Weekly")) {
			mActionbar.setTitle("Weekly Report");
			mSpinnerLectureNo.setVisibility(View.GONE);
			textLectureNo.setVisibility(View.GONE);
			setStartDateOnView();

		} else if (reportType.equals("3 Days")) {
			mActionbar.setTitle("3 Days Report");
			mSpinnerLectureNo.setVisibility(View.GONE);
			textLectureNo.setVisibility(View.GONE);
			setStartDateOnView();

		} else if (reportType.equals("Daily")){
			mActionbar.setTitle("Daily Report");
			mSpinnerLectureNo.setVisibility(View.GONE);
			textLectureNo.setVisibility(View.GONE);
			mButtonDateStart.setVisibility(View.GONE);
			mTextViewTo.setVisibility(View.GONE);
		}else if (reportType.equals("Full")){
			mActionbar.setTitle("Full Report");
			mSpinnerLectureNo.setVisibility(View.GONE);
			textLectureNo.setVisibility(View.GONE);
			mButtonDateStart.setVisibility(View.GONE);
			mButtonDateEnd.setVisibility(View.GONE);
			mTextViewTo.setVisibility(View.GONE);
			mTextViewTotalLecture.setVisibility(View.VISIBLE);
			new GettingAttendanceApi().execute(subjectid);
			
		}else if(reportType.equals("Taken")){
			mActionbar.setTitle("Attendance Taken");
			mSpinnerLectureNo.setVisibility(View.GONE);
			textLectureNo.setVisibility(View.GONE);
			mButtonDateStart.setVisibility(View.GONE);
			mButtonDateEnd.setVisibility(View.GONE);
			mTextViewTo.setVisibility(View.GONE);
			Bundle bundle = new Bundle();
			bundle = getArguments();
			takeanDate = bundle.getString("TakenDate");
			String startd = takeanDate + " " + "00:00:00";
			String endd = takeanDate + " " + "23:59:59";
			new GettingAttendanceApi().execute(classid, startd, endd);
			
		}
		else{
			mActionbar.setTitle("Edit Attendance");
			mButtonDateStart.setVisibility(View.GONE);
			mTextViewTo.setVisibility(View.GONE);
		}
		mListViewDailyList.setOnItemClickListener(new OnItemClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				
				if (reportType.equals("Edit"))
				{
						final TextView textSid=(TextView)arg1.findViewById(R.id.text_attend_stud_id);
						final RelativeLayout layout=(RelativeLayout)arg1.findViewById(R.id.layout_view);
						final TextView textAid=(TextView)arg1.findViewById(R.id.text_attend_attend_id);
						textAttend=(TextView)arg1.findViewById(R.id.text_attend_view);
						Log.e("","Sid: "+textSid.getText().toString());	
						Log.e("","Aid: "+textAid.getText().toString());	
						 PopupMenu popup = new PopupMenu(getActivity(), textAid); 
						 popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
						 popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {  
							 
							@SuppressLint("ResourceAsColor")
							@Override
							public boolean onMenuItemClick(
									android.view.MenuItem item) {
								// TODO Auto-generated method stub
								mAdapter.notifyDataSetChanged();
								mArrayArrayList.get(lectureNo).get(arg2).setAttend(item.getTitle()+"");
								mAdapter.notifyDataSetChanged();
								
								 textAttend.setText(item.getTitle()+"");
								 new UpdateAttendanceApi().execute(textAid.getText().toString(),item.getTitle()+"");
								return false;
							}  
					            });  
						 popup.show();
						}
					
				}
		});
		
		mSpinnerLectureNo.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if (mSpinnerLectureNo.getSelectedItem().equals("Select Lecture No")) {
					
				}else{
				
				 			mLinearLayoutAttend.removeAllViews();
							mLinearLayoutRollNo.removeAllViews();
							mListViewDailyList.setVisibility(View.VISIBLE);
							for (int i = 0; i < mArrayArrayList.get(arg2-1).size(); i++) {
								
										mAdapter=new AttendanceListAdapter(getActivity(), mArrayArrayList.get(arg2-1));
										mListViewDailyList.setAdapter(mAdapter);
										lectureNo=arg2-1;
							}
				
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});

	}

	@SuppressLint("SimpleDateFormat")
	public void setEndDateonView() {

		Calendar c = Calendar.getInstance();
		yearEnd = c.get(Calendar.YEAR);
		monthEnd = c.get(Calendar.MONTH);
		dayEnd = c.get(Calendar.DAY_OF_MONTH);

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat month_date = new SimpleDateFormat("yyyy-MM-dd");

		String month_name = month_date.format(cal.getTime());

		maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		mButtonDateEnd.setText(month_name);

		if (reportType.equals("Daily") || reportType.equals("Edit")) {
			startdate = month_name + " " + "00:00:00";
			enddate = month_name + " " + "23:59:59";
			new GettingAttendanceApi().execute(subjectid, startdate, enddate);
		} else {
			enddate = month_name;

		}
		
	}

	@SuppressLint("SimpleDateFormat")
	public void setStartDateOnView() {
		Calendar c = Calendar.getInstance();
		int y = c.get(Calendar.YEAR);
		int m = c.get(Calendar.MONTH);
		int d = c.get(Calendar.DAY_OF_MONTH);
		
		Log.e("Class Id","ClassId: "+classid);
		if (reportType.equals("3 Days")) {
			int sz=3;
			SimpleDateFormat month_date = new SimpleDateFormat("yyyy-MM-dd");
			String day[]=new String[sz];
			Log.e(sz+"","size");
			daysStart= new String[3];
			daysEnd=new String[3];
			days=new String[3];
			for(int i=sz-1;i>=0;i--)
			{				
				day[i]=month_date.format(c.getTime());
				c.add(Calendar.DAY_OF_MONTH, -1);
				daysStart[i]=day[i]+" "+"00:00:00";
				daysEnd[i]=day[i]+" "+"23:59:59";
				days[i]=day[i];
			}
			mButtonDateStart.setText(day[0].toString());
			mButtonDateEnd.setText(day[2].toString());
			new GettingAttendanceApi().execute(subjectid, daysStart[0], daysEnd[2]);
			
			yearStart=Integer.parseInt(day[0].substring(0,4));
			monthStart=Integer.parseInt(day[0].substring(6,7))-1;
			dayStart=Integer.parseInt(day[0].substring(9,10));

		} else if (reportType.equals("Weekly")) {

			int sz=7;
			SimpleDateFormat month_date = new SimpleDateFormat("yyyy-MM-dd");
			String day[]=new String[sz];
			Log.e(sz+"","size");
			daysStart= new String[7];
			daysEnd=new String[7];
			days=new String[7];
			for(int i=sz-1;i>=0;i--)
			{	
				day[i]=month_date.format(c.getTime());
				c.add(Calendar.DAY_OF_MONTH, -1);
				daysStart[i]=day[i]+" "+"00:00:00";
				daysEnd[i]=day[i]+" "+"23:59:59";
				days[i]=day[i].substring(5,10);
			}
			mButtonDateStart.setText(day[0].toString());
			mButtonDateEnd.setText(day[6].toString());
			new GettingAttendanceApi().execute(subjectid, daysStart[0], daysEnd[6]);
		
			yearStart=Integer.parseInt(day[0].substring(0,4));
			monthStart=Integer.parseInt(day[0].substring(6,7))-1;
			dayStart=Integer.parseInt(day[0].substring(9,10));
	
			
		}else if(reportType.equals("Taken"))
		{
			
		}
	}

	public void openDialogForDateEnd() {
		mDatePickerDialogEndDate = new DatePickerDialog(getActivity(),
				datePickerEnd, yearEnd, monthEnd, dayEnd);
		mDatePickerDialogEndDate.show();
	}

	public void openDialogForDateStart() {
		mDatePickerDialogStartDate = new DatePickerDialog(getActivity(),
				datePickerStart, yearStart, monthStart, dayStart);
		mDatePickerDialogStartDate.show();
	}

	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID_END:

			return new DatePickerDialog(getActivity(), datePickerEnd, yearEnd,
					monthEnd, dayEnd);
		case DATE_DIALOG_ID_START:

			return new DatePickerDialog(getActivity(), datePickerStart,
					yearStart, monthStart, dayStart);
		}
		return null;
	}

	public void addListenerOnButton() {

		mButtonDateEnd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				openDialogForDateEnd();
			}

		});
		mButtonDateStart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				openDialogForDateStart();
			}

		});

	}
	
	private DatePickerDialog.OnDateSetListener datePickerStart = new DatePickerDialog.OnDateSetListener() {

		@Override
		@SuppressLint("SimpleDateFormat")
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			yearStart = selectedYear;
			monthStart = selectedMonth;
			dayStart = selectedDay;

			previousMonthDate = new StringBuilder().append(dayStart)
					.append("-").append(monthStart + 1).append("-")
					.append(yearStart).append(" ")
					+ "";
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			Date date = new Date();
			try {
				date = dateFormat.parse(previousMonthDate);
				lastCalender = Calendar.getInstance();
				lastCalender.setTime(date);
				SimpleDateFormat month_date = new SimpleDateFormat("yyyy-MM-dd");
				String month_name = month_date.format(lastCalender.getTime());
				maxDay = lastCalender.getActualMaximum(Calendar.DAY_OF_MONTH);
				mButtonDateStart.setText(month_name);
				startdate = month_name;
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (reportType.equals("3 Days")) {
				
				int sz=3;
				SimpleDateFormat month_date = new SimpleDateFormat("yyyy-MM-dd");
				String day[]=new String[sz];
				
				daysStart= new String[3];
				daysEnd=new String[3];
				days=new String[3];
				
				for(int i=0;i<sz;i++)
				{				
					day[i]=month_date.format(lastCalender.getTime());
					lastCalender.add(Calendar.DAY_OF_MONTH, 1);
					daysStart[i]=day[i]+" "+"00:00:00";
					daysEnd[i]=day[i]+" "+"23:59:59";
					days[i]=day[i];
				}
				
				
				mButtonDateStart.setText(day[0].toString());
				mButtonDateEnd.setText(day[2].toString());
				new GettingAttendanceApi().execute(subjectid, daysStart[0], daysEnd[2]);
					yearEnd=Integer.parseInt(day[2].substring(0,4));
					monthEnd=Integer.parseInt(day[2].substring(6,7))-1;
					dayEnd=Integer.parseInt(day[2].substring(9,10));

			} else if (reportType.equals("Weekly")) {
				
				int sz=7;
				SimpleDateFormat month_date = new SimpleDateFormat("yyyy-MM-dd");
				String day[]=new String[sz];
				
				daysStart= new String[7];
				daysEnd=new String[7];
				days=new String[7];
				
				for(int i=0;i<sz;i++)
				{				
					day[i]=month_date.format(lastCalender.getTime());
					lastCalender.add(Calendar.DAY_OF_MONTH, 1);
					daysStart[i]=day[i]+" "+"00:00:00";
					daysEnd[i]=day[i]+" "+"23:59:59";
					days[i]=day[i].substring(5,10);
				}
				
				
				
				mButtonDateStart.setText(day[0].toString());
				mButtonDateEnd.setText(day[6].toString());
				new GettingAttendanceApi().execute(subjectid, daysStart[0], daysEnd[6]);
				
				yearEnd=Integer.parseInt(day[6].substring(0,4));
				monthEnd=Integer.parseInt(day[6].substring(6,7))-1;
				dayEnd=Integer.parseInt(day[6].substring(9,10));

			}

		}
	};

	private DatePickerDialog.OnDateSetListener datePickerEnd = new DatePickerDialog.OnDateSetListener() {

		@Override
		@SuppressLint("SimpleDateFormat")
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			yearEnd = selectedYear;
			monthEnd = selectedMonth;
			dayEnd = selectedDay;

			previousMonthDate = new StringBuilder().append(yearEnd).append("-")
					.append(monthEnd + 1).append("-").append(dayEnd)
					.append(" ")
					+ "";
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			try {
				date = dateFormat.parse(previousMonthDate);
				lastCalender = Calendar.getInstance();
				lastCalender.setTime(date);
				SimpleDateFormat month_date = new SimpleDateFormat("yyyy-MM-dd");
				String month_name = month_date.format(lastCalender.getTime());
				maxDay = lastCalender.getActualMaximum(Calendar.DAY_OF_MONTH);
				mButtonDateEnd.setText(month_name);
				enddate = month_name;
				
				if(reportType.equals("Daily")|| reportType.equals("Edit")){
					enddate = month_name + " " + "23:59:59";
					startdate = month_name + " " + "00:00:00";
					 new GettingAttendanceApi().execute(subjectid, startdate, enddate);
				}
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (reportType.equals("3 Days")) {
				
				int sz=3;
				SimpleDateFormat month_date = new SimpleDateFormat("yyyy-MM-dd");
				String day[]=new String[sz];
				
				daysStart= new String[3];
				daysEnd=new String[3];
				days=new String[3];
				
				for(int i=sz-1;i>=0;i--)
				{				
					day[i]=month_date.format(lastCalender.getTime());
					lastCalender.add(Calendar.DAY_OF_MONTH, -1);
					daysStart[i]=day[i]+" "+"00:00:00";
					daysEnd[i]=day[i]+" "+"23:59:59";
					days[i]=day[i];
				}
				
				mButtonDateStart.setText(day[0].toString());
				mButtonDateEnd.setText(day[2].toString());
				new GettingAttendanceApi().execute(subjectid, daysStart[0], daysEnd[2]);
					yearStart=Integer.parseInt(day[0].substring(0,4));
					monthStart=Integer.parseInt(day[0].substring(6,7))-1;
					dayStart=Integer.parseInt(day[0].substring(9,10));
				

			} else if (reportType.equals("Weekly")) {

				int sz=7;
				SimpleDateFormat month_date = new SimpleDateFormat("yyyy-MM-dd");
				String day[]=new String[sz];
				
				daysStart= new String[7];
				daysEnd=new String[7];
				days=new String[7];

				for(int i=sz-1;i>=0;i--)
				{				
					day[i]=month_date.format(lastCalender.getTime());
					lastCalender.add(Calendar.DAY_OF_MONTH, -1);
					daysStart[i]=day[i]+" "+"00:00:00";
					daysEnd[i]=day[i]+" "+"23:59:59";
					days[i]=day[i].substring(5,10);
				}
				
				mButtonDateStart.setText(day[0].toString());
				mButtonDateEnd.setText(day[6].toString());
				new GettingAttendanceApi().execute(subjectid, daysStart[0], daysEnd[6]);
				
				yearStart=Integer.parseInt(day[0].substring(0,4));
				monthStart=Integer.parseInt(day[0].substring(6,7))-1;
				dayStart=Integer.parseInt(day[0].substring(9,10));

			}
		}
	};

	public class UpdateAttendanceApi extends AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		  /*pd = new ProgressDialog(getActivity());
			pd.setMessage("Updating Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();*/
		}

		
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("Id", params[0]));
			postParams.add(new BasicNameValuePair("Attend", params[1]));
			
			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "attendance_update.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("UpdateAttendanceApi", "Error : " + e.toString());
				return null;
			}
		}

		
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
		//	pd.dismiss();
			try {
				if (result != null) {
					processUpdate(result);
				} else {
					
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processUpdate(JSONObject result) {
		try {
			String message = result.getString("Message");
			if (result.getInt("Status") == 1) {
				ToastMsg.showSuccess(getActivity(), message,ToastMsg.GRAVITY_CENTER);
				mAdapter.notifyDataSetChanged();
			} else {
				ToastMsg.showError(getActivity(), message,ToastMsg.GRAVITY_CENTER);
				String check=textAttend.getText().toString();
				if(check.equals("Present")){
					textAttend.setText("Absent");
				}else{
					textAttend.setText("Present");
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
		}
	}
	
	class GettingAttendanceApi extends AsyncTask<String, String, JSONObject> {
		JSONObject jsonobject;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... transaction) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();
			try {
				List<NameValuePair> postParams = new ArrayList<NameValuePair>();

				if (reportType.equals("Full")){
					postParams.add(new BasicNameValuePair("SubjectId",
							transaction[0]));
					return parser.getJSONFromUrl(Global.SERVER_PATH
							+ "list_all_attendance_by_subjectid.php", postParams);
				}else{
				
				if(reportType.equals("Taken")){
					postParams.add(new BasicNameValuePair("ClassId",
							transaction[0]));
				}else{
					postParams.add(new BasicNameValuePair("SubjectId",
							transaction[0]));
				}
				
				postParams.add(new BasicNameValuePair("StartTime",
						transaction[1]));
				postParams
						.add(new BasicNameValuePair("EndTime", transaction[2]));
				
				if(reportType.equals("Taken")){
					return parser.getJSONFromUrl(Global.SERVER_PATH
							+ "today_attendance_report.php", postParams);
				}else{
					return parser.getJSONFromUrl(Global.SERVER_PATH
							+ "report_test.php", postParams);
				}
				}

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("report_test", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			//pd.dismiss();
			try {
				JSONArray jat = result.getJSONArray("Data");
				Log.e("Full result","Full result "+jat.length()+" data "+result);

				int t=0;				
				mArrayArrayList= new ArrayList<ArrayList<AttendenceMaster>>();
				mArrayAId.clear();
				mArrayLectureNo.clear();
				mArrayArrayList.clear();
				mArrayList.clear();
				if (jat.length() > 0) {
				
					AttendenceMaster obj;
					mArrayList = new ArrayList<AttendenceMaster>();
					for (int i = 0; i < jat.length(); i++) {

						jsonobject = jat.getJSONObject(i);
						obj = new AttendenceMaster(
								jsonobject.getInt("Id"),
								jsonobject.getInt("AttendanceId"),
								jsonobject.getInt("StudentId"),
								jsonobject.getString("StudentName"),
								jsonobject.getString("LectureNo"),
								jsonobject.getInt("RollNo"),							
								jsonobject.getString("Attend"),
								jsonobject.getString("Time"));
						boolean flag = true;
						for (int j = 0; j < mArrayAId.size(); j++) {
							if (obj.getAid() == mArrayAId.get(j)) {
								flag = false;					
							}
						}
						if (flag == true) {
							if(t>0)
							{
								mArrayArrayList.add(mArrayList);
								mArrayList = new ArrayList<AttendenceMaster>();
							}
							mArrayAId.add(obj.getAid());
							mArrayLectureNo.add(Integer.parseInt(obj.getLectureNo()));
							t++;
						}
						mArrayList.add(obj);
					}
					mArrayArrayList.add(mArrayList);
					
					if(reportType.equals("Edit")){
						spinnerListLectureNo=new String[mArrayLectureNo.size()+1];
						for(int l=0;l<=mArrayLectureNo.size();l++){
							if(l==0){
								spinnerListLectureNo[l]="Select Lecture No";
							}else{
								spinnerListLectureNo[l]=mArrayLectureNo.get(l-1).toString();
								Log.e("spinnerListLectureNo","spinnerListLectureNo: "+spinnerListLectureNo[l]);
							}
							
						}

						ArrayAdapter<String> categoriesAdapter = new ArrayAdapter<String>(
								getActivity(), R.layout.spinner_item, spinnerListLectureNo);
						categoriesAdapter
								.setDropDownViewResource(android.R.layout.simple_list_item_1);
						mSpinnerLectureNo.setAdapter(categoriesAdapter);
						
					}else if (reportType.equals("Full")){
						Log.e("Full","Full");
						totalNumOfPresentCounter();
						
					}else{
						if(mArrayAId.size()==1){
							mLinearLayoutAttend.removeAllViews();
							mLinearLayoutRollNo.removeAllViews();
							mListViewDailyList.setVisibility(View.VISIBLE);
							for (int i = 0; i < jat.length(); i++) {
								jsonobject = jat.getJSONObject(i);
								
									mAttendenceMasterList=new AttendenceMaster(jsonobject.getInt("Id"),jsonobject.getInt("StudentId"),jsonobject.getString("StudentName"),jsonobject.getInt("RollNo"), jsonobject.getString("Attend"));
									arry_attendenceMaster.add(mAttendenceMasterList);
									if (mAdapter == null) {
										mAdapter=new AttendanceListAdapter(getActivity(), arry_attendenceMaster);
										mListViewDailyList.setAdapter(mAdapter);
										
									}else{
										mAdapter.notifyDataSetChanged();
									}
							
							}
						}else{
							mListViewDailyList.setVisibility(View.GONE);
							loadLayout();
							fillRollNo();
							if(reportType.equals("Taken")){
								showReport();
							}
							
						}
					}
				
				} else {
					// Do Some thing when null
					mListViewDailyList.setVisibility(View.GONE);
					mLinearLayoutAttend.removeAllViews();
					mLinearLayoutRollNo.removeAllViews();
					ToastMsg.showError(getActivity(), "No Attendance Taken",ToastMsg.GRAVITY_TOP);
					spinnerListLectureNo=new String[0];
					ArrayAdapter<String> categoriesAdapter = new ArrayAdapter<String>(
							getActivity(), R.layout.spinner_item, spinnerListLectureNo);
					categoriesAdapter
							.setDropDownViewResource(android.R.layout.simple_list_item_1);
					mSpinnerLectureNo.setAdapter(categoriesAdapter);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.e("Try catch", e.toString());

			}
			pd.dismiss();
			super.onPostExecute(result);
		}
	}
	
	public void totalNumOfPresentCounter(){
		numberOfTotalPresent=new int[mArrayArrayList.get(0).size()][1];
		counterNumberOfTotalPresent=new int[mArrayArrayList.get(0).size()];

			for(int i=0;i<mArrayArrayList.get(0).size();i++)
				{
					counterNumberOfTotalPresent[i]=0;
					for(int j=0;j<mArrayArrayList.size();j++)
					{
							if(mArrayArrayList.get(j).get(i).getAttend().equals("Present"))
							{
								Log.e("counterNumberOfTotalPresent[i]","counterNumberOfTotalPresent[i]: "+counterNumberOfTotalPresent[i]);
								counterNumberOfTotalPresent[i]++;
							}
					}
					numberOfTotalPresent[i][0]=counterNumberOfTotalPresent[i];
				}
		
		FullReportListAdapter fullAdapter=null;
		ArrayList<FullReport> arryFullReport;
		FullReport fullReport;
		arryFullReport=new ArrayList<FullReport>();
		mTextViewTotalLecture.setText("Total Lectures: "+mArrayLectureNo.size());
		for (int i = 0; i < mArrayArrayList.get(0).size(); i++) {
			
			fullReport=new FullReport(mArrayList.get(i).getSid(),mArrayList.get(i).getEnrolmentno(),mArrayList.get(i).getStudname(),mArrayLectureNo.size()+"",counterNumberOfTotalPresent[i]+"");
			arryFullReport.add(fullReport);
				if (fullAdapter == null) {
					fullAdapter=new FullReportListAdapter(getActivity(), arryFullReport);
					mListViewDailyList.setAdapter(fullAdapter);
					
				}else{
					fullAdapter.notifyDataSetChanged();
				}
		}
		
	}
	
	@SuppressLint("ResourceAsColor")
	public void loadLayout(){
		mLinearLayoutAttend.removeAllViews();
		LinearLayout linearLayout;
		Log.e("","mArrayArrayList: "+mArrayArrayList.size());
		int arrayListlength=mArrayArrayList.size();
		totalAbsent=new int[mArrayLectureNo.size()][1];
		totalPresent=new int[mArrayLectureNo.size()][1];
		
	    strLeft=new String[mArrayLectureNo.size()];
	    strAdded=new String[mArrayLectureNo.size()];
	    //sort the array based on lecture number
	    for(int i=0;i<mArrayLectureNo.size();i++)
	    {
	    	for(int j=0;j<=i;j++)
	    	{
	    		if(mArrayLectureNo.get(i).toString().compareTo(mArrayLectureNo.get(j).toString())<0)
	    		{
	    			int tempStr=mArrayLectureNo.get(j);	    			
	    			mArrayLectureNo.set(j,mArrayLectureNo.get(i));
	    			mArrayLectureNo.set(i,tempStr);
	    			ArrayList<AttendenceMaster> tempAM;
	    			tempAM=mArrayArrayList.get(j);
	    			mArrayArrayList.set(j, mArrayArrayList.get(i));
	    			mArrayArrayList.set(i, tempAM);
	    		}
	    	}
	    }
		for(int i=0;i<arrayListlength;i++)
		{
			
			counterAbsent=0;
			counterPresent=0;
			
			View view = getActivity().getLayoutInflater().inflate(
					R.layout.custom_layout, null);	
			linearLayout = (LinearLayout) view.findViewById(R.id.custom_layout);			
			//Here we have to set width based on number of attendance to view
			switch(arrayListlength)
			{
			case 1:
				linearLayout.getLayoutParams().width=240;
				break;
			case 2:
				linearLayout.getLayoutParams().width=225;
				break;
			case 3:
				linearLayout.getLayoutParams().width=150;
				break;
			case 4:
				linearLayout.getLayoutParams().width=110;
				break;
			case 5:
				linearLayout.getLayoutParams().width=90;
				break;
			default:				
				linearLayout.getLayoutParams().width=90;
				break;
				
			}
			
			for(int j=0;j<mArrayArrayList.get(i).size();j++)
			{
				//j loop is for number of students in attendanceclass
				//Log.e("j",j+"" );
				if(j==0){
					View viewDate = getActivity().getLayoutInflater().inflate(
							R.layout.attendance_list_date_wise, null);
					TextView textno = (TextView) viewDate.findViewById(R.id.text_s_eno);
					LinearLayout layoutLine = (LinearLayout) viewDate.findViewById(R.id.layout_line);
					layoutLine.setBackgroundResource(R.color.actionbar_color);
					textno.setTextSize(12);
					if(reportType.equals("Taken")){
						textno.setText("Lecture\n"+mArrayArrayList.get(i).get(j).getLectureNo());
					}else{
						textno.setText(mArrayArrayList.get(i).get(j).getTime().substring(5, 10)+"\nLecture:"+mArrayArrayList.get(i).get(j).getLectureNo());
					}
					
					linearLayout.addView(viewDate);	
				}
				View view1 = getActivity().getLayoutInflater().inflate(
						R.layout.attendance_list_date_wise, null);
				TextView textno = (TextView) view1.findViewById(R.id.text_s_eno);
			
				if(mArrayArrayList.get(i).get(j).getAttend().equals("Present"))
				{
					textno.setText("P");
					counterPresent++;
				}else if(mArrayArrayList.get(i).get(j).getAttend().equals("Absent")){
					textno.setText("A");
					counterAbsent++;
					
				}
				
				linearLayout.addView(view1);					
			}
			totalAbsent[i][0]=counterAbsent;
			totalPresent[i][0]=counterPresent;
			
			if(i>0)
            {
                    strLeft[i]="";
                    strAdded[i]="";
                    
                    for(int l=0;l<mArrayArrayList.get(i).size();l++)
                    {
                            if(mArrayArrayList.get(i-1).get(l).getAttend().equals("Present") && mArrayArrayList.get(i).get(l).getAttend().equals("Absent"))
                            {
                                    strLeft[i]=strLeft[i]+mArrayArrayList.get(i).get(l).getEnrolmentno()+",";
                            }
                            if(mArrayArrayList.get(i-1).get(l).getAttend().equals("Absent") && mArrayArrayList.get(i).get(l).getAttend().equals("Present"))
                            {
                                    strAdded[i]=strAdded[i]+mArrayArrayList.get(i).get(l).getEnrolmentno()+",";
                            }
                    }
            }
		
			mLinearLayoutAttend.addView(view);
			
		}
		
		
	}
	
	public void fillRollNo() {	
		Log.e("fillRollNo", "fillRollNo");
		mLinearLayoutRollNo.removeAllViews();
		for (int i = 0; i < mArrayArrayList.get(0).size(); i++) {
			if(i==0){
				View viewDate = getActivity().getLayoutInflater().inflate(
						R.layout.attendance_list_date_wise, null);
				TextView textno = (TextView) viewDate.findViewById(R.id.text_s_eno);
				LinearLayout layoutLine = (LinearLayout) viewDate.findViewById(R.id.layout_line);
				layoutLine.setBackgroundResource(R.color.actionbar_color);
				textno.setTextSize(12);
				if(reportType.equals("Taken")){
					textno.setText("Show\nReport");
					textno.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							showReport();
						}
					});
				}else{
					textno.setText(" \n ");
				}
					
				//}
				
					mLinearLayoutRollNo.addView(viewDate);	
			}
			
			View view = getActivity().getLayoutInflater().inflate(
					R.layout.attendance_list_date_wise, null);
			TextView textno = (TextView) view.findViewById(R.id.text_s_eno);
			TextView textname = (TextView) view.findViewById(R.id.text_s_name);
			textno.setText(mArrayList.get(i).getEnrolmentno() + "");			
			//textname.setText(mArrayList.get(i).getStudname() + "");			
			mLinearLayoutRollNo.addView(view);

		}		
		View view = getActivity().getLayoutInflater().inflate(
				R.layout.attendance_list_date_wise, null);
		TextView textno = (TextView) view.findViewById(R.id.text_s_eno);
		mLinearLayoutRollNo.addView(view);

	}
	@SuppressLint("NewApi")
	public void showReport(){
		
		Bundle bundle = new Bundle();
		bundle = getArguments();
		classname = bundle.getString("Class Name");
		
		String msg="";
		
		Log.e("strAdded.length","strAdded.length: "+strAdded.length);
		Log.e("mArrayLectureNo.size()","mArrayLectureNo.size(): "+mArrayLectureNo.size());
		for(int i=1;i<strAdded.length;i++)
        {
                msg=msg+"\nLecture: "+mArrayArrayList.get(i).get(0).getLectureNo()+"\n    Bunked No: "+strLeft[i]+"\n    Newly Join No: "+strAdded[i];
        }
		msg="		"+classname+"\n"+msg;
		//AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),R.style.CustomAlertDialog);
		LayoutInflater adbInflater = LayoutInflater.from(getActivity());
		View reportView = adbInflater.inflate(R.layout.report_view_diff, null);
		TextView textClassName=(TextView)reportView.findViewById(R.id.text_classname);
		ListView listReport=(ListView)reportView.findViewById(R.id.listview_show_report);
		ShowReportListAdapter showAdapter=null;
		ArrayList<ShowReport> arryReport;
		ShowReport showReport;
		//Array Report need to fill
		//Manoj
		
		arryReport=new ArrayList<ShowReport>();
		textClassName.setText(classname);
		
		for(int i=1;i<strAdded.length;i++)
        {
		/*	if(i==1){
				showReport=new ShowReport(mArrayArrayList.get(i-1).get(0).getLectureNo(), totalPresent[i-1][0]+"", totalAbsent[i-1][0]+"");
				arryReport.add(showReport);
			}*/
			showReport=new ShowReport(mArrayArrayList.get(i).get(0).getLectureNo(), totalPresent[i][0]+"", totalAbsent[i][0]+"", strLeft[i]+"", strAdded[i]+"");
			arryReport.add(showReport);
			//showAdapter=new ShowReportListAdapter(getActivity(), arryReport);
        }
		showAdapter=new ShowReportListAdapter(getActivity(), arryReport);
		listReport.setAdapter(showAdapter);
		
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Today Lectures Report");
		//builder.setMessage(msg);
		builder.setView(reportView);
		
		builder.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		});
	
		builder.show();	
		
	}

	
	public void removeAddView(){
		WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		int width = display.getWidth();  // deprecated
		int height = display.getHeight();
		
		if(height<500){
			HomeActivity.mRLayoutAdd.setVisibility(View.VISIBLE);
		}else{
			LayoutParams params = HomeActivity.mDrawerLayout.getLayoutParams();
			((MarginLayoutParams) params).setMargins(0, 0, 0, 0); //left, top, right, bottom
			HomeActivity.mDrawerLayout.setLayoutParams(params);
		}
		Log.e("display","width: "+width+" height: "+height);
		
	}
}
