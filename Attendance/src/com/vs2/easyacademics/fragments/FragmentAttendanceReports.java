package com.vs2.easyacademics.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.R;

public class FragmentAttendanceReports extends SherlockFragment {

	private View mView;
	private ActionBar mActionbar;
	private Button mButtonDailyReport,mButton3daysReport,mButtonWeeklyReport,mButtonFullReport,
					mButtonEditAttendance;
	public String classid,subjetid,classname,subjectname;
	private TextView mTextViewClassName;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_attendance_reports, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		mActionbar.setTitle("Attendance Reports");
		
		Bundle bundle=new Bundle();
		bundle = getArguments();
		subjetid = bundle.getString("Subject ID");
		classid = bundle.getString("Class ID");
		classname = bundle.getString("Class Name");
		subjectname = bundle.getString("Subject Name");
		initGloble();
		return mView;
	}
	public void initGloble(){
		
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();	 
		
		mButtonDailyReport=(Button)mView.findViewById(R.id.button_daily_report);
		mButton3daysReport=(Button)mView.findViewById(R.id.button_3days_report);
		mButtonWeeklyReport=(Button)mView.findViewById(R.id.button_weekly_report);
		mButtonFullReport=(Button)mView.findViewById(R.id.button_full_report);
		mButtonEditAttendance=(Button)mView.findViewById(R.id.button_edit_attendance);
		mTextViewClassName=(TextView)mView.findViewById(R.id.text_classname);
		mTextViewClassName.setText(classname +" -> "+subjectname );
				
		mButtonDailyReport.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Bundle bundle=new Bundle();
				bundle.putString("Subject ID", subjetid);
				bundle.putString("Report", "Daily");
				bundle.putString("Class ID", classid);
				FragmentViewAttendance fragmentViewAttendance=new FragmentViewAttendance();
				fragmentViewAttendance.setArguments(bundle);
	    		fragmentTransaction.replace(R.id.frame_layout, fragmentViewAttendance).addToBackStack(null).commit();
			}
		});
		mButton3daysReport.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				/*Bundle bundle=new Bundle();
				bundle.putString("Subject ID", subjetid);
				bundle.putString("Report", "3 Days");
				bundle.putString("Class ID", classid);
				FragmentReportViewAttendance fragmentReportViewAttendance=new FragmentReportViewAttendance();
				fragmentReportViewAttendance.setArguments(bundle);
	    		fragmentTransaction.replace(R.id.frame_layout, fragmentReportViewAttendance).addToBackStack(null).commit();*/
				Bundle bundle=new Bundle();
				bundle.putString("Subject ID", subjetid);
				bundle.putString("Report", "3 Days");
				bundle.putString("Class ID", classid);
				FragmentViewAttendance fragmentViewAttendance=new FragmentViewAttendance();
				fragmentViewAttendance.setArguments(bundle);
	    		fragmentTransaction.replace(R.id.frame_layout, fragmentViewAttendance).addToBackStack(null).commit();
			}
		});
		mButtonWeeklyReport.setOnClickListener(new View.OnClickListener() {
	
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Bundle bundle=new Bundle();
				bundle.putString("Subject ID", subjetid);
				bundle.putString("Report", "Weekly");
				bundle.putString("Class ID", classid);
				FragmentViewAttendance fragmentViewAttendance=new FragmentViewAttendance();
				fragmentViewAttendance.setArguments(bundle);
	    		fragmentTransaction.replace(R.id.frame_layout, fragmentViewAttendance).addToBackStack(null).commit();
			}
		});
		mButtonFullReport.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Bundle bundle=new Bundle();
				bundle.putString("Subject ID", subjetid);
				bundle.putString("Subject Name", subjectname);
				bundle.putString("Report", "Full");
				bundle.putString("Class ID", classid);
				FragmentViewAttendance fragmentViewAttendance=new FragmentViewAttendance();
				fragmentViewAttendance.setArguments(bundle);
	    		fragmentTransaction.replace(R.id.frame_layout, fragmentViewAttendance).addToBackStack(null).commit();
			}
		});
		mButtonEditAttendance.setOnClickListener(new View.OnClickListener() {
	
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Bundle bundle=new Bundle();
				bundle.putString("Subject ID", subjetid);
				bundle.putString("Report", "Edit");
				bundle.putString("Class ID", classid);
				FragmentViewAttendance fragmentViewAttendance=new FragmentViewAttendance();
				fragmentViewAttendance.setArguments(bundle);
	    		fragmentTransaction.replace(R.id.frame_layout, fragmentViewAttendance).addToBackStack(null).commit();
			}
		});
	}
}
