package com.vs2.easyacademics.fragments;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.adapters.AssignmentListAdapter;
import com.vs2.easyacademics.adapters.AssignmentReportListAdapter;
import com.vs2.easyacademics.fragments.FragmentAdminClasses.CreateClassApi;
import com.vs2.easyacademics.fragments.FragmentSubjectList.DeleteSubjectApi;
import com.vs2.easyacademics.objects.AssignmentMaster;
import com.vs2.easyacademics.objects.AssignmentReportMaster;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.ToastMsg;
import com.vs2.easyacademics.objects.Utility;

public class FragmentAssignmentlist extends SherlockFragment {
	
	private View mView;
	private ActionBar mActionbar;
	private ListView mListviewAssignment,mListviewStudent;
	private AssignmentListAdapter mAdapter;
	private AssignmentMaster mAssignmentMasterList;
	ArrayList<AssignmentMaster> arry_AssignmentMaster;
	
	private AssignmentReportListAdapter mAdapterReport;
	private AssignmentReportMaster mAssignmentReportMasterList;
	ArrayList<AssignmentReportMaster> arry_AssignmentReportMaster;
	
	String subjectid,AssignmentId,action,classid,classname,subjectname;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_assignmentlist, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		mActionbar.setTitle("Assignments");
		Bundle bundle=new Bundle();
		bundle = getArguments();
		subjectid = bundle.getString("Subject ID");
		action = bundle.getString("Action");
		classid = bundle.getString("Class ID");
		classname = bundle.getString("Class Name");
		subjectname=bundle.getString("Subject Name");
		initGloble();
		return mView;
	}
	public void initGloble(){
		arry_AssignmentMaster=new ArrayList<AssignmentMaster>();
		arry_AssignmentReportMaster=new ArrayList<AssignmentReportMaster>();
		mListviewAssignment=(ListView)mView.findViewById(R.id.listview_assignments_list);
		mListviewStudent=(ListView)mView.findViewById(R.id.listview_student_list);
	//	mListviewStudent.setVisibility(View.GONE);
		new GetingAssignmentListApi().execute(subjectid);
		
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();	
		
		mListviewAssignment.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				final TextView ass_id = (TextView) view.findViewById(R.id.text_assignment_id);
				AssignmentId = ass_id.getText().toString();
				
				//Log.e("AssignmentId","AssignmentId: "+AssignmentId);
				
				if(action.equals("Take Assignments")){
				    FragmentTakeAttendance fragmentTakeAttendance=new FragmentTakeAttendance();
					Bundle bundle=new Bundle();
					bundle.putString("Subject ID", subjectid);
					bundle.putString("Subject Name", subjectname);
					bundle.putString("Class ID", classid);
					bundle.putString("Assignment ID", AssignmentId);
					bundle.putString("Action", action);
					fragmentTakeAttendance.setArguments(bundle);
					fragmentTransaction.replace(R.id.frame_layout, fragmentTakeAttendance).addToBackStack(null).commit();
				}else{
					new GetingAssignmentReportApi().execute(AssignmentId);
					
				}
			}
		});
		mListviewStudent.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					final int position, long id) {
				// TODO Auto-generated method stub
				final TextView textId = (TextView) view.findViewById(R.id.text_assignment_report_id);
				
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setMessage("Do you want to delete this record?");
				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						new DeleteAssignmentDetailsApi().execute(textId.getText().toString());
						arry_AssignmentReportMaster.remove(position);
						mAdapterReport.notifyDataSetChanged();
					}
				});
				builder.setNegativeButton("No", null);
				builder.show();
			}
		});
		
		mListviewAssignment.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					final int arg2, long arg3) {
				// TODO Auto-generated method stub
				final TextView ass_id = (TextView) arg1.findViewById(R.id.text_assignment_id);
				AssignmentId = ass_id.getText().toString();
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setMessage("Are you sure want to delete this assignment?");
				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						new DeleteAssignmentApi().execute(AssignmentId);
						arry_AssignmentMaster.remove(arg2);
					}
				});
				builder.setNegativeButton("No", null);
				builder.show();
				return false;
			}
		});
	
	}
	public class DeleteAssignmentApi extends AsyncTask<String,Integer,JSONObject> {

		ProgressDialog pd;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
			
		}
		
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			JSONParser parser = new JSONParser();

			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("AssignmentId", params[0]));

			try {
				
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "assignment_master_delete.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("assignment_master_delete", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				mAdapter.notifyDataSetChanged();
				ToastMsg.showSuccess(getActivity(), "Assignment Successfully Deleted.", ToastMsg.GRAVITY_TOP);
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}
	}
	
	public class DeleteAssignmentDetailsApi extends AsyncTask<String,Integer,JSONObject> {

		ProgressDialog pd;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
			
		}
		
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			JSONParser parser = new JSONParser();

			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("AssignmentId", params[0]));

			try {
				
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "assignment_details_delete.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("assignment_details_delete", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				mAdapter.notifyDataSetChanged();
				ToastMsg.showSuccess(getActivity(), "Record Successfully Deleted.", ToastMsg.GRAVITY_TOP);
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}
	}
	class GetingAssignmentListApi extends AsyncTask<String, String, JSONObject> {
		JSONObject jsonobject;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... transaction) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();
			try {
				List<NameValuePair> postParams = new ArrayList<NameValuePair>();
				postParams.add(new BasicNameValuePair("SubjectId",transaction[0]));
								
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "assignment_master_list.php",
						postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("Class List", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject resultTransaction) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				
				if (resultTransaction != null) {
					JSONArray jat = resultTransaction.getJSONArray("Data");
					Log.e("Transactionlist length ", jat.length() + "");				
					for(int i = 0; i < jat.length(); i++)
		   			{
						jsonobject=jat.getJSONObject(i);
						mAssignmentMasterList=new AssignmentMaster(jsonobject.getInt("Id"),jsonobject.getString("AssignmentName"), jsonobject.getString("GivenDate"), jsonobject.getString("SubmitionDate"));
						arry_AssignmentMaster.add(mAssignmentMasterList);
											
		   			} 
					if (mAdapter == null) {
						mAdapter=new AssignmentListAdapter(getActivity(), arry_AssignmentMaster);
						mListviewAssignment.setAdapter(mAdapter);
						
					}else{
						//mAdapter.notifyDataSetChanged();
						mListviewAssignment.setAdapter(mAdapter);
					}
						
					
				} else{
					ToastMsg.showError(getActivity(),
							"Result is null", ToastMsg.GRAVITY_TOP);
				}			
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			super.onPostExecute(resultTransaction);
		}
	}
	
	class GetingAssignmentReportApi extends AsyncTask<String, String, JSONObject> {
		JSONObject jsonobject;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... transaction) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();
			try {
				List<NameValuePair> postParams = new ArrayList<NameValuePair>();
				postParams.add(new BasicNameValuePair("AssignmentId",transaction[0]));
								
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "assignment_report.php",
						postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("Class List", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject resultTransaction) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				
				if (resultTransaction != null) {
					JSONArray jat = resultTransaction.getJSONArray("Data");
					if(jat.length()>0){	
						mListviewAssignment.setVisibility(View.GONE);
						for(int i = 0; i < jat.length(); i++)
						{
							jsonobject=jat.getJSONObject(i);
							mAssignmentReportMasterList=new AssignmentReportMaster(jsonobject.getInt("Id"),jsonobject.getString("SubmitDate"),jsonobject.getString("RollNo"), jsonobject.getString("StudentName"));
							arry_AssignmentReportMaster.add(mAssignmentReportMasterList);
							
						} 
						if (mAdapterReport == null) {
								mAdapterReport=new AssignmentReportListAdapter(getActivity(), arry_AssignmentReportMaster);
								mListviewStudent.setAdapter(mAdapterReport);
							
							}else{
								mListviewStudent.setAdapter(mAdapterReport);
								//mAdapterReport.notifyDataSetChanged();
							}
					}else{
						ToastMsg.showError(getActivity(),
								"No one Submit this assignment", ToastMsg.GRAVITY_TOP);
					}
					
				} else{
					ToastMsg.showError(getActivity(),
							"Result is null", ToastMsg.GRAVITY_TOP);
				}			
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			super.onPostExecute(resultTransaction);
		}
	}
}
