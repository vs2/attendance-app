package com.vs2.easyacademics.fragments;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.HomeActivity;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.adapters.AttendanceListAdapter;
import com.vs2.easyacademics.adapters.FullReportListAdapter;
import com.vs2.easyacademics.adapters.ShowReportListAdapter;
import com.vs2.easyacademics.fragments.FragmentViewAttendance.GettingAttendanceApi;
import com.vs2.easyacademics.fragments.FragmentViewAttendance.UpdateAttendanceApi;
import com.vs2.easyacademics.objects.AttendenceMaster;
import com.vs2.easyacademics.objects.FullReport;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.ShowReport;
import com.vs2.easyacademics.objects.StudentReportMaster;
import com.vs2.easyacademics.objects.ToastMsg;

public class FragmentStudentReport extends SherlockFragment {

	private View mView;
	private ActionBar mActionbar;
	String classid,classname,studentid,action,previousMonthDate;

	private Button mButtonDateStart, mButtonDateEnd,mButtonGo;
	static final int DATE_DIALOG_ID_END = 999, DATE_DIALOG_ID_START = 888;
	private DatePickerDialog mDatePickerDialogEndDate,
			mDatePickerDialogStartDate;
	String enddate = null, startdate = null;
	private int yearEnd, monthEnd, dayEnd, yearStart, monthStart, dayStart;
	private Calendar lastCalender;
	private LinearLayout mLinearLayoutRollNo, mLinearLayoutAttend;
	
	private StudentReportMaster mStudentReportMasterList;
	private ArrayList<StudentReportMaster> mArrayList,mArrayTime,mArrayAll;
	private ArrayList<ArrayList<StudentReportMaster>> mArrayArrayList;
	private ArrayList<Integer> mArrayLectureNo;
	
	private Boolean mCheck;
		
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_student_report, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		mActionbar.setTitle("Student Card");
		Bundle bundle = new Bundle();
		bundle = getArguments();
		classid = bundle.getString("Class ID");
		studentid = bundle.getString("Student ID");
		action = bundle.getString("Action");
		initGloble();
		return mView;
	}

	public void initGloble() {
		
		mArrayList = new ArrayList<StudentReportMaster>();
		mArrayAll = new ArrayList<StudentReportMaster>();
		mArrayTime = new ArrayList<StudentReportMaster>();
		mArrayLectureNo = new ArrayList<Integer>();
		mArrayArrayList= new ArrayList<ArrayList<StudentReportMaster>>();
		
		mButtonDateStart = (Button) mView.findViewById(R.id.button_date_start);
		mButtonDateEnd = (Button) mView.findViewById(R.id.button_date_end);
		mButtonGo = (Button) mView.findViewById(R.id.button_go);
		mLinearLayoutRollNo = (LinearLayout) mView
				.findViewById(R.id.layout_date);
		mLinearLayoutAttend = (LinearLayout) mView
				.findViewById(R.id.layout_lecture);
		setEndDateonView();
		mButtonGo.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new GettingStudentReportApi().execute(studentid, mButtonDateStart.getText().toString()+" 00:00:00", mButtonDateEnd.getText().toString()+" 23:59:59");
				//ToastMsg.showSuccess(getActivity(), mButtonDateStart.getText().toString()+" 00:00:00"+ " AND "+mButtonDateEnd.getText().toString()+" 23:59:59", ToastMsg.GRAVITY_CENTER);
			}
		});
		mButtonDateEnd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				openDialogForDateEnd();
			}

		});
		mButtonDateStart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				openDialogForDateStart();
			}

		});
	
	}

	@SuppressLint("SimpleDateFormat")
	public void setEndDateonView() {

		Calendar c = Calendar.getInstance();
		yearEnd = c.get(Calendar.YEAR);
		monthEnd = c.get(Calendar.MONTH);
		dayEnd = c.get(Calendar.DAY_OF_MONTH);
		
		yearStart = c.get(Calendar.YEAR);
		monthStart = c.get(Calendar.MONTH);
		dayStart = c.get(Calendar.DAY_OF_MONTH);

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat month_date = new SimpleDateFormat("yyyy-MM-dd");

		String month_name = month_date.format(cal.getTime());
		mButtonDateEnd.setText(month_name);
		mButtonDateStart.setText(month_name);
	}

	public void openDialogForDateEnd() {
		mDatePickerDialogEndDate = new DatePickerDialog(getActivity(),
				datePickerEnd, yearEnd, monthEnd, dayEnd);
		mDatePickerDialogEndDate.show();
	}

	public void openDialogForDateStart() {
		mDatePickerDialogStartDate = new DatePickerDialog(getActivity(),
				datePickerStart, yearStart, monthStart, dayStart);
		mDatePickerDialogStartDate.show();
	}

	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID_END:

			return new DatePickerDialog(getActivity(), datePickerEnd, yearEnd,
					monthEnd, dayEnd);
		case DATE_DIALOG_ID_START:

			return new DatePickerDialog(getActivity(), datePickerStart,
					yearStart, monthStart, dayStart);
		}
		return null;
	}

	public void addListenerOnButton() {

		mButtonDateEnd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				openDialogForDateEnd();
			}

		});
		mButtonDateStart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				openDialogForDateStart();
			}

		});

	}
	
	private DatePickerDialog.OnDateSetListener datePickerStart = new DatePickerDialog.OnDateSetListener() {

		@Override
		@SuppressLint("SimpleDateFormat")
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			yearStart = selectedYear;
			monthStart = selectedMonth;
			dayStart = selectedDay;

			previousMonthDate = new StringBuilder().append(dayStart)
					.append("-").append(monthStart + 1).append("-")
					.append(yearStart).append(" ")
					+ "";
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			Date date = new Date();
			try {
				date = dateFormat.parse(previousMonthDate);
				lastCalender = Calendar.getInstance();
				lastCalender.setTime(date);
				SimpleDateFormat month_date = new SimpleDateFormat("yyyy-MM-dd");
				String month_name = month_date.format(lastCalender.getTime());
				mButtonDateStart.setText(month_name);
				startdate = month_name;
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};

	private DatePickerDialog.OnDateSetListener datePickerEnd = new DatePickerDialog.OnDateSetListener() {

		@Override
		@SuppressLint("SimpleDateFormat")
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			yearEnd = selectedYear;
			monthEnd = selectedMonth;
			dayEnd = selectedDay;

			previousMonthDate = new StringBuilder().append(yearEnd).append("-")
					.append(monthEnd + 1).append("-").append(dayEnd)
					.append(" ")
					+ "";
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			try {
				date = dateFormat.parse(previousMonthDate);
				lastCalender = Calendar.getInstance();
				lastCalender.setTime(date);
				SimpleDateFormat month_date = new SimpleDateFormat("yyyy-MM-dd");
				String month_name = month_date.format(lastCalender.getTime());
				mButtonDateEnd.setText(month_name);
				enddate = month_name;
				
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};

	class GettingStudentReportApi extends AsyncTask<String, String, JSONObject> {
		JSONObject jsonobject;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... transaction) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();
			try {
				List<NameValuePair> postParams = new ArrayList<NameValuePair>();

				postParams.add(new BasicNameValuePair("StudentId",transaction[0]));
				postParams.add(new BasicNameValuePair("StartTime",transaction[1]));
				postParams.add(new BasicNameValuePair("EndTime", transaction[2]));
				
			
					return parser.getJSONFromUrl(Global.SERVER_PATH
							+ "student_report.php", postParams);
				

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("report_test", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				JSONArray jat = result.getJSONArray("Data");
				
				int t=0;				
				mArrayList.clear();
				mArrayTime.clear();
				mArrayAll.clear();
				mArrayLectureNo.clear();
				mArrayArrayList.clear();
				
				if (jat.length() > 0) {
				
					StudentReportMaster obj;
					for (int i = 0; i < jat.length(); i++) {

						jsonobject = jat.getJSONObject(i);
						obj = new StudentReportMaster(
								jsonobject.getInt("AttendanceId"),
								jsonobject.getString("SubjectName"),
								jsonobject.getInt("LectureNo"),
								jsonobject.getString("Time").substring(0,10),
								jsonobject.getString("Attend"));
		
						boolean flag = true;
						for (int j = 0; j < mArrayTime.size(); j++) {
							if (obj.getTime().equals(mArrayTime.get(j).getTime())) 
							{
								flag = false;					
							}
						}
						if (flag == true) {
							if(t>0)
							{
								mArrayArrayList.add(mArrayList);
								mArrayList = new ArrayList<StudentReportMaster>();
							}
							mArrayLectureNo.add(obj.getLectureNo());
							mArrayTime.add(obj);
							t++;
						}
						mArrayList.add(obj);
						mArrayAll.add(obj);
				
					}
					mArrayArrayList.add(mArrayList);
					loadLayout();
					fillRollNo();
				
				} else {
					// Do Some thing when null
					mLinearLayoutAttend.removeAllViews();
					mLinearLayoutRollNo.removeAllViews();
					ToastMsg.showError(getActivity(), "No Attendance Taken",ToastMsg.GRAVITY_TOP);
					
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.e("Try catch", e.toString());

			}
			pd.dismiss();
			super.onPostExecute(result);
		}
	}
	public void loadLayout(){
		mLinearLayoutAttend.removeAllViews();
		LinearLayout linearLayout;
		
		int arrayListlength=0;
		for (int l = 0; l < mArrayAll.size(); l++) {
			
			if(arrayListlength>mArrayAll.get(l).getLectureNo())
			{
				
			}else{
				arrayListlength=mArrayAll.get(l).getLectureNo();
			}
		}
				
		for(int i=0;i<arrayListlength;i++)
		{
						
			View view = getActivity().getLayoutInflater().inflate(
					R.layout.custom_layout, null);	
			linearLayout = (LinearLayout) view.findViewById(R.id.custom_layout);
			linearLayout.getLayoutParams().width=90;
					
			for(int j=0;j<mArrayTime.size();j++)
			{
				Log.e("mArrayArrayList",mArrayArrayList.size()+"" );
				Log.e("mArrayArrayList.get(0)",mArrayArrayList.get(j).size()+"" );
				if(j==0){
					View viewDate = getActivity().getLayoutInflater().inflate(
							R.layout.attendance_list_date_wise, null);
					TextView textno = (TextView) viewDate.findViewById(R.id.text_s_eno);
					LinearLayout layoutLine = (LinearLayout) viewDate.findViewById(R.id.layout_line);
					TextView textname = (TextView) viewDate.findViewById(R.id.text_s_name);
					textname.setVisibility(View.GONE);
					layoutLine.setBackgroundResource(R.color.actionbar_color);
					textno.setTextSize(12);
					int p=i+1;
					textno.setText("Lecture:\n"+p);
					
					linearLayout.addView(viewDate);	
				}
				View view1 = getActivity().getLayoutInflater().inflate(
						R.layout.attendance_list_date_wise, null);
				final TextView textno = (TextView) view1.findViewById(R.id.text_s_eno);
				TextView textname = (TextView) view1.findViewById(R.id.text_s_name);
				textname.setVisibility(View.GONE);
				textno.setTextSize(22);
				int l=mArrayArrayList.get(j).size();
				int lectureno=i+1;
				mCheck=false;
				int value = 0;
				for(int k=0;k<mArrayArrayList.get(j).size();k++)
				{
					if(lectureno==mArrayArrayList.get(j).get(k).getLectureNo())
					{
						mCheck=true;
						value=k;
					}
				}
				if(mCheck)
				{
					if(mArrayArrayList.get(j).get(value).getAttend().equals("Present"))
					{
						textno.setText("P");
					}else if(mArrayArrayList.get(j).get(value).getAttend().equals("Absent"))
					{
						textno.setText("A");
					}
					
				}else{
					textno.setText("-");
				}
			/*	textno.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(mCheck)
						{
							Dialog dialog = new Dialog(getActivity());
							dialog.setContentView(R.layout.student_report_list_item);
							TextView txt = (TextView)dialog.findViewById(R.id.text_stude_date);
							txt.setText("manoj");
							dialog.show();
						}
					}
				});*/
		
				linearLayout.addView(view1);					
			}
			
			mLinearLayoutAttend.addView(view);
			
		}
		
		
	}
	
	public void fillRollNo() {	
		mLinearLayoutRollNo.removeAllViews();
		for (int i = 0; i < mArrayTime.size(); i++) {
			
			if(i==0)
			{
				View viewDate = getActivity().getLayoutInflater().inflate(
						R.layout.attendance_list_date_wise, null);
				TextView textno = (TextView) viewDate.findViewById(R.id.text_s_eno);
				LinearLayout layoutLine = (LinearLayout) viewDate.findViewById(R.id.layout_line);
				TextView textname = (TextView) viewDate.findViewById(R.id.text_s_name);
				textname.setVisibility(View.GONE);
				layoutLine.setBackgroundResource(R.color.actionbar_color);
				textno.setTextSize(12);
				textno.setText(" \n ");
				mLinearLayoutRollNo.addView(viewDate);	
			}
			
			View view = getActivity().getLayoutInflater().inflate(
					R.layout.attendance_list_date_wise, null);
			TextView textno = (TextView) view.findViewById(R.id.text_s_eno);
			TextView textname = (TextView) view.findViewById(R.id.text_s_name);
			textname.setVisibility(View.GONE);
			textno.setTextSize(22);
			textno.setText(mArrayTime.get(i).getTime().substring(5,10)+ "");			
			mLinearLayoutRollNo.addView(view);

		}	
		

	}

		
}
