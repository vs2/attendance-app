package com.vs2.easyacademics.fragments;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.SubjectMaster;
import com.vs2.easyacademics.objects.ToastMsg;
import com.vs2.easyacademics.objects.Utility;

public class FragmentCreateSubject extends SherlockFragment {

	
	private View mView;
	private ActionBar mActionbar;
	private Button mButtonSave;
	String action,classid,classname,subjectid,subjectname;
	private TextView mTextViewClassName,mTextViewSelectTeacher;
	private EditText mEdittextSubjectName;
	private LinearLayout mLayoutSubject;
	private SubjectMaster mSubjectMasterrList;
	private ArrayList<SubjectMaster> array_subjectmaster;
	private String mTeacherIds;
	private int i;	
	String[] spinnerListTeacherName,spinnerListTeacherId;
	Boolean[] flag;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_create_subject, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		mActionbar.setTitle("Subject");
		Bundle bundle = new Bundle();
		bundle = getArguments();
		action = bundle.getString("Action");
		classid = bundle.getString("Class ID");
		classname = bundle.getString("Class Name");
		initiGloble();
		return mView;
	}
	public void initiGloble() {
		Utility.strTeachersId = "";
		Utility.strTeachersName="";
		array_subjectmaster= new ArrayList<SubjectMaster>();
		mLayoutSubject=(LinearLayout)mView.findViewById(R.id.layout_subject);
		mButtonSave=(Button)mView.findViewById(R.id.button_save_subject);
		mTextViewClassName=(TextView)mView.findViewById(R.id.text_classname);
		mTextViewSelectTeacher=(TextView)mView.findViewById(R.id.textSelectTeacher);
		mEdittextSubjectName=(EditText)mView.findViewById(R.id.edittext_subject_name);
		mTextViewClassName.setText(classname);
		new GetingTeacherListApi().execute(Global.sUserMaster.getCollegeid()+"");

		if(action.equals("Update"))
		{
			Bundle bundle = new Bundle();
			bundle = getArguments();
			subjectid = bundle.getString("Subject ID");
			subjectname = bundle.getString("Subject Name");
			mButtonSave.setText("Update");
			mEdittextSubjectName.setText(subjectname);			
			new GetingSubjectDetailApi().execute(subjectid);
		}
		
		mButtonSave.setOnClickListener(new View.OnClickListener() {
	
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(action.equals("Update"))
				{
					//update the records in subject master and detail
					//update subjectname in subjectmaster
					Utility.strTeachersId=Utility.strTeachersId+Global.sUserMaster.getUserid()+",";
					new UpdateSubjectApi().execute(subjectid,classid,mEdittextSubjectName.getText().toString(),Utility.strTeachersId);
				}
				else
				{
					if(mEdittextSubjectName.getText().toString().equals(""))
					{
						ToastMsg.showError(getActivity(),
								"Please fill Subject Name", ToastMsg.GRAVITY_TOP);
					}
					else if(mTextViewSelectTeacher.getText().toString().equals("Select Teacher(s)"))					
					{
						ToastMsg.showError(getActivity(),
								"Please select Teacher(s)", ToastMsg.GRAVITY_TOP);
					}
					else					
					{	
						Utility.strTeachersId=Utility.strTeachersId+Global.sUserMaster.getUserid()+",";
						new CreateSubjectApi().execute(classid,mEdittextSubjectName.getText().toString(),Utility.strTeachersId);	
				}
				}
			}
		});
		mTextViewSelectTeacher.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showTeacherDialog();
			}
		});
	}
	
	private void fillTeacherName()
	{
		Utility.strTeachersId="";
		Utility.strTeachersName="";
		for(int i=0;i<array_subjectmaster.size();i++)
		{
			if(array_subjectmaster.get(i).getUserId()!=Global.sUserMaster.getUserid())
			{
				Utility.AddTeacherId(Utility.strTeachersId,String.valueOf(array_subjectmaster.get(i).getUserId()));
				for(int j=0;j<spinnerListTeacherId.length;j++)
				{
					if(spinnerListTeacherId[j].equals(String.valueOf(array_subjectmaster.get(i).getUserId())))
					{
						Utility.AddTeacherName(Utility.strTeachersName,spinnerListTeacherName[j]);
					}
				}
			}
		}
		if(Utility.strTeachersName!="")
		{
			mTextViewSelectTeacher.setText(Utility.strTeachersName);
		}
		else
		{
			mTextViewSelectTeacher.setText("Select Teacher(s)");
		}
	}
	
	class GetingSubjectDetailApi extends AsyncTask<String, String, JSONObject> {
		JSONObject jsonobject;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... transaction) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();
			try {
				List<NameValuePair> postParams = new ArrayList<NameValuePair>();
				postParams.add(new BasicNameValuePair("SubjectId",transaction[0]));					
					return parser.getJSONFromUrl(Global.SERVER_PATH
							+ "subject_list_master_details.php",
							postParams);
				
			} catch (Exception e) {
				Log.e("Subject List", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject resultTransaction) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				JSONArray jat = resultTransaction.getJSONArray("Data");
				if (resultTransaction != null) {
					if(jat.length()>0){
						for(int i = 0; i < jat.length(); i++)
			   			{
							jsonobject=jat.getJSONObject(i);
							mSubjectMasterrList=new SubjectMaster(Integer.parseInt(jsonobject.getString("UserId")));
							array_subjectmaster.add(mSubjectMasterrList);
			   			}					
					}
					Log.e("nims",jat.length()+":"+array_subjectmaster.size());
					fillTeacherName();
					
				} 
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			super.onPostExecute(resultTransaction);
		}
	}
	
	//start update subject
	
	public class UpdateSubjectApi extends AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("please wait....");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();

		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("SubjectId", params[0]));
			postParams.add(new BasicNameValuePair("ClassId", params[1]));
			postParams.add(new BasicNameValuePair("SubjectName", params[2]));
			postParams.add(new BasicNameValuePair("TeacherIds", params[3]));

			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "subject_update_and_assign_teachers.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("UpdateSubjectApi", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("JSON", result + "");
				if (result != null) {
					processUpdateSubject(result);
				} else {
					Log.e("Update Subject",
							"Server encountered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processUpdateSubject(JSONObject result) {

		if (result.has("Result") && result.has("Message")) {
			try {
				if (result.getInt("Result") == 1) {
					ToastMsg.showSuccess(getActivity(),
							"Subject Successfully Updated", ToastMsg.GRAVITY_TOP);
					
					FragmentManager fragmentManager = getActivity()
							.getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					FragmentSubjectList fragmentSubjectList=new FragmentSubjectList();
					Bundle bundle=new Bundle();
					bundle.putString("Action", action);
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					fragmentSubjectList.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentSubjectList).commit();
					
				} else {
					ToastMsg.showError(getActivity(),
							"Subject Updation Error", ToastMsg.GRAVITY_TOP);

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("processUpdateSubject", "processUpdateSubject error");
			}
		} else {
			Toast.makeText(getActivity(), "Unable to Update a Subject",
					Toast.LENGTH_LONG).show();
		}
	}
	//end update subject

	public class CreateSubjectApi extends AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("please wait....");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();

		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("ClassId", params[0]));
			postParams.add(new BasicNameValuePair("SubjectName", params[1]));
			postParams.add(new BasicNameValuePair("TeacherIds", params[2]));

			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "subject_create_and_assign_teachers.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("CreateSubjectApi", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("JSON", result + "");
				if (result != null) {
					processCreateSubject(result);
				} else {
					Log.e("Create Subject",
							"Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processCreateSubject(JSONObject result) {

		if (result.has("Result") && result.has("Message")) {
			try {
				if (result.getInt("Result") == 1) {
					ToastMsg.showSuccess(getActivity(),
							"Subject Successfully Added", ToastMsg.GRAVITY_TOP);
					
					FragmentManager fragmentManager = getActivity()
							.getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					FragmentAdminAction fragmentAdminAction=new FragmentAdminAction();
					Bundle bundle=new Bundle();
					bundle.putString("Action", action);
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					fragmentAdminAction.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminAction).commit();
					
				} else {
					ToastMsg.showError(getActivity(),
							"Subject Creation Error", ToastMsg.GRAVITY_TOP);

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("processCreateSubject", "processCreateSubject error");
			}
		} else {
			Toast.makeText(getActivity(), "Unable to Create a Subject",
					Toast.LENGTH_LONG).show();
		}
	}
	
	class GetingTeacherListApi extends AsyncTask<String, String, JSONObject> {
		JSONObject jsonobject;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... transaction) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();
			try {
				List<NameValuePair> postParams = new ArrayList<NameValuePair>();
				postParams.add(new BasicNameValuePair("CollegeId",transaction[0]));
								
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "teacher_list.php",
						postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("Teacher List", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				Log.e("RESULT",result+"");
				if (result != null) {
					JSONArray jat = result.getJSONArray("Data");
					spinnerListTeacherId = new String[jat.length()];
					spinnerListTeacherName = new String[jat.length()];
					flag = new Boolean[jat.length()];
					for(int i = 0; i < jat.length(); i++)
		   			{
						jsonobject=jat.getJSONObject(i);
						spinnerListTeacherName[i]=jsonobject.getString("UserName");
						spinnerListTeacherId[i]=jsonobject.getInt("Id")+"";
						flag[i]=false;
		   			} 
					
					/*ArrayAdapter<String> AdapterName = new ArrayAdapter<String>(
							getActivity(), R.layout.spinner_item, spinnerListTeacherName);
					AdapterName
							.setDropDownViewResource(android.R.layout.simple_list_item_1);
					mSpinnerTeacher.setAdapter(AdapterName);
					AdapterName.notifyDataSetChanged();
					*/
				
				}else{
					ToastMsg.showError(getActivity(),
							"No Data Found", ToastMsg.GRAVITY_TOP);
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			super.onPostExecute(result);
		}
	}
	public void showTeacherDialog(){
		LayoutInflater adbInflater = LayoutInflater.from(getActivity());
		View TeacherView = adbInflater.inflate(R.layout.teacher_layout, null);
		LinearLayout teacherLayout=(LinearLayout)TeacherView.findViewById(R.id.teacher_layout);
		teacherLayout.removeAllViews();
		//need to set view items
				if(mTextViewSelectTeacher.getText().equals("Select Teacher(s)"))
		{
			Utility.strTeachersId="";
			Utility.strTeachersName="";
		}
		else
		{
			Utility.strTeachersName=mTextViewSelectTeacher.getText().toString();	
		}	
		for(i=0;i<spinnerListTeacherName.length;i++)
		{
			LayoutInflater listInflater = LayoutInflater.from(getActivity());
			View TeacherListView = listInflater.inflate(R.layout.teacher_inner_layout, null);
			final TextView textTeacherName = (TextView) TeacherListView.findViewById(R.id.text_teacher_name);
			final TextView textTeacherId = (TextView) TeacherListView.findViewById(R.id.text_teacher_id);
		
			CheckBox checkBox=(CheckBox)TeacherListView.findViewById(R.id.checkbox_teacher);
			textTeacherName.setText(spinnerListTeacherName[i]);
			textTeacherId.setText(spinnerListTeacherId[i]);
			if(Utility.strTeachersName.contains(spinnerListTeacherName[i]))			
			{
				checkBox.setChecked(true);
				
			}
			//Log.e("","text: "+textTeacherflag.getText().toString());
			teacherLayout.addView(TeacherListView);
			checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					// TODO Auto-generated method stub
					if(isChecked)
					{
						Utility.AddTeacherId(Utility.strTeachersId,								
								textTeacherId.getText().toString());
						Utility.AddTeacherName(Utility.strTeachersName,
								textTeacherName.getText().toString());
					}else{
						Utility.RemoveTeacherId(Utility.strTeachersId,
								textTeacherId.getText().toString());
						Utility.RemoveTeacherName(Utility.strTeachersName,
								textTeacherName.getText().toString());
					}
				}
			});
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Teachers");
		builder.setView(TeacherView);
		
		builder.setPositiveButton("Done",new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				if(Utility.strTeachersName=="")
				{
					mTextViewSelectTeacher.setText("Select Teacher(s)");
				}
				else
				{
					mTextViewSelectTeacher.setText(Utility.strTeachersName);
				}
			}
		});
	
		builder.show();	
	}
	
}
