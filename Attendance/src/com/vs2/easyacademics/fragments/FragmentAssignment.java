package com.vs2.easyacademics.fragments;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.fragments.FragmentAddStudents.StudentEntryApi;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.ToastMsg;

public class FragmentAssignment extends SherlockFragment {

	private View mView;
	private ActionBar mActionbar;
	private  Button mButtonSave,mButtonDate;
	private EditText mEditTextAssignmentName;
	String subjectid,action,classid,classname,subjectname;
	
	static final int DATE_DIALOG_ID = 999;
	private DatePickerDialog mDatePickerDialog;
	private int  mYear,mMonth,mDay;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_assignment, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		mActionbar.setTitle("Assignment");
		
		Bundle bundle = new Bundle();
		bundle = getArguments();
		subjectid = bundle.getString("Subject ID");
		subjectname = bundle.getString("Subject Name");
		action = bundle.getString("Action");
	//	if(action.equals("Assignments")){
			classid = bundle.getString("Class ID");
			classname = bundle.getString("Class Name");
			Log.e("Class name","CName: "+classname);
		//}
		
		initiGloble();
		return mView;
	}
public void initiGloble(){
		
		mButtonSave=(Button)mView.findViewById(R.id.button_save_assignment);
		mButtonDate=(Button)mView.findViewById(R.id.button_submition_date);
		mEditTextAssignmentName=(EditText)mView.findViewById(R.id.edittext_assignment_name);
		
		Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
		mButtonDate.setOnClickListener(new View.OnClickListener() {
		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				openDialog();
			}
		});
		mButtonSave.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mButtonDate.getText().toString().equals("Select Submition Date")){
					ToastMsg.showError(getActivity(), "Please Select Submition Date",ToastMsg.GRAVITY_TOP);
					
				}else if(mEditTextAssignmentName.getText().toString().equals("")){
					ToastMsg.showError(getActivity(), "Please Enter Assignment Name",ToastMsg.GRAVITY_TOP);
					
				}else{
					new CreatingAssignmentApi().execute(subjectid,mEditTextAssignmentName.getText().toString(),mButtonDate.getText().toString());
				}
				
			}
		});
	}

public void openDialog() {
	mDatePickerDialog = new DatePickerDialog(getActivity(),
			datePicker, mYear, mMonth, mDay);
	mDatePickerDialog.show();
}
private DatePickerDialog.OnDateSetListener datePicker = new DatePickerDialog.OnDateSetListener() {

	@Override
	@SuppressLint("SimpleDateFormat")
	public void onDateSet(DatePicker view, int selectedYear,
			int selectedMonth, int selectedDay) {
		mYear = selectedYear;
		mMonth = selectedMonth;
		mDay = selectedDay;

		String dateValue = new StringBuilder().append(mDay)
				.append("-").append(mMonth + 1).append("-")
				.append(mYear).append(" ")
				+ "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		try {
			date = dateFormat.parse(dateValue);
			Calendar lastCalender = Calendar.getInstance();
			lastCalender.setTime(date);
			SimpleDateFormat month_date = new SimpleDateFormat("yyyy-MM-dd");
			String month_name = month_date.format(lastCalender.getTime());
			mButtonDate.setText(month_name);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
};

public class CreatingAssignmentApi extends AsyncTask<String, Integer, JSONObject> {

	ProgressDialog pd;

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		pd = new ProgressDialog(getActivity());
		pd.setMessage("Please wait....");
		pd.setIndeterminate(false);
		pd.setCancelable(false);
		pd.show();

	}

	@Override
	protected JSONObject doInBackground(String... params) {
		// TODO Auto-generated method stub

		JSONParser parser = new JSONParser();
		List<NameValuePair> postParams = new ArrayList<NameValuePair>();
		postParams.add(new BasicNameValuePair("SubjectId", params[0]));
		postParams.add(new BasicNameValuePair("AssignmentName", params[1]));
		postParams.add(new BasicNameValuePair("SubmitionDate", params[2]));

		try {

			return parser.getJSONFromUrl(Global.SERVER_PATH
					+ "assignment_master_create.php", postParams);

		} catch (Exception e) {
			// TODO: handle exception
			Log.e("CreatingAssignmentApi", "Error : " + e.toString());
			return null;
		}
	}

	@Override
	protected void onPostExecute(JSONObject result) {
		// TODO Auto-generated method stub
		try {
			pd.dismiss();
			Log.e("JSON", result + "");
			if (result != null) {
				processAddingAssignment(result);
			} else {
				Log.e("CreatingAssignmentApi",
						"Server encontered problem try again later");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.onPostExecute(result);
	}

}

private void processAddingAssignment(JSONObject result) {

	if (result.has("Status") && result.has("Message")) {
		try {
			String message = result.getString("Message");
			if (result.getInt("Status") == 1) {
				ToastMsg.showSuccess(getActivity(),
						"Assignment Added Successfully", ToastMsg.GRAVITY_TOP);
				
				FragmentManager fragmentManager = getActivity()
						.getSupportFragmentManager();
				final FragmentTransaction fragmentTransaction = fragmentManager
						.beginTransaction();
				Bundle bundle=new Bundle();
				if(action.equals("Assignments")){
					FragmentClassProfile fragmentClassProfile=new FragmentClassProfile();
					bundle.putString("Subject ID", subjectid);
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					bundle.putString("Subject Name", subjectname);
					bundle.putString("Action", "Assignments");
					fragmentClassProfile.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentClassProfile).commit();
				}else{
					FragmentAdminAction fragmentAdminAction=new FragmentAdminAction();
					bundle.putString("Action", "AssignmentsAction");
					bundle.putString("Class ID", classid);
					bundle.putString("Subject ID", subjectid);
					bundle.putString("Class Name", classname);
					bundle.putString("Subject Name", subjectname);
					fragmentAdminAction.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminAction).commit();
				}
				
						    		
			} else {
				// Toast.makeText(getActivity(),message,
				// Toast.LENGTH_LONG).show();
				ToastMsg.showError(getActivity(), message,
						ToastMsg.GRAVITY_TOP);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e("CreatingAssignmentApi", "CreatingAssignmentApi error");
		}
	} else {
		Toast.makeText(getActivity(), "Unable to CreatingAssignmentApi",
				Toast.LENGTH_LONG).show();
	}
}
}
