package com.vs2.easyacademics.fragments;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.HomeActivity;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.RegistrationActivity;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.PreferenceUser;
import com.vs2.easyacademics.objects.ToastMsg;
import com.vs2.easyacademics.objects.UserMaster;

public class FragmentSetting extends SherlockFragment {

	private View mView;
	private ActionBar mActionbar;

	private EditText mEditTextCurrPapss, mEditTextNewPass, mEditTextRePass;
	private Button mButtonSaveChange;
	private String action,back;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_setting, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		
		mActionbar.setTitle("Settings");
		Bundle bundle=new Bundle();
		bundle = getArguments();
		action = bundle.getString("Action");
		Log.e("ACTION ",""+action);
		initGloble();
		return mView;
	}

	public void initGloble() {
		
		mEditTextCurrPapss=(EditText)mView.findViewById(R.id.edittext_current_pass);
		mEditTextNewPass=(EditText)mView.findViewById(R.id.edittext_new_pass);
		mEditTextRePass=(EditText)mView.findViewById(R.id.edittext_re_pass);
		mButtonSaveChange=(Button)mView.findViewById(R.id.button_savechange);
		
		if(action.equals("Edit Information")){
			
			mEditTextCurrPapss.setInputType(InputType.TYPE_CLASS_TEXT);
			mEditTextNewPass.setInputType(InputType.TYPE_CLASS_TEXT);
			mEditTextRePass.setInputType(InputType.TYPE_CLASS_TEXT);
			Log.e("UserValue",""+Global.sUserMaster.getUsername()+" "+Global.sUserMaster.getEmail()+" "+Global.sUserMaster.getPhoneno());
			mEditTextCurrPapss.setText(Global.sUserMaster.getUsername());
			mEditTextNewPass.setText(Global.sUserMaster.getEmail());
			mEditTextRePass.setText(Global.sUserMaster.getPhoneno());
			
		}else if(action.equals("Change Password")){
			Bundle bundle=new Bundle();
			bundle = getArguments();
			back = bundle.getString("Back");
		}
		
		mButtonSaveChange.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(action.equals("Edit Information")){
					
					new ChangeInformationApi().execute(Global.sUserMaster.getUserid()+"",mEditTextCurrPapss.getText().toString(),mEditTextNewPass.getText().toString(),mEditTextRePass.getText().toString());
					
				}else if(action.equals("Change Password")){
					if(notBlank(mEditTextCurrPapss) && notBlank(mEditTextNewPass)
							&& notBlank(mEditTextRePass)){
						if(mEditTextNewPass.getText().toString().equals(mEditTextRePass.getText().toString())){
							new ChangePassApi().execute(Global.sUserMaster.getUserid()+"",mEditTextNewPass.getText().toString(),mEditTextCurrPapss.getText().toString());
						}else{
							ToastMsg.showError(getActivity(),
									"Password not Match", ToastMsg.GRAVITY_TOP);
						}
						
					}
				}
			}
		});
		
		mEditTextRePass.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
				  if (!hasFocus) {
					  if(mEditTextNewPass.getText().toString().equals(mEditTextRePass.getText().toString())){
						
						}else{
							ToastMsg.showError(getActivity(),
									"Password not Match", ToastMsg.GRAVITY_TOP);
						}
				      }
				
			}
		});
	}
	private boolean notBlank(EditText edit){
		if(edit.getText().length() > 0){
			edit.setError(null);
			return true;
		}else{
			edit.setError("Required " + edit.getHint());
			return false;
		}
	}
	
	public class ChangePassApi extends AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("Id", params[0]));
			postParams.add(new BasicNameValuePair("NewPassword", params[1]));
			postParams.add(new BasicNameValuePair("CurrentPassword", params[2]));
			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "change_password.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("Change Password", "Error : " + e.toString());
				return null;
			}
		}
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				if (result != null) {
					processChange(result);
				} else {
					Log.e( "Change Password",
							"Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processChange(JSONObject result) {
		try {
			String message = result.getString("Message");
			if (result.getInt("Status") == 1) {
				ToastMsg.showSuccess(getActivity(), "Password Change Successfully.", ToastMsg.GRAVITY_TOP);
				PreferenceUser.clearPreference(getActivity());	
				Global.sUserMaster = UserMaster.getUserMaster(result.getJSONObject("Data"));										
				UserMaster.saveUserMaster(Global.sUserMaster,getActivity());
				
				if(back.equals("Option")){
					FragmentManager fragmentManager = getActivity()
							.getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					fragmentTransaction.replace(R.id.frame_layout, new FragmentOptions()).commit();
				}else{
					
					Intent i=new Intent(getActivity(), HomeActivity.class);
					startActivity(i);
				}
				
				
			} else {
				ToastMsg.showError(getActivity(),
						message, ToastMsg.GRAVITY_TOP);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			ToastMsg.showError(getActivity(),
					"Change error"+e.toString(), ToastMsg.GRAVITY_TOP);
			Log.e("Change", "Change error"+e.toString());
		}
	}
	public class ChangeInformationApi extends AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("UserId", params[0]));
			postParams.add(new BasicNameValuePair("UserName", params[1]));
			postParams.add(new BasicNameValuePair("Email", params[2]));
			postParams.add(new BasicNameValuePair("PhoneNo", params[3]));
			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "user_details_update.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("Change Information", "Error : " + e.toString());
				return null;
			}
		}
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				if (result != null) {
					processChangeInformation(result);
				} else {
					Log.e( "Change Information",
							"Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processChangeInformation(JSONObject result) {
		try {
			String message = result.getString("Message");
			if (result.getInt("Status") == 1) {
				ToastMsg.showSuccess(getActivity(), "Information Change Successfully.", ToastMsg.GRAVITY_TOP);
				PreferenceUser.clearPreference(getActivity());	
				Global.sUserMaster = UserMaster.getUserMaster(result.getJSONObject("Data"));										
				UserMaster.saveUserMaster(Global.sUserMaster,getActivity());
				
				
			} else {
				ToastMsg.showError(getActivity(),
						message, ToastMsg.GRAVITY_TOP);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e("Change", "Change error"+e.toString());
		}
	}
}
