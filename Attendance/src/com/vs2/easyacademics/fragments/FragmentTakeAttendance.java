package com.vs2.easyacademics.fragments;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.HomeActivity;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.adapters.StudentListGridViewAdapter;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.StudentMaster;
import com.vs2.easyacademics.objects.SubjectMaster;
import com.vs2.easyacademics.objects.ToastMsg;
import com.vs2.easyacademics.objects.Utility;

public class FragmentTakeAttendance extends SherlockFragment {

	private View mView;
	private ActionBar mActionbar;
	ArrayList<SubjectMaster> array_subjectmaster;
	ArrayList<StudentMaster> array_studentmaster;
	String action, classid, classname, attend, subjectid, subjectname, attendA,
			attendP, formetDate;
	public static String SID;
	String[] SubjectList, StudentList, StudentID;
	private Button mButtonSave;
	private TextView mButtonDateChoose;
	StudentListGridViewAdapter mStudentListAdapter;
	public static String[] StudentAttendId;
	public static String[][] Attendance;
	public static String SubjectId;
	private StudentMaster mStudentMaster;
	Spinner mSpinnerPreAb, mSpinnerLecture;
	String[] spinnerList, spinnerListLectureNo;

	GridView gridView;
	public static Boolean flag = true;
	public static int color, AttendanceId;
	private String LastStudId, AssignmentId, TodayDate;

	static final int DATE_DIALOG_ID = 999;
	private DatePickerDialog mDatePickerDialog;
	private int mYear, mMonth, mDay;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_take_attendance, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		Bundle bundle = new Bundle();
		bundle = getArguments();
		classid = bundle.getString("Class ID");
		classname = bundle.getString("Class Name");
		subjectid = bundle.getString("Subject ID");
		subjectname = bundle.getString("Subject Name");
		action = bundle.getString("Action");
		mActionbar.setTitle("Attendance");
		initGloble();
		removeAddView();
		return mView;
	}

	public void removeAddView() {
		WindowManager wm = (WindowManager) getActivity().getSystemService(
				Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		int width = display.getWidth(); // deprecated
		int height = display.getHeight();

		if (height < 500) {
			HomeActivity.mRLayoutAdd.setVisibility(View.GONE);
		} else {
			int px=(int) (50 * getActivity().getResources().getDisplayMetrics().density);
			LayoutParams params = HomeActivity.mDrawerLayout.getLayoutParams();
			((MarginLayoutParams) params).setMargins(0, 0, 0, px); // left, top,
																	// right,
																	// bottom
			HomeActivity.mDrawerLayout.setLayoutParams(params);
		}
		Log.e("display", "width: " + width + " height: " + height);

	}

	public void initGloble() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat ds = new SimpleDateFormat("yyyy-MM-dd");
		TodayDate = ds.format(c.getTime());
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

		gridView = (GridView) mView.findViewById(R.id.gridView1);
		mButtonDateChoose = (TextView) mView
				.findViewById(R.id.text_choose_date);
		mButtonSave = (Button) mView.findViewById(R.id.button_save);
		mSpinnerPreAb = (Spinner) mView.findViewById(R.id.spinner_pre_ab);
		mSpinnerLecture = (Spinner) mView.findViewById(R.id.spinner_lecture);
		mButtonDateChoose.setText(TodayDate);
		array_studentmaster = new ArrayList<StudentMaster>();
		array_subjectmaster = new ArrayList<SubjectMaster>();
		Utility.strRollNumbers = "";
		Utility.strNotAttendRollNumbers = "";
		mStudentMaster = new StudentMaster();
		spinnerList = new String[2];
		spinnerList[0] = "Absents";
		spinnerList[1] = "Presents";
		spinnerListLectureNo = new String[11];
		spinnerListLectureNo[0] = "Select Lecture No";
		spinnerListLectureNo[1] = "Lecture No: 1";
		spinnerListLectureNo[2] = "Lecture No: 2";
		spinnerListLectureNo[3] = "Lecture No: 3";
		spinnerListLectureNo[4] = "Lecture No: 4";
		spinnerListLectureNo[5] = "Lecture No: 5";
		spinnerListLectureNo[6] = "Lecture No: 6";
		spinnerListLectureNo[7] = "Lecture No: 7";
		spinnerListLectureNo[8] = "Lecture No: 8";
		spinnerListLectureNo[9] = "Lecture No: 9";
		spinnerListLectureNo[10] = "Lecture No: 10";

		ArrayAdapter<String> categoriesAdapter = new ArrayAdapter<String>(
				getActivity(), R.layout.spinner_item, spinnerList);
		categoriesAdapter
				.setDropDownViewResource(android.R.layout.simple_list_item_1);
		mSpinnerPreAb.setAdapter(categoriesAdapter);

		ArrayAdapter<String> spinnerLectureAdapter = new ArrayAdapter<String>(
				getActivity(), R.layout.spinner_item, spinnerListLectureNo);
		spinnerLectureAdapter
				.setDropDownViewResource(android.R.layout.simple_list_item_1);
		mSpinnerLecture.setAdapter(spinnerLectureAdapter);

		color = R.color.absent;
		mStudentListAdapter = new StudentListGridViewAdapter(getActivity(),
				array_studentmaster);
		new GetingStudentListApi().execute(classid);

		if (action.equals("Take Assignments")) {
			mSpinnerPreAb.setVisibility(View.GONE);
			mSpinnerLecture.setVisibility(View.GONE);
			color = R.color.absent;
			Bundle bundle = new Bundle();
			bundle = getArguments();
			AssignmentId = bundle.getString("Assignment ID");
			Log.e("AssignmentId", "AssignmentId: " + AssignmentId);
		}
		mButtonDateChoose.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				openDialog();
			}
		});
		mButtonSave.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (Utility.isOnline(getActivity())) {
					Calendar c = Calendar.getInstance();
					SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
					formetDate = df.format(c.getTime());
					Log.e("", "Time: " + formetDate);

					attend = mSpinnerPreAb.getSelectedItem() + "";
					if (attend.equals("Present")) {
						attendP = "Present";
						attendA = "Absent";
					} else {
						attendP = "Absent";
						attendA = "Present";
					}

					if (action.equals("Take Assignments")) {

						// takingAssignments();

						if (Utility.strRollNumbers.equals("")) {
							ToastMsg.showError(getActivity(),
									"No Student Selected", ToastMsg.GRAVITY_TOP);
						} else {
							new TakingAssignmnetApi().execute(
									AssignmentId + "", Utility.strRollNumbers);
						}

					} else {
						if (Utility.strRollNumbers.equals("")) {
							ToastMsg.showError(getActivity(),
									"No Student Selected", ToastMsg.GRAVITY_TOP);
						} else if (mSpinnerLecture.getSelectedItem().toString()
								.equals("Select Lecture No")) {
							ToastMsg.showError(getActivity(),
									"Please Select Lecture No",
									ToastMsg.GRAVITY_TOP);
						} else {

							new CraeteAttendanceApi().execute(subjectid,
									mSpinnerLecture.getSelectedItemId() + "",
									mButtonDateChoose.getText().toString()
											+ " " + formetDate,
									Utility.strRollNumbers, attendP,
									mButtonDateChoose.getText().toString(),
									classid);
						}
					}
				} else {
					Log.e("No connection", "No internet connection found!");
					ToastMsg.showError(getActivity(),
							"No internet connection found!",
							ToastMsg.GRAVITY_CENTER);
				}

			}

		});
		mSpinnerPreAb.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if (mSpinnerPreAb.getSelectedItem().equals("Presents")) {

					color = R.color.absent;
				} else {
					color = R.color.present;
				}
				mStudentListAdapter.notifyDataSetChanged();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		gridView.setOnItemClickListener(new OnItemClickListener() {

			@SuppressLint("ResourceAsColor")
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub

				TextView textRollNo = (TextView) arg1
						.findViewById(R.id.text_identifier);
				if (array_studentmaster.get(arg2).getSelection()
						.equals("false")) {
					mStudentListAdapter.notifyDataSetChanged();
					array_studentmaster.get(arg2).setSelection("true");
					mStudentListAdapter.notifyDataSetChanged();
					Utility.AddRollNoInAttendance(Utility.strRollNumbers,
							textRollNo.getText().toString());

					/*
					 * ToastMsg.showError(getActivity(), Utility.strRollNumbers,
					 * ToastMsg.GRAVITY_TOP);
					 */

					Utility.RemoveRollNoInNotAttendAttendance(
							Utility.strNotAttendRollNumbers, textRollNo
									.getText().toString());

				} else {

					mStudentListAdapter.notifyDataSetChanged();
					array_studentmaster.get(arg2).setSelection("false");
					mStudentListAdapter.notifyDataSetChanged();
					Utility.RemoveRollNoInAttendance(Utility.strRollNumbers,
							textRollNo.getText().toString());
					Utility.AddRollNoInNotAttendAttendance(
							Utility.strNotAttendRollNumbers, textRollNo
									.getText().toString());
				}

			}
		});

	}

	public void openDialog() {
		mDatePickerDialog = new DatePickerDialog(getActivity(), datePicker,
				mYear, mMonth, mDay);
		mDatePickerDialog.show();
	}

	private DatePickerDialog.OnDateSetListener datePicker = new DatePickerDialog.OnDateSetListener() {

		@Override
		@SuppressLint("SimpleDateFormat")
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			mYear = selectedYear;
			mMonth = selectedMonth;
			mDay = selectedDay;

			String dateValue = new StringBuilder().append(mDay).append("-")
					.append(mMonth + 1).append("-").append(mYear).append(" ")
					+ "";
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			Date date = new Date();
			try {
				date = dateFormat.parse(dateValue);
				Calendar lastCalender = Calendar.getInstance();
				lastCalender.setTime(date);
				SimpleDateFormat month_date = new SimpleDateFormat("yyyy-MM-dd");
				String month_name = month_date.format(lastCalender.getTime());
				mButtonDateChoose.setText(month_name);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};

	@SuppressLint("NewApi")
	public void takingAssignments() {

		SparseBooleanArray checked = gridView.getCheckedItemPositions();
		int numChecked = checked.size();
		int size = array_studentmaster.size();
		int sj = size - numChecked;
		numChecked = numChecked + sj;
		LastStudId = array_studentmaster.get(numChecked - 1).getStudid() + "";
		for (int i = 0; numChecked > i; i++) {
			if (checked.get(i)) {
				Log.e("", "true get: " + checked.indexOfKey(i));
				Log.e("", "Student: " + array_studentmaster.get(i).getStudid()
						+ "  :" + attendP);

				new TakingAssignmnetApi().execute(AssignmentId + "",
						array_studentmaster.get(i).getStudid() + "");

			} else {

				Log.e("", "false get: " + checked.indexOfKey(i) + " : "
						+ attendA);
			}

		}

	}

	public class TakingAssignmnetApi extends
			AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("AssignmentId", params[0]));
			postParams.add(new BasicNameValuePair("StrRollNo", params[1]));

			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "assignment_details_create.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("CraeteAttendanceMasterApi", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("JSON", result + "");
				if (result != null) {
					processAssignment(result);
				} else {
					Log.e("CraeteAttendanceMasterApi",
							"Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processAssignment(JSONObject result) {

		if (result.has("Status") && result.has("Message")) {
			try {
				String message = result.getString("Message");
				if (result.getInt("Status") == 1) {

					ToastMsg.showSuccess(getActivity(), message,
							ToastMsg.GRAVITY_TOP);

				} else {

					ToastMsg.showError(getActivity(), message,
							ToastMsg.GRAVITY_TOP);
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("Assignment", "Assignment error");
			}
		} else {

			ToastMsg.showError(getActivity(), "Unable to Take Assignment",
					ToastMsg.GRAVITY_TOP);
		}
	}

	@SuppressLint("NewApi")
	public void createAttendance(int Aid) {

		SparseBooleanArray checked = gridView.getCheckedItemPositions();
		int numChecked = checked.size();
		int size = array_studentmaster.size();
		int sj = size - numChecked;
		numChecked = numChecked + sj;
		LastStudId = array_studentmaster.get(numChecked - 1).getStudid() + "";
		Log.e("LastStudId", "LastStudId: " + LastStudId);
		Log.e("Aid", "Aid: " + Aid);
		Utility.strNotAttendRollNumbers = "";
		try {
			for (int i = 0; numChecked > i; i++) {

				if (checked.get(i)) {

				} else {

					Utility.AddRollNoInNotAttendAttendance(
							Utility.strNotAttendRollNumbers,
							array_studentmaster.get(i).getStudid() + "");
					Log.e("", " me " + array_studentmaster.get(i).getStudid()
							+ "");

				}

			}

			new CraeteAttendanceDetailsApi().execute(subjectid, formetDate,
					Utility.strNotAttendRollNumbers, attendA, Aid + "");
			Log.e(" Attend", "  attend: " + Utility.strRollNumbers);
			Log.e("Not Attend", " not attend: "
					+ Utility.strNotAttendRollNumbers);

		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Catch", "Catch Error: " + e.toString());
		}

	}

	public class CraeteAttendanceApi extends
			AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();

		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("SubjectId", params[0]));
			postParams.add(new BasicNameValuePair("LectureNo", params[1]));
			postParams.add(new BasicNameValuePair("Time", params[2]));
			postParams.add(new BasicNameValuePair("StrRollNo", params[3]));
			postParams.add(new BasicNameValuePair("Attend", params[4]));
			postParams.add(new BasicNameValuePair("StartTime", params[5]));
			postParams.add(new BasicNameValuePair("ClassId", params[6]));

			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "attendance_creator.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("CraeteAttendanceDetailsApi", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("JSON", result + "");
				if (result != null) {
					processTakingAttendance(result);
				} else {
					Log.e("CraeteAttendanceDetailsApi",
							"Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processTakingAttendance(JSONObject result) {

		if (result.has("Status") && result.has("Message")) {
			try {
				String message = result.getString("Message");
				if (result.getInt("Status") == 1) {

					/*
					 * ToastMsg.showSuccess(getActivity(), message,
					 * ToastMsg.GRAVITY_TOP);
					 */
					AttendanceId = result.getJSONObject("Data").getInt(
							"AttendanceId");
					Log.e("AttendanceId", "AttendanceId: " + AttendanceId);
					new CraeteAttendanceDetailsApi().execute(subjectid,
							formetDate, Utility.strNotAttendRollNumbers,
							attendA, AttendanceId + "");
					// createAttendance(AttendanceId);
				} else {

					ToastMsg.showError(getActivity(), message,
							ToastMsg.GRAVITY_TOP);
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("AttendanceDetails", "AttendanceDetails error");
			}
		} else {
			Toast.makeText(getActivity(), "Unable to Take Attendance",
					Toast.LENGTH_LONG).show();
		}

	}

	public class CraeteAttendanceDetailsApi extends
			AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("SubjectId", params[0]));
			postParams.add(new BasicNameValuePair("Time", params[1]));
			postParams.add(new BasicNameValuePair("StrRollNo", params[2]));
			postParams.add(new BasicNameValuePair("Attend", params[3]));
			postParams.add(new BasicNameValuePair("AttendanceId", params[4]));

			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "attendance_details.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("CraeteAttendanceDetailsApi", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("JSON", result + "");
				if (result != null) {
					processTakingAttendanceDetails(result);
				} else {
					Log.e("CraeteAttendanceDetailsApi",
							"Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processTakingAttendanceDetails(JSONObject result) {

		if (result.has("Status") && result.has("Message")) {
			try {
				String message = result.getString("Message");
				if (result.getInt("Status") == 1) {

					ToastMsg.showSuccess(getActivity(), message,
							ToastMsg.GRAVITY_TOP);
					// AttendanceId=result.getJSONObject("Data").getInt("AttendanceId");
					// go to attendanceview tab
					FragmentManager fragmentManager = getActivity()
							.getSupportFragmentManager();
					final FragmentTransaction fragmentTransaction = fragmentManager
							.beginTransaction();
					/*
					 * FragmentAttendanceReports fragmentAttendanceReports=new
					 * FragmentAttendanceReports(); Bundle bundle=new Bundle();
					 * bundle.putString("Subject ID", subjectid);
					 * bundle.putString("Class ID", classid);
					 * bundle.putString("Subject Name", subjectname);
					 * bundle.putString("Class Name", classname);
					 * bundle.putString("Action", action);
					 * fragmentAttendanceReports.setArguments(bundle);
					 * fragmentTransaction.replace(R.id.frame_layout,
					 * fragmentAttendanceReports).addToBackStack(null).commit();
					 */
					Bundle bundle = new Bundle();
					bundle.putString("Subject ID", subjectid);
					bundle.putString("Report", "Taken");
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					bundle.putString("TakenDate", mButtonDateChoose.getText()
							.toString());
					FragmentViewAttendance fragmentViewAttendance = new FragmentViewAttendance();
					fragmentViewAttendance.setArguments(bundle);
					fragmentTransaction
							.replace(R.id.frame_layout, fragmentViewAttendance).commit();

					Log.e("AttendanceId", "AttendanceId: " + AttendanceId);

				} else {

					ToastMsg.showError(getActivity(), message,
							ToastMsg.GRAVITY_TOP);
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("AttendanceDetails", "AttendanceDetails error");
			}
		} else {

			ToastMsg.showSuccess(getActivity(),
					"Assignment successfully Submited", ToastMsg.GRAVITY_TOP);
		}

	}

	class GetingStudentListApi extends AsyncTask<String, String, JSONObject> {
		JSONObject jsonobject;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... transaction) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();
			try {
				List<NameValuePair> postParams = new ArrayList<NameValuePair>();
				postParams
						.add(new BasicNameValuePair("ClassId", transaction[0]));

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "student_list.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("student_list", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			array_studentmaster = new ArrayList<StudentMaster>();

			try {

				if (result != null) {
					JSONArray jat = result.getJSONArray("Data");
					for (int i = 0; i < jat.length(); i++) {
						jsonobject = jat.getJSONObject(i);
						mStudentMaster = new StudentMaster(
								jsonobject.getInt("Id"),
								jsonobject.getString("RollNo"),
								jsonobject.getString("StudentName"), "false");
						Utility.strNotAttendRollNumbers = Utility.strNotAttendRollNumbers
								+ jsonobject.getString("Id") + ",";
						array_studentmaster.add(mStudentMaster);
						mStudentListAdapter = new StudentListGridViewAdapter(
								getActivity(), array_studentmaster);
						gridView.setAdapter(mStudentListAdapter);
					}

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}
			super.onPostExecute(result);
		}
	}

}
