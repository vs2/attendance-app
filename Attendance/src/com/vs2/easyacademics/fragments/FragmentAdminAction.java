package com.vs2.easyacademics.fragments;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.FragmentManager.OnBackStackChangedListener;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.ToastMsg;
import com.vs2.easyacademics.objects.Utility;

public class FragmentAdminAction extends SherlockFragment {

	
	private View mView;
	private ActionBar mActionbar;
	private Button mButtonFirst,mButtonSecond,mButtonThird;
	String action,classid,classname,subjectid,subjectname;
	private TextView mTextViewClassName;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_admin_action, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		mActionbar.setTitle("My Profile");
		Bundle bundle = new Bundle();
		bundle = getArguments();
		action = bundle.getString("Action");
		classid = bundle.getString("Class ID");
		classname = bundle.getString("Class Name");
		Log.e("Action","Action: "+action);
		initiGloble();
		return mView;
	}
	public void initiGloble() {
		mButtonFirst=(Button)mView.findViewById(R.id.button_first);
		mButtonSecond=(Button)mView.findViewById(R.id.button_second);
		mButtonThird=(Button)mView.findViewById(R.id.button_third);
		mTextViewClassName=(TextView)mView.findViewById(R.id.text_classname);
		mTextViewClassName.setText(classname);
		
		FragmentManager fragmentManager = getActivity()
				.getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		final FragmentAdminAction fragmentAdminAction=new FragmentAdminAction();
		
		if(action.equals("Classes")){
			mButtonFirst.setText("Manage Subjects");
			mButtonSecond.setText("Manage Students");
			mButtonThird.setText("Manage Assignments");
			
		}else if(action.equals("Subjects")){
			mButtonFirst.setText("Create Subject");
			mButtonSecond.setText("Subjects List");
			mButtonThird.setVisibility(View.GONE);
		}else if(action.equals("AssignmentsAction")){
			Bundle bundle = new Bundle();
			bundle = getArguments();
			mButtonFirst.setText("Create Assignments");
			mButtonSecond.setText("Assignments List");
			mButtonThird.setVisibility(View.GONE);
			subjectid = bundle.getString("Subject ID");
			subjectname = bundle.getString("Subject Name");
			mTextViewClassName.setText(classname+ " -> "+subjectname);
		}else if(action.equals("Students")){
			Bundle bundle = new Bundle();
			bundle = getArguments();
			mButtonFirst.setText("Add Student");
			mButtonSecond.setText("Student List");
			classname = bundle.getString("Class Name");
			classid = bundle.getString("Class ID");
			mButtonThird.setVisibility(View.GONE);
			
		}
		final Bundle bundle=new Bundle();
		mButtonFirst.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(action.equals("Classes")){
					
					bundle.putString("Action", "Subjects");
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					fragmentAdminAction.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminAction).addToBackStack(null).commit();
					
				}else if(action.equals("Subjects")){
					//createNew();
					FragmentCreateSubject fragmentCreateSubject=new FragmentCreateSubject();
					bundle.putString("Action", action);
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					fragmentCreateSubject.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentCreateSubject).addToBackStack(null).commit();
				}else if(action.equals("AssignmentsAction")){
					
					FragmentAssignment fragmentAssignment=new FragmentAssignment();
					bundle.putString("Action", action);
					bundle.putString("Subject ID", subjectid);
					bundle.putString("Subject Name", subjectname);
					bundle.putString("Class Name", classname);
					fragmentAssignment.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAssignment).addToBackStack(null).commit();
				}else if(action.equals("Update"))
				{
					FragmentCreateSubject fragmentCreateSubject=new FragmentCreateSubject();
					bundle.putString("Action", action);
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					fragmentCreateSubject.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentCreateSubject).addToBackStack(null).commit();
					
				}else if(action.equals("Students")){
					howToAddStudent("How to Add Students Details?");
				}
				
			}
		});
		mButtonSecond.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(action.equals("Classes")){
					bundle.putString("Action", "Students");
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					fragmentAdminAction.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminAction).addToBackStack(null).commit();
					
				}else if(action.equals("Subjects")){
					bundle.putString("Class ID", classid);
					bundle.putString("Action", "Subject List");
					bundle.putString("Class Name", classname);
					FragmentSubjectList fragmentSubjectList = new FragmentSubjectList();
					fragmentSubjectList.setArguments(bundle);
					fragmentTransaction
							.replace(R.id.frame_layout, fragmentSubjectList)
							.addToBackStack(null).commit();
					
				}else if(action.equals("AssignmentsAction")){
					FragmentAssignmentlist fragmentAssignmentlist=new FragmentAssignmentlist();
					bundle.putString("Action", action);
					bundle.putString("Subject ID", subjectid);
					bundle.putString("Subject Name", subjectname);
					bundle.putString("Class Name", classname);
					fragmentAssignmentlist.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAssignmentlist).addToBackStack(null).commit();
					
				}else if(action.equals("Students")){
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					FragmentStudentList fragmentStudentList = new FragmentStudentList();
					fragmentStudentList.setArguments(bundle);
					fragmentTransaction.replace(R.id.frame_layout, fragmentStudentList).addToBackStack(null).commit();
				}
			}
		});
		mButtonThird.setOnClickListener(new View.OnClickListener() {
	
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(action.equals("Classes")){
					
					bundle.putString("Class ID", classid);
					bundle.putString("Action", "AssignmentsAction");
					bundle.putString("Class Name", classname);
					FragmentSubjectList fragmentSubjectList = new FragmentSubjectList();
					fragmentSubjectList.setArguments(bundle);
					fragmentTransaction.replace(R.id.frame_layout, fragmentSubjectList).addToBackStack(null).commit();
										
				}
			}
		});
		
	}
	
	public void createNew() {
		LayoutInflater adbInflater = LayoutInflater.from(getActivity());
		View agreeView = adbInflater.inflate(R.layout.new_class, null);
		final EditText edittextClassName = (EditText) agreeView
				.findViewById(R.id.edittext_classname);
		edittextClassName.setHint("Enter Subject Name");
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Add New Subject");
		builder.setView(agreeView);
		builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				if (edittextClassName.getText().toString().equals("")) {
					ToastMsg.showError(getActivity(),
							"You Must Enter Subject Name",
							ToastMsg.GRAVITY_CENTER);
				} else {

					if (Utility.isOnline(getActivity())) {

						new CreateSubjectApi().execute(classid,edittextClassName.getText().toString());
					} else {
						Log.e("No connection", "No internet connection found!");
						ToastMsg.showError(getActivity(),
								"No internet connection found!",
								ToastMsg.GRAVITY_CENTER);
					}

				}

			}
		});
		builder.setNegativeButton("No", null);
		builder.show();
	}
	public class CreateSubjectApi extends AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("please wait....");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();

		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("ClassId", params[0]));
			postParams.add(new BasicNameValuePair("SubjectName", params[1]));

			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "subject_create.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("CreateSubjectApi", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("JSON", result + "");
				if (result != null) {
					processCreateSubject(result);
				} else {
					Log.e("Create Subject",
							"Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processCreateSubject(JSONObject result) {

		if (result.has("Status") && result.has("Message")) {
			try {
				if (result.getInt("Status") == 1) {
					ToastMsg.showSuccess(getActivity(),
							"Subject Successfully Added", ToastMsg.GRAVITY_TOP);
				} else {
					ToastMsg.showError(getActivity(),
							"Subject Creation Error", ToastMsg.GRAVITY_TOP);

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("processCreateSubject", "processCreateSubject error");
			}
		} else {
			Toast.makeText(getActivity(), "Unable to Create a Subject",
					Toast.LENGTH_LONG).show();
		}
	}
	
	public void howToAddStudent(String message) {

		FragmentManager fragmentManager = getActivity()
				.getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Choose way");
		builder.setMessage(message);
		builder.setPositiveButton("Upload file",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						showDialog();
					}
				});
		builder.setNegativeButton("Insert Details",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Bundle bundle = new Bundle();
						bundle.putString("Class ID", classid);
						bundle.putString("Class Name", classname);
						bundle.putString("Student", "Save");
						bundle.putString("Action", "Action");
						FragmentAddStudents fragmentAddStudents = new FragmentAddStudents();
						fragmentAddStudents.setArguments(bundle);
						fragmentTransaction
								.replace(R.id.frame_layout, fragmentAddStudents)
								.addToBackStack(null).commit();
					}
				});
		builder.show();
	}
	public void showDialog() {
		LayoutInflater adbInflater = LayoutInflater.from(getActivity());
		View agreeView = adbInflater.inflate(R.layout.view_of_xls_format, null);
		FragmentManager fragmentManager = getActivity()
				.getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("xls Format View");
		builder.setView(agreeView);
		builder.setMessage("You must need to upload xls file like this format");
		builder.setPositiveButton("Continue",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Bundle bundle = new Bundle();
						bundle.putString("Class ID", classid);
						bundle.putString("Student", "Upload");
						FragmentAddStudents fragmentAddStudents = new FragmentAddStudents();
						fragmentAddStudents.setArguments(bundle);
						fragmentTransaction
								.replace(R.id.frame_layout, fragmentAddStudents)
								.addToBackStack(null).commit();
					}
				});
		builder.setNegativeButton("No,Thanks", null);
		builder.show();
	}
}
