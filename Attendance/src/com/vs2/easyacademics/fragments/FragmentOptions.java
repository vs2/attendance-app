package com.vs2.easyacademics.fragments;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.fragments.FragmentSetting.ChangePassApi;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.PreferenceUser;
import com.vs2.easyacademics.objects.ToastMsg;
import com.vs2.easyacademics.objects.UserMaster;

public class FragmentOptions extends SherlockFragment {
	
	private View mView;
	private ActionBar mActionbar;
	private LinearLayout mLayoutYearPicker,mLayoutChangePassword,mLayoutEditInfo;
	private TextView mTextViewYear,mTextViewSecretCode;
	private DatePickerDialog mDatePickerDialogStartDate;
	
	static final int DATE_DIALOG_ID = 1;
	private int mYear;
	private int mMonth;
	private int mDay;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_options, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		mActionbar.setTitle("My Profile");
		initiGloble();
		return mView;
	}
	public void initiGloble() {
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		final Bundle bundle = new Bundle();
		final FragmentSetting fragmentSetting = new FragmentSetting();
		
		mLayoutYearPicker=(LinearLayout)mView.findViewById(R.id.layout_year_picker);
		mLayoutChangePassword=(LinearLayout)mView.findViewById(R.id.layout_change_password);
		mLayoutEditInfo=(LinearLayout)mView.findViewById(R.id.layout_edit_info);
		mTextViewSecretCode=(TextView)mView.findViewById(R.id.textview_s_code);
		mTextViewYear=(TextView)mView.findViewById(R.id.text_set_year);
		mTextViewYear.setText(FragmentAdmin.sCurrentTermValue);
		mTextViewSecretCode.setText("Your Secret Code Is - "+Global.sUserMaster.getSecretcode());
		mYear=Integer.valueOf(FragmentAdmin.sCurrentTermValue)+1;
		mLayoutYearPicker.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("","CLick");
				openDialogForDateStart();
			}
		});
		mLayoutChangePassword.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				
				bundle.putString("Action", "Change Password");
				bundle.putString("Back", "Option");
				fragmentSetting.setArguments(bundle);
				fragmentTransaction
						.replace(R.id.frame_layout, fragmentSetting)
						.addToBackStack(null).commit();
			}
		});
		mLayoutEditInfo.setOnClickListener(new View.OnClickListener() {
	
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bundle.putString("Action", "Edit Information");
				fragmentSetting.setArguments(bundle);
				fragmentTransaction
						.replace(R.id.frame_layout, fragmentSetting)
						.addToBackStack(null).commit();
			}
		});
		
	}
	public void openDialogForDateStart() {
		/*mDatePickerDialogStartDate = new DatePickerDialog(getActivity(),
				datePickerStart, mYear, mMonth, mDay);*/
		mDatePickerDialogStartDate= customDatePicker();
		
		mDatePickerDialogStartDate.show();
	}
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:

			//return new DatePickerDialog(getActivity(), datePickerStart, mYear, mMonth, mDay);
			DatePickerDialog datePickerDialog = customDatePicker();
			return datePickerDialog;
		
		}
		return null;
	}
	private DatePickerDialog.OnDateSetListener datePickerStart = new DatePickerDialog.OnDateSetListener() {

		@Override
		@SuppressLint("SimpleDateFormat")
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			mYear = selectedYear;
			mMonth = selectedMonth;
			mDay = selectedDay;
			updateDate();
			
		}
	};
	protected void updateDate() {
		int localMonth = (mMonth + 1);
		String monthString = localMonth < 10 ? "0" + localMonth : Integer
				.toString(localMonth);
		String localYear = Integer.toString(mYear);
		/*etPickADate.setText(new StringBuilder()
		// Month is 0 based so add 1
				.append(monthString).append("/").append(localYear).append(" "));*/
		
		if(FragmentAdmin.sCurrentTermValue.equals(localYear)){
			mTextViewYear.setText(localYear);
		}else{
			new ChangeTermApi().execute(mYear+"",Global.sUserMaster.getCollegeid()+"");
		}
		
		
		//openDialogForDateStart();
	}
	private DatePickerDialog customDatePicker() {
		DatePickerDialog dpd = new DatePickerDialog(getActivity(), datePickerStart,
				mYear, mMonth, mDay);
		try {

			Field[] datePickerDialogFields = dpd.getClass().getDeclaredFields();
			for (Field datePickerDialogField : datePickerDialogFields) {
				if (datePickerDialogField.getName().equals("mDatePicker")) {
					datePickerDialogField.setAccessible(true);
					DatePicker datePicker = (DatePicker) datePickerDialogField
							.get(dpd);
					Field datePickerFields[] = datePickerDialogField.getType()
							.getDeclaredFields();
					for (Field datePickerField : datePickerFields) {
						
						if ("mDayPicker".equals(datePickerField.getName())
								|| "mDaySpinner".equals(datePickerField
										.getName())) {
							datePickerField.setAccessible(true);
							Object dayPicker = new Object();
							dayPicker = datePickerField.get(datePicker);
							((View) dayPicker).setVisibility(View.GONE);
						}
						if ("mMonthPicker".equals(datePickerField.getName())
								|| "mMonthSpinner".equals(datePickerField
										.getName())) {
							datePickerField.setAccessible(true);
							Object dayPicker = new Object();
							dayPicker = datePickerField.get(datePicker);
							((View) dayPicker).setVisibility(View.GONE);
						}
						if ("mCalendarView".equals(datePickerField.getName())) {
							datePickerField.setAccessible(true);
							Object dayPicker = new Object();
							dayPicker = datePickerField.get(datePicker);
							((View) dayPicker).setVisibility(View.GONE);
						}
					}
				}
			

			}
		} catch (Exception ex) {
		}
		return dpd;
	}
	
	public class ChangeTermApi extends AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("TermValue", params[0]));
			postParams.add(new BasicNameValuePair("CollegeId", params[1]));
			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "academic_term_set_defualt.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("Change Term", "Error : " + e.toString());
				return null;
			}
		}
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				if (result != null) {
					processChange(result);
				} else {
					Log.e( "Change Term",
							"Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processChange(JSONObject result) {
		try {
			if (result.getInt("Status") == 1) {
				
				mTextViewYear.setText(Integer.toString(mYear));
				FragmentAdmin.sCurrentTermValue=Integer.toString(mYear);
				Global.sUserMaster.setTermid(result.getJSONObject("Data").getInt("TermId"));
				Log.e("TermId","TermId: "+result.getJSONObject("Data").getInt("TermId"));
				
			} else {
				ToastMsg.showError(getActivity(),
						"Error on Change Academic Term", ToastMsg.GRAVITY_TOP);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e("Change", "Change error"+e.toString());
		}
	}
}
