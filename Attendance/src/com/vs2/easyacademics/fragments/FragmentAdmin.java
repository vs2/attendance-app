package com.vs2.easyacademics.fragments;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.ToastMsg;
import com.vs2.easyacademics.objects.Utility;

public class FragmentAdmin extends SherlockFragment {

	private View mView;
	private ActionBar mActionbar;

	private LinearLayout mLayoutClasses, mLayoutStudents, mLayoutSubjects,
			mLayoutAssignments, mLayoutTeachers, mLayoutOptions;
	private TextView mTextViewCurrentTerm,mTextViewCollegeName,mTextViewSecretCode;
	private static Calendar c = Calendar.getInstance();
	public static String sCurrentTermValue=null,sCollegeName=null,sSecretCode=null,sRuningYear=c.get(Calendar.YEAR)+"";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_admin, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		mActionbar.setTitle("My Profile");
		
		
		initiGloble();
		return mView;
	}

	public void initiGloble() {
		mLayoutAssignments = (LinearLayout) mView
				.findViewById(R.id.layout_assignments);
		mLayoutClasses = (LinearLayout) mView.findViewById(R.id.layout_classes);
		mLayoutStudents = (LinearLayout) mView
				.findViewById(R.id.layout_students);
		mLayoutSubjects = (LinearLayout) mView
				.findViewById(R.id.layout_subject);
		mLayoutTeachers = (LinearLayout) mView
				.findViewById(R.id.layout_teachers);
		mLayoutOptions = (LinearLayout) mView.findViewById(R.id.layout_options);
		mTextViewCurrentTerm=(TextView)mView.findViewById(R.id.textview_current_year);
		mTextViewCollegeName=(TextView)mView.findViewById(R.id.textview_college_name);
		mTextViewSecretCode=(TextView)mView.findViewById(R.id.textview_secret_code);
		
		setUpOnClickListener();
		Log.e("UserMaster","User: "+Global.sUserMaster.getUsername());
		Log.e("TermMaster","Term: "+Global.sUserMaster.getTermid());
		new GettingTermId().execute(Global.sUserMaster.getCollegeid()+"");
		//new GettingCollegeName().execute(Global.sUserMaster.getCollegeid()+"");
		mTextViewCollegeName.setText(""+Global.sUserMaster.getCollegename());
		mTextViewSecretCode.setText("("+Global.sUserMaster.getSecretcode()+")");
		mTextViewCurrentTerm.setText("You Are In - "+sCurrentTermValue);
		
	}

	public void setUpOnClickListener() {
		
		FragmentManager fragmentManager = getActivity()
				.getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		final FragmentAdminClasses fragmentAdminClasses=new FragmentAdminClasses();
		final Bundle bundle=new Bundle();

		mLayoutAssignments.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (Utility.isOnline(getActivity())) {
					
					bundle.putString("Action", "AssignmentsAction");
					fragmentAdminClasses.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminClasses).addToBackStack(null).commit();
					/*bundle.putString("Action", "Assignments");
					fragmentAdminAction.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminAction).addToBackStack(null).commit();*/
				} else {
					Log.e("No connection", "No internet connection found!");
					ToastMsg.showError(getActivity(),
							"No internet connection found!",
							ToastMsg.GRAVITY_CENTER);
				}
				

			}
		});
		mLayoutClasses.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (Utility.isOnline(getActivity())) {
					bundle.putString("Action", "Classes");
					fragmentAdminClasses.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminClasses).addToBackStack(null).commit();
				} else {
					Log.e("No connection", "No internet connection found!");
					ToastMsg.showError(getActivity(),
							"No internet connection found!",
							ToastMsg.GRAVITY_CENTER);
				}
			}
		});
		mLayoutStudents.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (Utility.isOnline(getActivity())) {
					bundle.putString("Action", "Students");
					fragmentAdminClasses.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminClasses).addToBackStack(null).commit();
				} else {
					Log.e("No connection", "No internet connection found!");
					ToastMsg.showError(getActivity(),
							"No internet connection found!",
							ToastMsg.GRAVITY_CENTER);
				}
			}
		});
		mLayoutSubjects.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (Utility.isOnline(getActivity())) {
					bundle.putString("Action", "Subjects");
					fragmentAdminClasses.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminClasses).addToBackStack(null).commit();
				} else {
					Log.e("No connection", "No internet connection found!");
					ToastMsg.showError(getActivity(),
							"No internet connection found!",
							ToastMsg.GRAVITY_CENTER);
				}
			}
		});
		mLayoutTeachers.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (Utility.isOnline(getActivity())) {
					bundle.putString("Action", "Teachers");
					fragmentAdminClasses.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminClasses).addToBackStack(null).commit();
				} else {
					Log.e("No connection", "No internet connection found!");
					ToastMsg.showError(getActivity(),
							"No internet connection found!",
							ToastMsg.GRAVITY_CENTER);
				}
			}
		});
		mLayoutOptions.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (Utility.isOnline(getActivity())) {
					fragmentTransaction.replace(R.id.frame_layout, new FragmentOptions()).addToBackStack(null).commit();
				} else {
					Log.e("No connection", "No internet connection found!");
					ToastMsg.showError(getActivity(),
							"No internet connection found!",
							ToastMsg.GRAVITY_CENTER);
				}
			}
		});
	}
	
	class GettingTermId extends AsyncTask<String, String, JSONObject> {
		JSONObject jsonobject;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... transaction) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();
			try {
				List<NameValuePair> postParams = new ArrayList<NameValuePair>();
				postParams.add(new BasicNameValuePair("CollegeId",transaction[0]));
								
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "academic_term_getting.php",
						postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("academic_term_getting", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject resultTransaction) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				//JSONArray jat = resultTransaction.getJSONArray("Data");
				if (resultTransaction != null) {
					sCurrentTermValue=resultTransaction.getJSONObject("Data").getString("TermValue");
					mTextViewCurrentTerm.setText("You Are In - "+sCurrentTermValue);
				} 
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			super.onPostExecute(resultTransaction);
		}
	}
	
	class GettingCollegeName extends AsyncTask<String, String, JSONObject> {
		JSONObject jsonobject;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... transaction) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();
			try {
				List<NameValuePair> postParams = new ArrayList<NameValuePair>();
				postParams.add(new BasicNameValuePair("CollegeId",transaction[0]));
								
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "college_details.php",
						postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("college_details", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject resultTransaction) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				//JSONArray jat = resultTransaction.getJSONArray("Data");
				if (resultTransaction != null) {
					sCollegeName=resultTransaction.getJSONObject("Data").getString("CollegeName");
					sSecretCode=resultTransaction.getJSONObject("Data").getString("SecretCode");
					mTextViewCollegeName.setText(""+sCollegeName);
					mTextViewSecretCode.setText("("+sSecretCode+")");
				} 
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			super.onPostExecute(resultTransaction);
		}
	}
}
