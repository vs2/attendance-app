package com.vs2.easyacademics.fragments;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.ToastMsg;

public class FragmentAddStudents extends SherlockFragment {

	private View mView;
	private ActionBar mActionbar;
	private EditText mEditTextEnrolmentNo, mEditTextStudentName,
			mEditTextContactNo, mEditTextAddress, mEditTextEmail,mEditTextUplaodedFilePath;
	private Button mButtonSave, mButtonUpdate, mButtonDelete,mButtonUpload,mButtonBrowser;
	private String classid,classname;
	public String studentid;
	public String uploadedFileName;
	public String uploadedFilePath;
	public String uploadedFileExtention;
	
	private RelativeLayout mLayoutUpload;
	private LinearLayout mLayoutAddStud;
	
    public static String EnrolmentNo,StudName,StudAddress,StudContNo,StudeEmail;
	public static int counter=0;
	
	String upLoadServerUri = "http://clg.vs2.in/upload_file.php";
	String uploadFilePath = "";
	String uploadFileName = "";
	int serverResponseCode = 0;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_add_student, null);
		mActionbar = getSherlockActivity().getSupportActionBar();

		Bundle bundle = new Bundle();
		bundle = getArguments();
		classid = bundle.getString("Class ID");
		classname = bundle.getString("Class Name");
		initGloble();
		return mView;
	}

	public void initGloble() {
		mEditTextEnrolmentNo = (EditText) mView
				.findViewById(R.id.edittext_enrolment_no);
		mEditTextStudentName = (EditText) mView
				.findViewById(R.id.edittext_student_name);
		mEditTextContactNo = (EditText) mView
				.findViewById(R.id.edittext_student_contact_no);
		mEditTextAddress = (EditText) mView
				.findViewById(R.id.edittext_student_Addressr);
		mEditTextEmail = (EditText) mView
				.findViewById(R.id.edittext_student_email);
		mEditTextUplaodedFilePath = (EditText) mView
				.findViewById(R.id.edittext_uploaded_filename);
		mButtonSave = (Button) mView.findViewById(R.id.button_save_student);
		mButtonUpdate = (Button) mView.findViewById(R.id.button_update_student);
		mButtonDelete = (Button) mView.findViewById(R.id.button_delete_student);
		mButtonUpload = (Button) mView.findViewById(R.id.button_upload);
		mButtonBrowser = (Button) mView.findViewById(R.id.button_browser);
		
		mLayoutUpload=(RelativeLayout)mView.findViewById(R.id.layout_upload);
		mLayoutAddStud=(LinearLayout)mView.findViewById(R.id.layout_add_stude);
		
		Bundle arguments = getArguments();
		if (arguments != null){
			if(arguments.get("Student").toString().matches("Save")){
				mActionbar.setTitle("Student Entry");
				mLayoutUpload.setVisibility(View.GONE);
				mButtonDelete.setVisibility(View.GONE);
				mButtonUpdate.setVisibility(View.GONE);
			}else if(arguments.get("Student").toString().matches("Upload")){
				mActionbar.setTitle("Upload File");
				mLayoutAddStud.setVisibility(View.GONE);
				
			}else{
				mLayoutUpload.setVisibility(View.GONE);
				mActionbar.setTitle("Student");
				Bundle bundle1 = new Bundle();
				bundle1 = getArguments();
				studentid=bundle1.getString("Student ID");
				Log.e("Student ID",""+studentid);
				mButtonSave.setVisibility(View.GONE);
				new GetingStudentDetailsApi().execute(studentid);
			}
			
		}
		
		mButtonSave.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(notBlank(mEditTextEnrolmentNo) && notBlank(mEditTextStudentName)){
					
					if(mEditTextEmail.getText().toString().length()>0){
						if(validEmail(mEditTextEmail.getText().toString())){
							new StudentEntryApi().execute(mEditTextEnrolmentNo.getText()
									.toString(), mEditTextStudentName.getText().toString(),classid,mEditTextContactNo.getText().toString(),
									mEditTextAddress.getText().toString(), mEditTextEmail
											.getText().toString());
						}else{
							ToastMsg.showError(getActivity(),"Invalid Email", ToastMsg.GRAVITY_CENTER);
						}
						
					}else{
						new StudentEntryApi().execute(mEditTextEnrolmentNo.getText()
								.toString(), mEditTextStudentName.getText().toString(),classid,mEditTextContactNo.getText().toString(),
								mEditTextAddress.getText().toString(), mEditTextEmail
										.getText().toString());
					}
				}
			}
		});
		mButtonUpdate.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new UpdateStudentDetailsApi().execute(studentid,mEditTextEnrolmentNo.getText()
						.toString(),mEditTextStudentName.getText()
						.toString(), mEditTextContactNo.getText().toString(),
						mEditTextAddress.getText().toString(), mEditTextEmail
								.getText().toString());
				
				
			}
		});
		mButtonDelete.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new DeleteStudentApi().execute(studentid);
				
			}
		});
		mButtonBrowser.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
	        	intent.setType("file/*");
	        	intent.setAction(Intent.ACTION_GET_CONTENT);
	        	startActivityForResult(Intent.createChooser(intent, "Select .xls File"),1);
			}
		});
		mButtonUpload.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mEditTextUplaodedFilePath.getText().toString().equals(""))
				{
					ToastMsg.showError(getActivity(), "Please Browser file", ToastMsg.GRAVITY_TOP);
				}else{
					if(uploadedFileExtention.matches(".xls")){
					
						uploadFilePath=mEditTextUplaodedFilePath.getText().toString();
						uploadFileName = uploadFilePath.substring( uploadFilePath.lastIndexOf('/')+1, uploadFilePath.length());
						new UploadFile().execute();
						Log.e("file path"," uploadFileName: uploads/"+uploadFileName);
					}else{
						ToastMsg.showError(getActivity(), "Uploaded File is not .xls format, You must need to upload .xls file.", ToastMsg.GRAVITY_TOP);
					}
				}
			}
		});
		
	}
	public boolean validEmail(String emailAddress)
	{
	        
	        if( Build.VERSION.SDK_INT >= 8 )
	          {
	            return android.util.Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches();
	          }

	          Pattern pattern;
	          Matcher matcher;
	          String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)(\\.[A-Za-z]{2,})$";
	          pattern = Pattern.compile(EMAIL_PATTERN);
	          matcher = pattern.matcher(emailAddress);

	          return matcher.matches();
	}
	@SuppressWarnings("static-access")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == getActivity().RESULT_OK) 
	     {
	        Uri uri= data.getData();
	        String url=uri+"";
	        uploadedFileName=url.substring(url.lastIndexOf('/')+1, url.lastIndexOf('.'))+url.substring(url.lastIndexOf("."));
	        uploadedFileExtention=url.substring(url.lastIndexOf("."));
	        Log.e("path","path "+uri.getPath());
	        uploadedFilePath=uri.getPath();
	        mEditTextUplaodedFilePath.setText(uri.getPath());
			
	      }
		super.onActivityResult(requestCode, resultCode, data);
	}
	 public static boolean isExternalStorageReadOnly() { 
	        String extStorageState = Environment.getExternalStorageState(); 
	        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) { 
	            return true; 
	        } 
	        return false; 
	    } 
	 
	    public static boolean isExternalStorageAvailable() { 
	        String extStorageState = Environment.getExternalStorageState(); 
	        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) { 
	            return true; 
	        } 
	        return false; 
	    } 

	public class DeleteStudentApi extends AsyncTask<String,Integer,JSONObject> {

		ProgressDialog pd;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Deleting Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
			
		}
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			JSONParser parser = new JSONParser();

			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("StudentId", params[0]));

			try {
				
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "student_delete.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("DeleteStudentApi", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				
				ToastMsg.showSuccess(getActivity(), "Student Successfully Deleted.", ToastMsg.GRAVITY_TOP);
				Bundle bundle=new Bundle();				
				FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
	    		FragmentTransaction fragmentTransaction = fragmentManager
	    				.beginTransaction();	 
	    		bundle.putString("Class ID", classid);
	    		bundle.putString("Class Name", classname);
				FragmentStudentList fragmentStudentList = new FragmentStudentList();
				fragmentStudentList.setArguments(bundle);
				fragmentTransaction
						.replace(R.id.frame_layout, fragmentStudentList).commit();
				
	    		
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}
	public class UpdateStudentDetailsApi extends AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Updating Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("StudentId", params[0]));
			postParams.add(new BasicNameValuePair("RollNo", params[1]));
			postParams.add(new BasicNameValuePair("StudentName", params[2]));
			postParams.add(new BasicNameValuePair("ContactNo", params[3]));
			postParams.add(new BasicNameValuePair("Address", params[4]));
			postParams.add(new BasicNameValuePair("Email", params[5]));
			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "student_update.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("UpdateStudentDetailsApi", "Error : " + e.toString());
				return null;
			}
		}

		
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				if (result != null) {
					processUpdate(result);
				} else {
					
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processUpdate(JSONObject result) {
		try {
			String message = result.getString("Message");
			if (result.getInt("Status") == 1) {
				ToastMsg.showSuccess(getActivity(), message,ToastMsg.GRAVITY_TOP);
				
				Bundle bundle=new Bundle();				
				FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
	    		FragmentTransaction fragmentTransaction = fragmentManager
	    				.beginTransaction();	 
	    		bundle.putString("Class ID", classid);
	    		bundle.putString("Class Name", classname);
				FragmentStudentList fragmentStudentList = new FragmentStudentList();
				fragmentStudentList.setArguments(bundle);
				fragmentTransaction
						.replace(R.id.frame_layout, fragmentStudentList).commit();
				
				
			} else {
				Toast.makeText(getActivity(), message, Toast.LENGTH_LONG)
						.show();
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
		}
	}
	class GetingStudentDetailsApi extends AsyncTask<String, String, JSONObject> {
		JSONObject jsonobject;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... transaction) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();
			try {
				List<NameValuePair> postParams = new ArrayList<NameValuePair>();
				postParams.add(new BasicNameValuePair("StudentId",transaction[0]));
								
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "student_details.php",
						postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("student_list", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				if (result != null) {
					JSONObject jsonobject=new JSONObject();
					jsonobject=result.getJSONObject("Data");
					Log.e("",jsonobject+"");
					mEditTextEnrolmentNo.setText(jsonobject.getInt("RollNo")+"");
					mEditTextStudentName.setText(jsonobject.getString("StudentName"));
					mEditTextContactNo.setText(jsonobject.getString("ContactNo"));
					mEditTextAddress.setText(jsonobject.getString("Address"));
					mEditTextEmail.setText(jsonobject.getString("Email"));
				} else {
					Log.e("student_list", "Server encontered problem try again later");
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			super.onPostExecute(result);
		}
	}
	public class StudentEntryApi extends AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Please wait....");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();

		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("RollNo", params[0]));
			postParams.add(new BasicNameValuePair("StudentName", params[1]));
			postParams.add(new BasicNameValuePair("ClassId", params[2]));
			postParams.add(new BasicNameValuePair("ContactNo", params[3]));
			postParams.add(new BasicNameValuePair("Address", params[4]));
			postParams.add(new BasicNameValuePair("Email", params[5]));
			Log.e("Values","Values: "+params[0]+" "+params[1]+" "+params[2]+" "+params[3]+" "+params[4]+" "+params[5]);
			//return null;
			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "student_create.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("StudentEntryApi", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("JSON", result + "");
				if (result != null) {
					processAddingStudent(result);
				} else {
					Log.e("StudentEntryApi",
							"Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processAddingStudent(JSONObject result) {

		if (result.has("Status") && result.has("Message")) {
			try {
				String message = result.getString("Message");
				if (result.getInt("Status") == 1) {
					ToastMsg.showSuccess(getActivity(),
							"Student Added Successfully", ToastMsg.GRAVITY_TOP);
					mEditTextEnrolmentNo.setText("");
					mEditTextStudentName.setText("");
					mEditTextContactNo.setText("");
					mEditTextAddress.setText("");
					mEditTextEmail.setText("");
					addMoreStudents();
							    		
				} else {
					// Toast.makeText(getActivity(),message,
					// Toast.LENGTH_LONG).show();
					ToastMsg.showError(getActivity(), message,
							ToastMsg.GRAVITY_TOP);
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("StudentDetails", "StudentDetails error");
			}
		} else {
			Toast.makeText(getActivity(), "Unable to StudentEntry",
					Toast.LENGTH_LONG).show();
		}
	}
	public void addMoreStudents(){
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		//builder.setTitle("Add New Subject");
		builder.setMessage("You Want to add more students?");
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		builder.setPositiveButton("Yes", null);
		builder.setNegativeButton("No",new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
				Bundle arguments = getArguments();
				if(arguments.get("Action").toString().matches("Action")){
					FragmentAdminAction fragmentAdminAction=new FragmentAdminAction();
					Bundle bundle=new Bundle();				
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					bundle.putString("Action", "Students");
					fragmentAdminAction.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminAction).commit();
				}else if(arguments.get("Action").toString().matches("Manage Students")){
					FragmentClassProfile fragmentClassProfile=new FragmentClassProfile();
					Bundle bundle=new Bundle();				
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					bundle.putString("Action", "Manage Students");
					fragmentClassProfile.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentClassProfile).commit();
				}else{
					FragmentClassProfile fragmentClassProfile=new FragmentClassProfile();
					Bundle bundle=new Bundle();				
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					bundle.putString("Action", "profile");
					fragmentClassProfile.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentClassProfile).commit();
				}
			}
		});
		builder.show();	
	}
	private boolean notBlank(EditText edit){
		if(edit.getText().length() > 0){
			edit.setError(null);
			return true;
		}else{
			edit.setError("Required " + edit.getHint());
			return false;
		}
	}
	public class StudentUploadApi extends AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Please wait....");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();

		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("RollNo", params[0]));
			postParams.add(new BasicNameValuePair("StudentName", params[1]));
			postParams.add(new BasicNameValuePair("ClassId", params[2]));
			postParams.add(new BasicNameValuePair("ContactNo", params[3]));
			postParams.add(new BasicNameValuePair("Address", params[4]));
			postParams.add(new BasicNameValuePair("Email", params[5]));
			Log.e("Values","Values: "+params[0]+" "+params[1]+" "+params[2]+" "+params[3]+" "+params[4]+" "+params[5]);
			//return null;
			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "student_create.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("StudentEntryApi", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("JSON", result + "");
				if (result != null) {
					processUploadStudent(result);
				} else {
					Log.e("StudentEntryApi",
							"Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}
	private void processUploadStudent(JSONObject result) {

		if (result.has("StudentDetails") && result.has("Message")) {
			try {
				String message = result.getString("Message");
				if (result.getInt("StudentDetails") == 1) {
					
					ToastMsg.showSuccess(getActivity(),
							"Student Added Successfully", ToastMsg.GRAVITY_TOP);
												    		
				} else {
					ToastMsg.showError(getActivity(), message,
							ToastMsg.GRAVITY_TOP);
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("StudentDetails", "StudentDetails error");
			}
		} else {
			Toast.makeText(getActivity(), "Unable to StudentEntry of "+EnrolmentNo,
					Toast.LENGTH_LONG).show();
		}
	}
	
	/**
	 * Uploading User Image
	 */
	public class UploadFile extends AsyncTask<String,Integer,String>{
		ProgressDialog pd;
		String fileUrl;
		
		public UploadFile(){
						
		}

		/**
		 * Things to be done before execution 
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Uploading file please wait...");
			pd.setCancelable(false);
			pd.show();
			super.onPreExecute();
		}
		/**
		 *  Do your long operations here and return the result
		 */
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			return uploadFile(uploadFilePath);
		}
		
		/**
		 * execution of result
		 */
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("Upload file", "Success");
				if(result !=  null && result.length() > 0){
					fileUrl = result;
					if(!fileUrl.equals("fail")){
						ToastMsg.show(getActivity(), "File uploaded succefully",ToastMsg.GRAVITY_TOP);
						new StudentDBApi().execute("uploads/"+uploadFileName,classid);
						
					}else{
						ToastMsg.show(getActivity(), "Unable to upload file",ToastMsg.GRAVITY_TOP);
						
					}
					
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			super.onPostExecute(result);
		}
		
	}
	
public String  uploadFile(String sourceFileUri) {
        
        String fileName = sourceFileUri;

        HttpURLConnection conn = null;
        DataOutputStream dos = null;  
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024; 
        File sourceFile = new File(sourceFileUri); 
        
        String imageUrl="";
         
        if (sourceFile.isFile()) {
             try { 
                  
                   // open a URL connection to the Servlet
                 FileInputStream fileInputStream = new FileInputStream(sourceFile);
                 URL url = new URL(upLoadServerUri);
                  
                 // Open a HTTP  connection to  the URL
                 conn = (HttpURLConnection) url.openConnection(); 
                 conn.setDoInput(true); // Allow Inputs
                 conn.setDoOutput(true); // Allow Outputs
                 conn.setUseCaches(false); // Don't use a Cached Copy
                 conn.setRequestMethod("POST");
                 conn.setRequestProperty("Connection", "Keep-Alive");
                 conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                 conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                 conn.setRequestProperty("uploaded_file", fileName);
                
                  
                 dos = new DataOutputStream(conn.getOutputStream());
        
                 dos.writeBytes(twoHyphens + boundary + lineEnd); 
                 dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""+ fileName +"\""+ lineEnd);
                  
                 dos.writeBytes(lineEnd);
        
                 // create a buffer of  maximum size
                 bytesAvailable = fileInputStream.available(); 
        
                 bufferSize = Math.min(bytesAvailable, maxBufferSize);
                 buffer = new byte[bufferSize];
        
                 // read file and write it into form...
                 bytesRead = fileInputStream.read(buffer, 0, bufferSize);  
                    
                 while (bytesRead > 0) {
                      
                   dos.write(buffer, 0, bufferSize);
                   bytesAvailable = fileInputStream.available();
                   bufferSize = Math.min(bytesAvailable, maxBufferSize);
                   bytesRead = fileInputStream.read(buffer, 0, bufferSize);   
                    
                  }
        
                 // send multipart form data necesssary after file data...
                 dos.writeBytes(lineEnd);
                 dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
        
                 // Responses from the server (code and message)
                 serverResponseCode = conn.getResponseCode();
                 final String serverResponseMessage = conn.getResponseMessage();
                 
                 if(serverResponseCode == 200){
               	  InputStream is = conn.getInputStream();
                   try {
  						imageUrl = streamToString(is);
  						/*
  						JSONObject json = new JSONObject(message.trim());
  						if(json.has("result")){
  							if(json.getJSONObject("result").has("filepath")){
  							  imageUrl = json.getJSONObject("result").getString("filepath");
  							}
  						}
  						*/
  					} catch (IOException e) {
  						// TODO Auto-generated catch block
  						
  					}
                                     
                 }   
                  
                 //close the streams //
                 fileInputStream.close();
                 dos.flush();
                 dos.close();
                   
            } catch (MalformedURLException ex) {
                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);  
            } catch (Exception e) {
                 
                  
            }
                   
             
             
         } // End else block
        return imageUrl;
       }

public static String streamToString(InputStream is) throws IOException {
    StringBuilder sb = new StringBuilder();
    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
    String line;
    while ((line = rd.readLine()) != null) {
        sb.append(line);
    }
    return sb.toString();
}
public class StudentDBApi extends AsyncTask<String, Integer, JSONObject> {

	ProgressDialog pd;

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		pd = new ProgressDialog(getActivity());
		pd.setMessage("Please wait....");
		pd.setIndeterminate(false);
		pd.setCancelable(false);
		pd.show();

	}

	@Override
	protected JSONObject doInBackground(String... params) {
		// TODO Auto-generated method stub

		JSONParser parser = new JSONParser();
		List<NameValuePair> postParams = new ArrayList<NameValuePair>();
		postParams.add(new BasicNameValuePair("FilePath", params[0]));
		postParams.add(new BasicNameValuePair("ClassId", params[1]));
		
		//return null;
		try {

			return parser.getJSONFromUrl(Global.SERVER_PATH
					+ "reader.php", postParams);

		} catch (Exception e) {
			// TODO: handle exception
			Log.e("StudentDBApi", "Error : " + e.toString());
			return null;
		}
	}

	@Override
	protected void onPostExecute(JSONObject result) {
		// TODO Auto-generated method stub
		try {
			pd.dismiss();
			Log.e("JSON", result + "");
			if (result != null) {
				processStudent(result);
			} else {
				Log.e("StudentEntryApi",
						"Server encontered problem try again later");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.onPostExecute(result);
	}

}

private void processStudent(JSONObject result) {

	if (result.has("Status") && result.has("Message")) {
		try {
			String message = result.getString("Message");
			if (result.getInt("Status") == 1) {
				/*ToastMsg.showSuccess(getActivity(),
						"Student Added Successfully", ToastMsg.GRAVITY_TOP);*/
									    		
			} else {
				/*ToastMsg.showError(getActivity(), message,
						ToastMsg.GRAVITY_TOP);*/
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e("StudentDetails", "StudentDetails error");
		}
	} else {
		Toast.makeText(getActivity(), "Unable to StudentEntry",
				Toast.LENGTH_LONG).show();
	}
}

}
