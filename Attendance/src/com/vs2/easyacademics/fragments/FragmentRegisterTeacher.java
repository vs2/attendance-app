package com.vs2.easyacademics.fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.fragments.FragmentAddStudents.StudentEntryApi;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.ToastMsg;
import com.vs2.easyacademics.objects.Utility;

public class FragmentRegisterTeacher extends SherlockFragment {

	private View mView;
	private ActionBar mActionbar;
	private EditText mEditTextUserName,mEditTextPassword,mEditTextRePassword,mEditTextEmail,mEditTextPhoneNo;
	private Button mButtonRegister,mButtonUpdate,mButtonDelete;
	private String action,userid;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_register_teacher, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		mActionbar.setTitle("My Profile");
		initiGloble();
		return mView;
	}
	public void initiGloble() {
		
		Bundle bundle = new Bundle();
		bundle = getArguments();
		action = bundle.getString("Action");
		
		
		mEditTextUserName=(EditText)mView.findViewById(R.id.edittext_teacher_username);
		mEditTextPassword=(EditText)mView.findViewById(R.id.edittext_teacher_password);
		mEditTextRePassword=(EditText)mView.findViewById(R.id.edittext_teacher_repassword);
		mEditTextEmail=(EditText)mView.findViewById(R.id.edittext_teacher_emailid);
		mEditTextPhoneNo=(EditText)mView.findViewById(R.id.edittext_teacher_contactno);
		mButtonRegister=(Button)mView.findViewById(R.id.button_register_teacher);
		mButtonUpdate=(Button)mView.findViewById(R.id.button_update_teacher);
		mButtonDelete=(Button)mView.findViewById(R.id.button_delete_teacher);
		

		if(action.equals("New")){
			mButtonDelete.setVisibility(View.GONE);
			mButtonUpdate.setVisibility(View.GONE);
		}else{
			userid = bundle.getString("User ID");
			mButtonRegister.setVisibility(View.GONE);
			new GetingTeacherDetailsApi().execute(userid);
			mEditTextPassword.setVisibility(View.GONE);
			mEditTextRePassword.setVisibility(View.GONE);
		}
		mButtonRegister.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(notBlank(mEditTextUserName) && notBlank(mEditTextPassword)
						&& notBlank(mEditTextRePassword)){
					
					if (Utility.isOnline(getActivity())) {
						
						if(mEditTextPassword.getText().toString().equals(mEditTextRePassword.getText().toString())){
							
							if(mEditTextEmail.getText().toString().length()>0){
								if(validEmail(mEditTextEmail.getText().toString())){
									new RegisterTeacherApi().execute(mEditTextUserName.getText().toString(),
											Global.sUserMaster.getCollegeid()+"","2",
											mEditTextPassword.getText().toString(), mEditTextEmail.getText().toString(),
											mEditTextPhoneNo.getText().toString());
								}else{
									ToastMsg.showError(getActivity(),"Invalid Email", ToastMsg.GRAVITY_CENTER);
								}
								
							}else{
								new RegisterTeacherApi().execute(mEditTextUserName.getText().toString(),
										Global.sUserMaster.getCollegeid()+"","2",
										mEditTextPassword.getText().toString(), mEditTextEmail.getText().toString(),
										mEditTextPhoneNo.getText().toString());
							}
							
						}else{
							ToastMsg.showError(getActivity(),
									"Password not Match", ToastMsg.GRAVITY_TOP);
						}
					} else {
						Log.e( "No connection", "No internet connection found!");
						ToastMsg.showError(getActivity(),"No internet connection found!", ToastMsg.GRAVITY_CENTER);
					}
					
				}
				
			}
		});
		
		mButtonDelete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new DeleteTeacherApi().execute(userid);
				
			}
		});
		mButtonUpdate.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new UpdateTeacherDetailsApi().execute(userid,mEditTextUserName.getText()
						.toString(),mEditTextEmail.getText()
						.toString(), mEditTextPhoneNo.getText().toString());
			}
		});
	}
	public boolean validEmail(String emailAddress)
	{
	        
	        if( Build.VERSION.SDK_INT >= 8 )
	          {
	            return android.util.Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches();
	          }

	          Pattern pattern;
	          Matcher matcher;
	          String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)(\\.[A-Za-z]{2,})$";
	          pattern = Pattern.compile(EMAIL_PATTERN);
	          matcher = pattern.matcher(emailAddress);

	          return matcher.matches();
	}
	public class UpdateTeacherDetailsApi extends AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Updating Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("UserId", params[0]));
			postParams.add(new BasicNameValuePair("UserName", params[1]));
			postParams.add(new BasicNameValuePair("Email", params[2]));
			postParams.add(new BasicNameValuePair("PhoneNo", params[3]));
			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "teacher_update.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("UpdateTeacherDetailsApi", "Error : " + e.toString());
				return null;
			}
		}

		
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				if (result != null) {
					processUpdate(result);
				} else {
					
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processUpdate(JSONObject result) {
		try {
			String message = result.getString("Message");
			if (result.getInt("Status") == 1) {
				ToastMsg.showSuccess(getActivity(), "Teacher Successfully Updated.",ToastMsg.GRAVITY_TOP);
				
				FragmentAdminClasses fragmentAdminClasses=new FragmentAdminClasses();
				Bundle bundle=new Bundle();				
				FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
	    		FragmentTransaction fragmentTransaction = fragmentManager
	    				.beginTransaction();	 
	    		bundle.putString("Action", "Teachers");
				fragmentAdminClasses.setArguments(bundle);
	    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminClasses).addToBackStack(null).commit();
				
				
			} else {
				Toast.makeText(getActivity(), message, Toast.LENGTH_LONG)
						.show();
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
		}
	}
	public class DeleteTeacherApi extends AsyncTask<String,Integer,JSONObject> {

		ProgressDialog pd;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Deleting Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
			
		}
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			JSONParser parser = new JSONParser();

			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("TeacherId", params[0]));

			try {
				
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "teacher_delete.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("DeleteTeacherApi", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				
				ToastMsg.showSuccess(getActivity(), "Teacher Successfully Deleted.", ToastMsg.GRAVITY_TOP);
				
				FragmentAdminClasses fragmentAdminClasses=new FragmentAdminClasses();
				Bundle bundle=new Bundle();				
				FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
	    		FragmentTransaction fragmentTransaction = fragmentManager
	    				.beginTransaction();	 
	    		bundle.putString("Action", "Teachers");
				fragmentAdminClasses.setArguments(bundle);
	    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminClasses).addToBackStack(null).commit();
	    		
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}
	class GetingTeacherDetailsApi extends AsyncTask<String, String, JSONObject> {
		JSONObject jsonobject;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... transaction) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();
			try {
				List<NameValuePair> postParams = new ArrayList<NameValuePair>();
				postParams.add(new BasicNameValuePair("TeacherId",transaction[0]));
								
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "teacher_details.php",
						postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("teacher_details", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				if (result != null) {
					JSONObject jsonobject=new JSONObject();
					jsonobject=result.getJSONObject("Data");
					Log.e("",jsonobject+"");
					mEditTextUserName.setText(jsonobject.getString("UserName"));
					mEditTextEmail.setText(jsonobject.getString("Email"));
					mEditTextPhoneNo.setText(jsonobject.getString("PhoneNo"));
				} else {
					Log.e("teacher_details", "Server encontered problem try again later");
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			super.onPostExecute(result);
		}
	}
	public class RegisterTeacherApi extends AsyncTask<String,Integer,JSONObject> {

		ProgressDialog pd;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Registering please wait....");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();

		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("UserName", params[0]));
			postParams.add(new BasicNameValuePair("CollegeId", params[1]));
			postParams.add(new BasicNameValuePair("UserTypeId", params[2]));
			postParams.add(new BasicNameValuePair("Password", params[3]));
			postParams.add(new BasicNameValuePair("Email", params[4]));
			postParams.add(new BasicNameValuePair("PhoneNo", params[5]));
		
			try {
				
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "teacher_create.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("RegisterTeacherApi", "Error : " + e.toString());
				return null;
			}
		}

		/**
		 * execution of result
		 */
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("JSON",result+"");
				if (result != null) {
					processRegistration(result);
				} else {
					Log.e("Registration", "Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}
	
	private void processRegistration(JSONObject result){
		
		if(result.has("Status") && result.has("Message")){
			try {
				String message = result.getString("Message");
				if(result.getInt("Status") == 1){
					ToastMsg.showSuccess(getActivity(),
							message, ToastMsg.GRAVITY_TOP);
					FragmentAdminClasses fragmentAdminClasses=new FragmentAdminClasses();
					Bundle bundle=new Bundle();				
					FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		    		FragmentTransaction fragmentTransaction = fragmentManager
		    				.beginTransaction();	 
		    		bundle.putString("Action", "Teachers");
					fragmentAdminClasses.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminClasses).commit();  
				}else{
					ToastMsg.showError(getActivity(),
							message, ToastMsg.GRAVITY_TOP);
					
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("Registration", "Registration error");
			}
		}else{
			ToastMsg.showError(getActivity(),
					"Unable to Register", ToastMsg.GRAVITY_TOP);
		}
	}
	private boolean notBlank(EditText edit){
		if(edit.getText().length() > 0){
			edit.setError(null);
			return true;
		}else{
			edit.setError("Required " + edit.getHint());
			return false;
		}
	}
}
