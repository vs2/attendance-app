package com.vs2.easyacademics.fragments;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.adapters.ClassListAdapter;
import com.vs2.easyacademics.fragments.FragmentAdminClasses.GetingClassListApi;
import com.vs2.easyacademics.objects.ClassMaster;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.ToastMsg;

public class FragmentClassList extends SherlockFragment {

	private View mView;
	private ActionBar mActionbar;
	private ListView mListviewClasses;
	public String classid , classname,action;
	private ClassListAdapter mAdapter;
	private ClassMaster mClassMasterList;
	ArrayList<ClassMaster> arry_classmaster;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_classlist, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		mActionbar.setTitle("Classes");		
		initGloble();
		Bundle arguments = getArguments();
		if (arguments != null){
			action=arguments.getString("Action");		
		}else{
			
		}
		return mView;
	}
		
	public void initGloble(){
		mListviewClasses=(ListView)mView.findViewById(R.id.listview_classlist);
		arry_classmaster=new ArrayList<ClassMaster>();			
		new GetingClassListApi().execute(Global.sUserMaster.getTermid()+"",Global.sUserMaster.getCollegeid()+"");
		
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();	 

		mListviewClasses.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub

				TextView text1 = (TextView) arg1.findViewById(R.id.text_classid);
				TextView class_name = (TextView) arg1.findViewById(R.id.text_classname);
				classid = text1.getText().toString();
				classname= class_name.getText().toString();
				
				if(action.equals("Take Attendance")){
					FragmentSubjectList fragmentSubjectList=new FragmentSubjectList();
					Bundle bundle=new Bundle();
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					bundle.putString("Action", action);
					fragmentSubjectList.setArguments(bundle);					
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentSubjectList);
		    		fragmentTransaction.addToBackStack(null);
		    		fragmentTransaction.commit();				
					
				}else if(action.equals("Manage Students")){
					FragmentClassProfile fragmentClassProfile=new FragmentClassProfile();
					Bundle bundle=new Bundle();
					bundle.putString("Class ID", classid);
					bundle.putString("Action", action);
					bundle.putString("Class Name", classname);
		    		fragmentClassProfile.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentClassProfile).addToBackStack(null).commit();
					
				}else if(action.equals("Attendance Report")){
					FragmentSubjectList fragmentSubjectList=new FragmentSubjectList();
					Bundle bundle=new Bundle();
					bundle.putString("Class ID", classid);
					bundle.putString("Action", action);
					bundle.putString("Class Name", classname);
					fragmentSubjectList.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentSubjectList).addToBackStack(null).commit();
				}else if(action.equals("Assignments")){
					FragmentSubjectList fragmentSubjectList=new FragmentSubjectList();
					Bundle bundle=new Bundle();
					bundle.putString("Class ID", classid);
					bundle.putString("Action", action);
					bundle.putString("Class Name", classname);
					fragmentSubjectList.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentSubjectList).addToBackStack(null).commit();
					
				}else if(action.equals("Student Details")){
					Bundle bundle = new Bundle();
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					bundle.putString("Action", action);
					FragmentStudentList fragmentStudentList = new FragmentStudentList();
					fragmentStudentList.setArguments(bundle);
					fragmentTransaction.replace(R.id.frame_layout, fragmentStudentList)
							.addToBackStack(null).commit();
				}else if(action.equals("Add Students")){
					
					howToAddStudent("How to Add Students Details?");
					
				}else if(action.equals("Assignment Report")){
					FragmentSubjectList fragmentSubjectList=new FragmentSubjectList();
					Bundle bundle=new Bundle();
					bundle.putString("Class ID", classid);
					bundle.putString("Action", action);
					bundle.putString("Class Name", classname);
					fragmentSubjectList.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentSubjectList).addToBackStack(null).commit();
					
				}else if(action.equals("Student Report")){
					Bundle bundle = new Bundle();
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					bundle.putString("Action", action);
					FragmentStudentList fragmentStudentList = new FragmentStudentList();
					fragmentStudentList.setArguments(bundle);
					fragmentTransaction.replace(R.id.frame_layout, fragmentStudentList)
							.addToBackStack(null).commit();
				}
				
				
			
			}
		});
		
	
	}	
	
	class GetingClassListApi extends AsyncTask<String, String, JSONObject> {
		JSONObject jsonobject;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... transaction) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();
			try {
				List<NameValuePair> postParams = new ArrayList<NameValuePair>();
				postParams.add(new BasicNameValuePair("TermId",transaction[0]));
				postParams.add(new BasicNameValuePair("CollegeId",transaction[1]));
								
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "class_list.php",
						postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("Class List", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject resultTransaction) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				
				if (resultTransaction != null) {
					JSONArray jat = resultTransaction.getJSONArray("Data");
					Log.e("Transactionlist length ", jat.length() + "");				
					for(int i = 0; i < jat.length(); i++)
		   			{
						jsonobject=jat.getJSONObject(i);
						mClassMasterList=new ClassMaster(jsonobject.getInt("Id"), jsonobject.getString("ClassName"));
						arry_classmaster.add(mClassMasterList);
		   			} 
					if (mAdapter == null) {
						mAdapter=new ClassListAdapter(getActivity(), arry_classmaster);
						mListviewClasses.setAdapter(mAdapter);
					}else{
						//mAdapter.notifyDataSetChanged();
						mListviewClasses.setAdapter(mAdapter);
					}

					
				} else{
					ToastMsg.showError(getActivity(),
							"Result is null", ToastMsg.GRAVITY_TOP);
				}			
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			super.onPostExecute(resultTransaction);
		}
	}
	public void howToAddStudent(String message) {

		FragmentManager fragmentManager = getActivity()
				.getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Choose way");
		builder.setMessage(message);
		builder.setPositiveButton("Upload file", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				showDialog();
			}
		});
		builder.setNegativeButton("Insert Details", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Bundle bundle = new Bundle();
				bundle.putString("Class ID", classid);
				bundle.putString("Class Name", classname);
				bundle.putString("Student", "Save");
				bundle.putString("Action", "profile");
				FragmentAddStudents fragmentAddStudents = new FragmentAddStudents();
				fragmentAddStudents.setArguments(bundle);
				fragmentTransaction
						.replace(R.id.frame_layout, fragmentAddStudents)
						.addToBackStack(null).commit();
			}
		});
		builder.show();
		
	}
	public void showDialog() {
		LayoutInflater adbInflater = LayoutInflater.from(getActivity());
		View agreeView = adbInflater.inflate(R.layout.view_of_xls_format, null);
		FragmentManager fragmentManager = getActivity()
				.getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("xls Format View");
		builder.setView(agreeView);
		builder.setMessage("You must need to upload xls file like this format");
		builder.setPositiveButton("Continue",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Bundle bundle = new Bundle();
						bundle.putString("Class ID", classid);
						bundle.putString("Student", "Upload");
						bundle.putString("Action", "profile");
						bundle.putString("Class Name", classname);
						FragmentAddStudents fragmentAddStudents = new FragmentAddStudents();
						fragmentAddStudents.setArguments(bundle);
						fragmentTransaction
								.replace(R.id.frame_layout, fragmentAddStudents)
								.addToBackStack(null).commit();
					}
				});
		builder.setNegativeButton("No,Thanks", null);
		builder.show();
	}
	
	public class DeleteClassApi extends AsyncTask<String,Integer,JSONObject> {

		ProgressDialog pd;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Erasing Class Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
			
		}
		
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			JSONParser parser = new JSONParser();

			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("classname", params[0]));

			try {
				
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "class_delete.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("class_delete", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				mAdapter.notifyDataSetChanged();
				ToastMsg.showSuccess(getActivity(), "Class Successfully Deleted.", ToastMsg.GRAVITY_TOP);
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}
	}
	
	public class UpdateClassApi extends AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Updating Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("classid", params[0]));
			postParams.add(new BasicNameValuePair("classname", params[1]));
			postParams.add(new BasicNameValuePair("userid", params[2]));
			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "class_update.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("class_update", "Error : " + e.toString());
				return null;
			}
		}
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				if (result != null) {
					processLogin(result);
				} else {
					Log.e( "class_update",
							"Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processLogin(JSONObject result) {
		try {
			String message = result.getString("Message");
			if (result.getInt("ClassDetails") == 1) {
				
				ToastMsg.showSuccess(getActivity(), "Class Successfully Updated.", ToastMsg.GRAVITY_TOP);
			} else {
				Toast.makeText(getActivity(), message, Toast.LENGTH_LONG)
						.show();
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e("Update", "Update error"+e.toString());
		}
	}
}
