package com.vs2.easyacademics.fragments;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.adapters.StudentListDetailsAdapter;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.StudentMaster;
import com.vs2.easyacademics.objects.ToastMsg;

public class FragmentStudentList extends SherlockFragment {

	private View mView;
	private ActionBar mActionbar;
	private ListView mListviewStudents;
	public String classid , stude_id,action;
	private StudentListDetailsAdapter mAdapter;
	private StudentMaster mStudentMasterList;
	ArrayList<StudentMaster> arry_studentmaster;
	private EditText mEditTextSearch;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_student_list, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		Bundle bundle=new Bundle();
		bundle = getArguments();
		classid = bundle.getString("Class ID");
		action=bundle.getString("Action");	
		mActionbar.setTitle("Student List");
		initGloble();
		return mView;
	}
	public void initGloble(){
		mListviewStudents=(ListView)mView.findViewById(R.id.listview_studentlist);
		mEditTextSearch=(EditText)mView.findViewById(R.id.edittext_search);
		arry_studentmaster=new ArrayList<StudentMaster>();
		new GetingStudentListApi().execute(classid);
		if(action.equals("Student Report")){
			mEditTextSearch.setVisibility(View.VISIBLE);
		}
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		final Bundle bundle=new Bundle();
		mListviewStudents.setOnItemClickListener(new OnItemClickListener() {
			
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			TextView text_stude_id = (TextView) arg1.findViewById(R.id.text_sid);
			stude_id = text_stude_id.getText().toString();
			if(action.equals("Student Report")){
				FragmentStudentReport fragmentStudentReport=new FragmentStudentReport();
				bundle.putString("Student ID",stude_id);
				bundle.putString("Class ID", classid);
				bundle.putString("Action",action);
				fragmentStudentReport.setArguments(bundle);
	    		fragmentTransaction.replace(R.id.frame_layout, fragmentStudentReport).addToBackStack(null).commit();
				
			}else{
				
				FragmentAddStudents fragmentAddStudents=new FragmentAddStudents();
				bundle.putString("Student ID",stude_id);
				bundle.putString("Class ID", classid);
				bundle.putString("Student","Delete");
	    		fragmentAddStudents.setArguments(bundle);
	    		fragmentTransaction.replace(R.id.frame_layout, fragmentAddStudents).addToBackStack(null).commit();
			}
			}
		});
		
	}	
	class GetingStudentListApi extends AsyncTask<String, String, JSONObject> {
		JSONObject jsonobject;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... transaction) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();
			try {
				List<NameValuePair> postParams = new ArrayList<NameValuePair>();
				postParams.add(new BasicNameValuePair("ClassId",transaction[0]));
								
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "student_list.php",
						postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("student_list", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				
				if (result != null) {
					JSONArray jat = result.getJSONArray("Data");
					for(int i = 0; i < jat.length(); i++)
		   			{
						jsonobject=jat.getJSONObject(i);
						mStudentMasterList=new StudentMaster(jsonobject.getInt("Id"),jsonobject.getString("RollNo"), jsonobject.getString("StudentName"), jsonobject.getString("ContactNo"), jsonobject.getString("Address"), jsonobject.getString("Email"));
						arry_studentmaster.add(mStudentMasterList);
												
		   			} 
		   			if (mAdapter == null) {
							mAdapter=new StudentListDetailsAdapter(getActivity(), arry_studentmaster);
							mListviewStudents.setAdapter(mAdapter);
							
						}else{
							//mAdapter.notifyDataSetChanged();
							mListviewStudents.setAdapter(mAdapter);
						}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			super.onPostExecute(result);
		}
	}
		
}
