package com.vs2.easyacademics.fragments;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.adapters.ClassListAdapter;
import com.vs2.easyacademics.fragments.FragmentSubjectList.DeleteSubjectApi;
import com.vs2.easyacademics.fragments.FragmentSubjectList.UpdateSubjectApi;
import com.vs2.easyacademics.objects.ClassMaster;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.ToastMsg;
import com.vs2.easyacademics.objects.Utility;

public class FragmentAdminClasses extends SherlockFragment {

	
	private View mView;
	private ActionBar mActionbar;
	String action,classid,classname;
	private Button mButtonAddClass;
	private ListView mListViewClasses;
	
	private ClassListAdapter mAdapter;
	private ClassMaster mClassMasterList;
	ArrayList<ClassMaster> arry_classmaster;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_admin_classes, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		mActionbar.setTitle("Classes");
		initiGloble();
		return mView;
		
	}
	public void initiGloble() {
		Bundle bundle = new Bundle();
		bundle = getArguments();
		action = bundle.getString("Action");
		arry_classmaster=new ArrayList<ClassMaster>();
		
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		
		mListViewClasses=(ListView)mView.findViewById(R.id.listview_adminclasslist);
		mButtonAddClass=(Button)mView.findViewById(R.id.button_admin_add_class);
		
		if(action.equals("Students")){
			mButtonAddClass.setVisibility(View.GONE);
			new GetingClassListApi().execute(Global.sUserMaster.getTermid()+"",Global.sUserMaster.getCollegeid()+"");
		}else if(action.equals("Teachers")){
			mActionbar.setTitle("Teachers");
			mButtonAddClass.setText("Create New Teacher");
			new GetingTeacherListApi().execute(Global.sUserMaster.getCollegeid()+"");
			
		}else if(action.equals("Classes")){
			mButtonAddClass.setText("Add Class");
			new GetingClassListApi().execute(Global.sUserMaster.getTermid()+"",Global.sUserMaster.getCollegeid()+"");
		}else if(action.equals("Subjects")){
			mButtonAddClass.setVisibility(View.GONE);
			new GetingClassListApi().execute(Global.sUserMaster.getTermid()+"",Global.sUserMaster.getCollegeid()+"");
		}else if(action.equals("AssignmentsAction")){
			mButtonAddClass.setVisibility(View.GONE);
			new GetingClassListApi().execute(Global.sUserMaster.getTermid()+"",Global.sUserMaster.getCollegeid()+"");
		}
		mListViewClasses.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				TextView text1 = (TextView) view.findViewById(R.id.text_classid);
				TextView class_name = (TextView) view.findViewById(R.id.text_classname);
				classid = text1.getText().toString();
				classname= class_name.getText().toString();
				Bundle bundle1 = new Bundle();
				if(action.equals("Classes")){
					FragmentAdminAction fragmentAdminAction=new FragmentAdminAction();
					bundle1.putString("Action", action);
					bundle1.putString("Class ID", classid);
					bundle1.putString("Class Name", classname);
					fragmentAdminAction.setArguments(bundle1);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminAction).addToBackStack(null).commit();
					
				}else if(action.equals("Students")){
					FragmentAdminAction fragmentAdminAction=new FragmentAdminAction();
					bundle1.putString("Action", action);
					bundle1.putString("Class ID", classid);
					bundle1.putString("Class Name", classname);
					fragmentAdminAction.setArguments(bundle1);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminAction).addToBackStack(null).commit();
					
				}else if(action.equals("Teachers")){
					
					FragmentRegisterTeacher fragmentRegisterTeacher=new FragmentRegisterTeacher();
					bundle1.putString("Action", "Update");
					bundle1.putString("User ID", classid);
					bundle1.putString("Class ID", classid);
					bundle1.putString("Class Name", classname);
					fragmentRegisterTeacher.setArguments(bundle1);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentRegisterTeacher).addToBackStack(null).commit();
				}else if(action.equals("Subjects")){
					FragmentAdminAction fragmentAdminAction=new FragmentAdminAction();
					bundle1.putString("Action", action);
					bundle1.putString("Class ID", classid);
					bundle1.putString("Class Name", classname);
					fragmentAdminAction.setArguments(bundle1);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminAction).addToBackStack(null).commit();
					
				}else if(action.equals("AssignmentsAction")){
					bundle1.putString("Action", action);
		    		bundle1.putString("Class ID", classid);
		    		bundle1.putString("Class Name", classname);
					FragmentSubjectList fragmentSubjectList = new FragmentSubjectList();
					fragmentSubjectList.setArguments(bundle1);
					fragmentTransaction
							.replace(R.id.frame_layout, fragmentSubjectList)
							.addToBackStack(null).commit();
					
				}
			}
		});
		
		mButtonAddClass.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(action.equals("Teachers")){
					FragmentRegisterTeacher fragmentRegisterTeacher=new FragmentRegisterTeacher();
					Bundle bundle1 = new Bundle();
					bundle1.putString("Action", "New");
					fragmentRegisterTeacher.setArguments(bundle1);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentRegisterTeacher).addToBackStack(null).commit();
					
				}else{
					
					showDialog();
				}
				
			}
		});
		
		mListViewClasses.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					final int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(Global.sUserMaster.getUsertypeid()==1){
					
					if(action.equals("Teachers")){
						
					}else
					{
					TextView text1 = (TextView) arg1.findViewById(R.id.text_classid);
					final TextView class_name = (TextView) arg1.findViewById(R.id.text_classname);
					classid = text1.getText().toString();
					classname= class_name.getText().toString();
					AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setTitle("Select Your Choice");
					builder.setMessage("You Want to Update your Class or delete?");
					builder.setPositiveButton("Update",new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							
							LayoutInflater adbInflater = LayoutInflater.from(getActivity());
							View agreeView = adbInflater.inflate(R.layout.new_class, null);
							final EditText edittextClassName=(EditText)agreeView.findViewById(R.id.edittext_classname);
							//final Button buttonAdd=(Button)agreeView.findViewById(R.id.button_addclass);
							edittextClassName.setText(classname);
							AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
							builder.setTitle("Update Class Name");
							builder.setView(agreeView);				
							builder.setPositiveButton("Update",new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									//arry_subjectmaster.get(arg2).setSubjectname(edittextClassName.getText().toString());
									class_name.setText(edittextClassName.getText().toString());
									new UpdateClassNameApi().execute(classid,edittextClassName.getText().toString());
									arry_classmaster.get(arg2).setClassname(edittextClassName.getText().toString());
								}
							});
							builder.setNegativeButton("No",null );
							builder.show();	
						}
					});
					builder.setNegativeButton("Delete",new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
							builder.setTitle("Are you sure?");
							builder.setMessage("You will loss all data for this class !");
							builder.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									
									new DeleteClassApi().execute(classid);
									arry_classmaster.remove(arg2);
								}
							});
							builder.setNegativeButton("No",new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									
								}
							} );
							builder.show();	
							
						}
					} );
					builder.show();	
					}
				}
				return false;
			}
		});
			
	}
	public class DeleteClassApi extends AsyncTask<String,Integer,JSONObject> {

		ProgressDialog pd;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Deleting Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
			
		}
		
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			JSONParser parser = new JSONParser();

			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("ClassId", params[0]));

			try {
				
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "class_delete.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("class_delete", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				mAdapter.notifyDataSetChanged();
				ToastMsg.showSuccess(getActivity(), "Class Successfully Deleted.", ToastMsg.GRAVITY_TOP);
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}
	}
	public class UpdateClassNameApi extends AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Updating Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("ClassId", params[0]));
			postParams.add(new BasicNameValuePair("ClassName", params[1]));
			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "class_update.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("subject_update", "Error : " + e.toString());
				return null;
			}
		}
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				if (result != null) {
					processUpdate(result);
				} else {
					Log.e( "subject_update",
							"Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processUpdate(JSONObject result) {
		try {
			
			String message = result.getString("Message");
			if (result.getInt("Status") == 1) {
				
				ToastMsg.showSuccess(getActivity(), "Class Successfully Updated.", ToastMsg.GRAVITY_TOP);

			} else {
				Toast.makeText(getActivity(), message, Toast.LENGTH_LONG)
						.show();
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e("Update", "Update error"+e.toString());
		}
	}
	public void showDialog() {
		LayoutInflater adbInflater = LayoutInflater.from(getActivity());
		View agreeView = adbInflater.inflate(R.layout.new_class, null);
		final EditText edittextClassName = (EditText) agreeView
				.findViewById(R.id.edittext_classname);
		edittextClassName.setHint("Enter Class Name");
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Add New Class");
		builder.setView(agreeView);
		builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				if (edittextClassName.getText().toString().equals("")) {
					ToastMsg.showError(getActivity(),
							"You Must Enter Class Name",
							ToastMsg.GRAVITY_CENTER);
				} else {

					if (Utility.isOnline(getActivity())) {

						new CreateClassApi().execute(Global.sUserMaster.getUserid()+"",edittextClassName.getText().toString(),Global.sUserMaster.getTermid()+"",Global.sUserMaster.getCollegeid()+"");
					} else {
						Log.e("No connection", "No internet connection found!");
						ToastMsg.showError(getActivity(),
								"No internet connection found!",
								ToastMsg.GRAVITY_CENTER);
					}

				}

			}
		});
		builder.setNegativeButton("No", null);
		builder.show();
	}
	public class CreateClassApi extends AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Creating Class please wait....");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();

		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("UserId", params[0]));
			postParams.add(new BasicNameValuePair("ClassName", params[1]));
			postParams.add(new BasicNameValuePair("TermId", params[2]));
			postParams.add(new BasicNameValuePair("CollegeId", params[3]));

			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "class_create.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("CreateClassApi", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("JSON", result + "");
				if (result != null) {
					processCreateClass(result);
				} else {
					Log.e("Create Class",
							"Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processCreateClass(JSONObject result) {

		if (result.has("Status") && result.has("Message")) {
			try {
				if (result.getInt("Status") == 1) {
					ToastMsg.showSuccess(getActivity(),
							"Class Successfully Created", ToastMsg.GRAVITY_TOP);
					arry_classmaster.clear();
					new GetingClassListApi().execute(Global.sUserMaster.getTermid()+"",Global.sUserMaster.getCollegeid()+"");
				} else {
					ToastMsg.showError(getActivity(),
							"Class Creation Error", ToastMsg.GRAVITY_TOP);

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("processCreateClass", "processCreateClass error");
			}
		} else {
			Toast.makeText(getActivity(), "Unable to Create a Class",
					Toast.LENGTH_LONG).show();
		}
	}
	
	class GetingClassListApi extends AsyncTask<String, String, JSONObject> {
		JSONObject jsonobject;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... transaction) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();
			try {
				List<NameValuePair> postParams = new ArrayList<NameValuePair>();
				postParams.add(new BasicNameValuePair("TermId",transaction[0]));
				postParams.add(new BasicNameValuePair("CollegeId",transaction[1]));
								
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "class_list.php",
						postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("Class List", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				Log.e("RESULT",result+"");
				
				if (result != null) {
					JSONArray jat = result.getJSONArray("Data");
					for(int i = 0; i < jat.length(); i++)
		   			{
						jsonobject=jat.getJSONObject(i);
						mClassMasterList=new ClassMaster(jsonobject.getInt("Id"), jsonobject.getString("ClassName"));
						arry_classmaster.add(mClassMasterList);
		   			} 
					//if (mAdapter == null) {
						mAdapter=new ClassListAdapter(getActivity(), arry_classmaster);
						mListViewClasses.setAdapter(mAdapter);
						
				//	}else{
						mAdapter.notifyDataSetChanged();
					//}
				}else{
					ToastMsg.showError(getActivity(),
							"Result is null", ToastMsg.GRAVITY_TOP);
				}
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			super.onPostExecute(result);
		}
	}
	class GetingTeacherListApi extends AsyncTask<String, String, JSONObject> {
		JSONObject jsonobject;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... transaction) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();
			try {
				List<NameValuePair> postParams = new ArrayList<NameValuePair>();
				postParams.add(new BasicNameValuePair("CollegeId",transaction[0]));
								
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "teacher_list.php",
						postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("Teacher List", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				Log.e("RESULT",result+"");
				
				if (result != null) {
					JSONArray jat = result.getJSONArray("Data");
					for(int i = 0; i < jat.length(); i++)
		   			{
						jsonobject=jat.getJSONObject(i);
						mClassMasterList=new ClassMaster(jsonobject.getInt("Id"), jsonobject.getString("UserName"));
						arry_classmaster.add(mClassMasterList);
		   			} 
					
				//	if (mAdapter == null) {
						mAdapter=new ClassListAdapter(getActivity(), arry_classmaster);
						mListViewClasses.setAdapter(mAdapter);
						
				//	}else{
						mAdapter.notifyDataSetChanged();
			//		}
					
				}else{
					ToastMsg.showError(getActivity(),
							"Result is null", ToastMsg.GRAVITY_TOP);
				}
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			super.onPostExecute(result);
		}
	}
}
