package com.vs2.easyacademics.fragments;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.adapters.SubjcetListAdapter;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.SubjectMaster;
import com.vs2.easyacademics.objects.ToastMsg;
import com.vs2.easyacademics.objects.Utility;

public class FragmentSubjectList extends SherlockFragment {


	private View mView;
	private ActionBar mActionbar;
	private ListView mListviewSubjects;
	public String classid,subjetid,subjectname,action,classname;
	private SubjcetListAdapter mAdapter;
	private SubjectMaster mSubjectMasterrList;
	ArrayList<SubjectMaster> arry_subjectmaster;
	private Button mButtonAddSubject;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_subject_list, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		Bundle bundle=new Bundle();
		bundle = getArguments();
		classid = bundle.getString("Class ID");
		classname = bundle.getString("Class Name");
		action = bundle.getString("Action");
		initiGlobe();
		return mView;
	}
	public void initiGlobe(){
		mButtonAddSubject=(Button)mView.findViewById(R.id.button_add_sub);
		mListviewSubjects=(ListView)mView.findViewById(R.id.listview_subjectlist);
		arry_subjectmaster=new ArrayList<SubjectMaster>();
		mActionbar.setTitle("Subjects");
		
		//if(Global.sUserMaster.getUserTypeid()==2){
			mButtonAddSubject.setVisibility(View.GONE);
	//	}
		
		new GetingSubjcetListApi().execute(classid,Global.sUserMaster.getUserid()+"");
		
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();	
		
		mButtonAddSubject.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				createNew();
			}
		});
		mListviewSubjects.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				TextView class_id = (TextView) arg1.findViewById(R.id.text_classid);
				final TextView class_name = (TextView) arg1.findViewById(R.id.text_classname);
				subjetid = class_id.getText().toString();
				subjectname= class_name.getText().toString();
				
				if(action.equals("Attendance Report")){
					FragmentAttendanceReports fragmentAttendanceReports=new FragmentAttendanceReports();
					Bundle bundle=new Bundle();
					bundle.putString("Subject ID", subjetid);
					bundle.putString("Class ID", classid);
					bundle.putString("Subject Name", subjectname);
					bundle.putString("Class Name", classname);
					bundle.putString("Action", action);
					fragmentAttendanceReports.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAttendanceReports).addToBackStack(null).commit();
					
				}else if(action.equals("Take Attendance")){
					FragmentTakeAttendance fragmentTakeAttendance=new FragmentTakeAttendance();
					Bundle bundle=new Bundle();
					bundle.putString("Subject ID", subjetid);
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					bundle.putString("Subject Name", subjectname);
					bundle.putString("Action", action);
		    		fragmentTakeAttendance.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentTakeAttendance).addToBackStack(null).commit();
					
				}else if(action.equals("Assignments")){
					Bundle bundle=new Bundle();
					FragmentClassProfile fragmentClassProfile=new FragmentClassProfile();
					bundle.putString("Subject ID", subjetid);
					bundle.putString("Class ID", classid);
					bundle.putString("Subject Name", subjectname);
					bundle.putString("Action", action);
					bundle.putString("Class Name", classname);
					fragmentClassProfile.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentClassProfile).addToBackStack(null).commit();
					
				}else if(action.equals("AssignmentsAction")){
					FragmentAdminAction fragmentAdminAction=new FragmentAdminAction();
					Bundle bundle=new Bundle();
					bundle.putString("Class Name", classname);
					bundle.putString("Subject Name", subjectname);
					bundle.putString("Action", "AssignmentsAction");
					bundle.putString("Subject ID", subjetid);
					fragmentAdminAction.setArguments(bundle);
		    		fragmentTransaction.replace(R.id.frame_layout, fragmentAdminAction).addToBackStack(null).commit();
					
				}else if(action.equals("Assignment Report")){
					Bundle bundle = new Bundle();
					FragmentAssignmentlist fragmentAssignmentlist = new FragmentAssignmentlist();
					bundle.putString("Subject ID", subjetid);
					bundle.putString("Class ID", classid);
					bundle.putString("Class Name", classname);
					bundle.putString("Subject Name", subjectname);
					bundle.putString("Action", "Manage Assignments");
					fragmentAssignmentlist.setArguments(bundle);
					fragmentTransaction.replace(R.id.frame_layout, fragmentAssignmentlist)
							.addToBackStack(null).commit();
				}
			}
		});
		mListviewSubjects.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					final int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(Global.sUserMaster.getUsertypeid()==1){
					TextView class_id = (TextView) arg1.findViewById(R.id.text_classid);
					final TextView class_name = (TextView) arg1.findViewById(R.id.text_classname);
					subjetid = class_id.getText().toString();
					subjectname= class_name.getText().toString();
					AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					builder.setTitle("Select Your Choice");
					builder.setMessage("You Want to Update your subject or delete?");
					builder.setPositiveButton("Update",new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							FragmentCreateSubject fragmentCreateSubject=new FragmentCreateSubject();
							Bundle bundle=new Bundle();
							bundle.putString("Action", "Update");
							bundle.putString("Class ID", classid);
							bundle.putString("Class Name", classname);
							bundle.putString("Subject ID", subjetid);
							bundle.putString("Subject Name", subjectname);
							fragmentCreateSubject.setArguments(bundle);
				    		fragmentTransaction.replace(R.id.frame_layout, fragmentCreateSubject).addToBackStack(null).commit();
				
						}
					});
					builder.setNegativeButton("Delete",new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
							builder.setTitle("Confirmation");
							builder.setMessage("Are you sure?");
							builder.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									
									new DeleteSubjectApi().execute(subjetid);
									arry_subjectmaster.remove(arg2);
								}
							});
							builder.setNegativeButton("No",new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									
								}
							} );
							builder.show();	
							
						}
					} );
					builder.show();	
				}
				return false;
			}
		});
	}
	
	public void createNew() {
		LayoutInflater adbInflater = LayoutInflater.from(getActivity());
		View agreeView = adbInflater.inflate(R.layout.new_class, null);
		final EditText edittextClassName = (EditText) agreeView
				.findViewById(R.id.edittext_classname);
		edittextClassName.setHint("Enter Subject Name");
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Add New Class");
		builder.setView(agreeView);
		builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				if (edittextClassName.getText().toString().equals("")) {
					ToastMsg.showError(getActivity(),
							"You Must Enter Subject Name",
							ToastMsg.GRAVITY_CENTER);
				} else {

					if (Utility.isOnline(getActivity())) {

						new CreateClassApi().execute(classid,edittextClassName.getText().toString());
					} else {
						Log.e("No connection", "No internet connection found!");
						ToastMsg.showError(getActivity(),
								"No internet connection found!",
								ToastMsg.GRAVITY_CENTER);
					}

				}

			}
		});
		builder.setNegativeButton("No", null);
		builder.show();
	}
	public class CreateClassApi extends AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Creating Class please wait....");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();

		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("ClassId", params[0]));
			postParams.add(new BasicNameValuePair("SubjectName", params[1]));

			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "subject_create.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("CreateSubjectApi", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("JSON", result + "");
				if (result != null) {
					processCreateSubject(result);
				} else {
					Log.e("Create Subject",
							"Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processCreateSubject(JSONObject result) {

		if (result.has("Status") && result.has("Message")) {
			try {
				if (result.getInt("Status") == 1) {
					ToastMsg.showSuccess(getActivity(),
							"Subject Successfully Added", ToastMsg.GRAVITY_TOP);
					arry_subjectmaster.clear();
					new GetingSubjcetListApi().execute(classid);
				} else {
					ToastMsg.showError(getActivity(),
							"Subject Creation Error", ToastMsg.GRAVITY_TOP);

				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("processCreateSubject", "processCreateSubject error");
			}
		} else {
			Toast.makeText(getActivity(), "Unable to Create a Subject",
					Toast.LENGTH_LONG).show();
		}
	}
	
	public class DeleteSubjectApi extends AsyncTask<String,Integer,JSONObject> {

		ProgressDialog pd;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Deleting Subject Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
			
		}
		
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			JSONParser parser = new JSONParser();

			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("SubjectId", params[0]));

			try {
				
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "subject_delete1.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("subject_delete", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				mAdapter.notifyDataSetChanged();
				ToastMsg.showSuccess(getActivity(), "Subject Successfully Deleted.", ToastMsg.GRAVITY_TOP);
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}
	}
	
	public class UpdateSubjectApi extends AsyncTask<String, Integer, JSONObject> {

		ProgressDialog pd;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Updating Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}
		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub

			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("SubjectId", params[0]));
			postParams.add(new BasicNameValuePair("SubjectName", params[1]));
			postParams.add(new BasicNameValuePair("ClassId", params[2]));
			try {

				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "subject_update.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("subject_update", "Error : " + e.toString());
				return null;
			}
		}
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				if (result != null) {
					processUpdate(result);
				} else {
					Log.e( "subject_update",
							"Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}

	private void processUpdate(JSONObject result) {
		try {
			
			String message = result.getString("Message");
			if (result.getInt("Status") == 1) {
				
				ToastMsg.showSuccess(getActivity(), "Subject Successfully Updated.", ToastMsg.GRAVITY_TOP);

			} else {
				Toast.makeText(getActivity(), message, Toast.LENGTH_LONG)
						.show();
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e("Update", "Update error"+e.toString());
		}
	}
	class GetingSubjcetListApi extends AsyncTask<String, String, JSONObject> {
		JSONObject jsonobject;
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Loading Please wait...");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected JSONObject doInBackground(String... transaction) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();
			try {
				List<NameValuePair> postParams = new ArrayList<NameValuePair>();
				
//				
				//if(Global.sUserMaster.getUsertypeid()==2){
					postParams.add(new BasicNameValuePair("ClassId",transaction[0]));
					postParams.add(new BasicNameValuePair("UserId",transaction[1]));
									
					return parser.getJSONFromUrl(Global.SERVER_PATH
							+ "subject_list_by_assigned_teacher.php",
							postParams);
				/*}else{
					postParams.add(new BasicNameValuePair("ClassId",transaction[0]));
									
					return parser.getJSONFromUrl(Global.SERVER_PATH
							+ "subject_list.php",
							postParams);
				} */
				

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("Subject List", "Error : " + e.toString());
				return null;
			}
		}

		@Override
		protected void onPostExecute(JSONObject resultTransaction) {
			// TODO Auto-generated method stub
			pd.dismiss();
			try {
				JSONArray jat = resultTransaction.getJSONArray("Data");
				if (resultTransaction != null) {
					if(jat.length()>0){
					for(int i = 0; i < jat.length(); i++)
		   			{
						jsonobject=jat.getJSONObject(i);
						mSubjectMasterrList=new SubjectMaster(jsonobject.getInt("SubjectId"), jsonobject.getString("SubjectName"));
						arry_subjectmaster.add(mSubjectMasterrList);
					
		   			}
		   				if (mAdapter == null) {
							mAdapter=new SubjcetListAdapter(getActivity(), arry_subjectmaster);
							mListviewSubjects.setAdapter(mAdapter);
							
						}else{
							//mAdapter.notifyDataSetChanged();
							mListviewSubjects.setAdapter(mAdapter);
						} 
					}
					
				} 
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			super.onPostExecute(resultTransaction);
		}
	}
}
