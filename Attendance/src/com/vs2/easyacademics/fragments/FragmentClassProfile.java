package com.vs2.easyacademics.fragments;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.HomeActivity;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.ToastMsg;
import com.vs2.easyacademics.objects.Utility;

public class FragmentClassProfile extends SherlockFragment {

	private View mView;
	private ActionBar mActionbar;
	public String classid, classname, subjectid, action,subjectname;
	public int totalStudents;
	private LinearLayout mButtonTakeAttendance, mButtonStudentDetails,
			mButtonAttendanceReports, mButtonAddStudent, mButtonAssignment;
	private TextView mTextViewClassName, mTextViewTakeAttendance,
			mTextViewStudentDetails, mTextViewAttendanceReports,
			mTextViewAddStudent, mTextViewAssignment;

	private ImageView mImageViewTakeAttendance, mImageViewAssignment,
			mImageViewAttendanceReport, mImageViewAddStudents,
			mImageViewStudents;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_class_profile, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		Bundle bundle = new Bundle();
		bundle = getArguments();
		action = bundle.getString("Action");
		initGloble();
		mActionbar.setTitle("Class Profile");
		return mView;
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void initGloble() {

		mImageViewAddStudents = (ImageView) mView
				.findViewById(R.id.image_add_student);
		mImageViewAssignment = (ImageView) mView
				.findViewById(R.id.image_assignments);
		mImageViewAttendanceReport = (ImageView) mView
				.findViewById(R.id.image_attendace_reports);
		mImageViewStudents = (ImageView) mView
				.findViewById(R.id.image_students);
		mImageViewTakeAttendance = (ImageView) mView
				.findViewById(R.id.image_take_attendance);

		mTextViewClassName = (TextView) mView
				.findViewById(R.id.textview_classname);
		mTextViewTakeAttendance = (TextView) mView
				.findViewById(R.id.text_take_attendance);
		mTextViewAssignment = (TextView) mView
				.findViewById(R.id.text_assigments);
		mTextViewAttendanceReports = (TextView) mView
				.findViewById(R.id.text_attendance_report);
		mTextViewAddStudent = (TextView) mView
				.findViewById(R.id.text_add_student);
		mTextViewStudentDetails = (TextView) mView
				.findViewById(R.id.text_student_details);
		mButtonTakeAttendance = (LinearLayout) mView
				.findViewById(R.id.button_attendance);
		mButtonStudentDetails = (LinearLayout) mView
				.findViewById(R.id.button_students_details);
		mButtonAttendanceReports = (LinearLayout) mView
				.findViewById(R.id.button_attendace_reports);
		mButtonAddStudent = (LinearLayout) mView
				.findViewById(R.id.button_add_student);
		mButtonAssignment = (LinearLayout) mView
				.findViewById(R.id.button_assignment);
		mTextViewClassName.setText("Wel Come "
				+ Global.sUserMaster.getUsername());
	
		if (action.equals("Manage Students")) {
			mButtonTakeAttendance.setVisibility(View.GONE);
			mButtonAttendanceReports.setVisibility(View.GONE);
			mButtonAssignment.setVisibility(View.GONE);
			Bundle bundle = new Bundle();
			bundle = getArguments();
			classid = bundle.getString("Class ID");
			classname = bundle.getString("Class Name");
			mTextViewClassName.setText(classname);

		} else if (action.equals("Assignments")) {

			mTextViewAttendanceReports.setText("Create Assignment");
			mTextViewAddStudent.setText("Take Assignment");
			mTextViewStudentDetails.setText("Assignment Reports");
			Bundle bundle = new Bundle();
			bundle = getArguments();
			classid = bundle.getString("Class ID");
			subjectid = bundle.getString("Subject ID");
			classname = bundle.getString("Class Name");
			subjectname = bundle.getString("Subject Name");
			mImageViewAttendanceReport.setImageResource(R.drawable.assignments);
			mImageViewAddStudents.setImageResource(R.drawable.take_assignment);
			mImageViewStudents.setImageResource(R.drawable.mange_assignment);
			mButtonTakeAttendance.setVisibility(View.GONE);
			mButtonAssignment.setVisibility(View.GONE);
			mTextViewClassName.setText(classname+ " -> "+subjectname);

		}
		FragmentManager fragmentManager = getActivity()
				.getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		final FragmentClassList fragmentClassList = new FragmentClassList();
		mButtonAssignment.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				if (Utility.isOnline(getActivity())) {
					Bundle bundle = new Bundle();
					bundle.putString("Action", "Assignments");
					fragmentClassList.setArguments(bundle);
					fragmentTransaction
							.replace(R.id.frame_layout, fragmentClassList)
							.addToBackStack(null).commit();
				} else {
					Log.e("No connection", "No internet connection found!");
					ToastMsg.showError(getActivity(),
							"No internet connection found!",
							ToastMsg.GRAVITY_CENTER);
				}

			}
		});
		mButtonTakeAttendance.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (Utility.isOnline(getActivity())) {
					Bundle bundle = new Bundle();
					bundle.putString("Action", "Take Attendance");
					fragmentClassList.setArguments(bundle);
					fragmentTransaction
							.replace(R.id.frame_layout, fragmentClassList)
							.addToBackStack(null).commit();
				} else {
					Log.e("No connection", "No internet connection found!");
					ToastMsg.showError(getActivity(),
							"No internet connection found!",
							ToastMsg.GRAVITY_CENTER);
				}

				

			}
		});
		mButtonStudentDetails.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (Utility.isOnline(getActivity())) {
					if (action.equals("Assignments")) {
						Bundle bundle = new Bundle();
						FragmentAssignmentlist fragmentAssignmentlist = new FragmentAssignmentlist();
						bundle.putString("Subject ID", subjectid);
						bundle.putString("Class ID", classid);
						bundle.putString("Class Name", classname);
						bundle.putString("Subject Name", subjectname);
						bundle.putString("Action", "Manage Assignments");
						fragmentAssignmentlist.setArguments(bundle);
						fragmentTransaction.replace(R.id.frame_layout, fragmentAssignmentlist)
								.addToBackStack(null).commit();

					} else if (action.equals("Manage Students")) {
						Bundle bundle = new Bundle();
						bundle.putString("Class ID", classid);
						bundle.putString("Class Name", classname);
						FragmentStudentList fragmentStudentList = new FragmentStudentList();
						fragmentStudentList.setArguments(bundle);
						fragmentTransaction.replace(R.id.frame_layout, fragmentStudentList)
								.addToBackStack(null).commit();

					} else {

						Bundle bundle = new Bundle();
						bundle.putString("Action", "Student Details");
						fragmentClassList.setArguments(bundle);
						fragmentTransaction
								.replace(R.id.frame_layout, fragmentClassList)
								.addToBackStack(null).commit();
					}
				} else {
					Log.e("No connection", "No internet connection found!");
					ToastMsg.showError(getActivity(),
							"No internet connection found!",
							ToastMsg.GRAVITY_CENTER);
				}
				
			}
		});
		mButtonAttendanceReports.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				
				if (Utility.isOnline(getActivity())) {
					if (action.equals("Assignments")) {
						Bundle bundle = new Bundle();
						FragmentAssignment fragmentAssignment = new FragmentAssignment();
						bundle.putString("Action", action);
						bundle.putString("Class ID", classid);
						bundle.putString("Subject ID", subjectid);
						bundle.putString("Class Name", classname);
						bundle.putString("Subject Name", subjectname);
						fragmentAssignment.setArguments(bundle);
						fragmentTransaction
								.replace(R.id.frame_layout, fragmentAssignment)
								.addToBackStack(null).commit();

					} else {
						Bundle bundle = new Bundle();
						/*
						FragmentAllReports fragmentAllReports = new FragmentAllReports();
						bundle.putString("Action", "Reports");
						fragmentClassList.setArguments(bundle);
						fragmentTransaction
								.replace(R.id.frame_layout, fragmentAllReports)
								.addToBackStack(null).commit();*/
						bundle.putString("Action", "Attendance Report");
						fragmentClassList.setArguments(bundle);
						fragmentTransaction
								.replace(R.id.frame_layout, fragmentClassList)
								.addToBackStack(null).commit();
					}
				} else {
					Log.e("No connection", "No internet connection found!");
					ToastMsg.showError(getActivity(),
							"No internet connection found!",
							ToastMsg.GRAVITY_CENTER);
				}
			
			}
		});

		mButtonAddStudent.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (Utility.isOnline(getActivity())) {
					if (action.equals("Assignments")) {
						
						Bundle bundle = new Bundle();
						FragmentAssignmentlist fragmentAssignmentlist = new FragmentAssignmentlist();
						bundle.putString("Subject ID", subjectid);
						bundle.putString("Class ID", classid);
						bundle.putString("Class Name", classname);
						bundle.putString("Subject Name", subjectname);
						bundle.putString("Action", "Take Assignments");
						fragmentAssignmentlist.setArguments(bundle);
						fragmentTransaction.replace(R.id.frame_layout, fragmentAssignmentlist)
								.addToBackStack(null).commit();
						

					} else if (action.equals("Manage Students")) {
						howToAddStudent("How to Add Students Details?");

					} else {

						Bundle bundle = new Bundle();
						bundle.putString("Action", "Add Students");
						bundle.putString("Class ID", classid);
						bundle.putString("Class Name", classname);
						fragmentClassList.setArguments(bundle);
						fragmentTransaction
								.replace(R.id.frame_layout, fragmentClassList)
								.addToBackStack(null).commit();
					}
				} else {
					Log.e("No connection", "No internet connection found!");
					ToastMsg.showError(getActivity(),
							"No internet connection found!",
							ToastMsg.GRAVITY_CENTER);
				}
			
			}
		});

	}

	public void howToAddStudent(String message) {

		FragmentManager fragmentManager = getActivity()
				.getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Choose way");
		builder.setMessage(message);
		builder.setPositiveButton("Upload file",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						showDialog();
					}
				});
		builder.setNegativeButton("Insert Details",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Bundle bundle = new Bundle();
						bundle.putString("Class ID", classid);
						bundle.putString("Class Name", classname);
						bundle.putString("Student", "Save");
						bundle.putString("Action", "Manage Students");
						FragmentAddStudents fragmentAddStudents = new FragmentAddStudents();
						fragmentAddStudents.setArguments(bundle);
						fragmentTransaction
								.replace(R.id.frame_layout, fragmentAddStudents)
								.addToBackStack(null).commit();
					}
				});
		builder.show();
	}

	public void showDialog() {
		LayoutInflater adbInflater = LayoutInflater.from(getActivity());
		View agreeView = adbInflater.inflate(R.layout.view_of_xls_format, null);
		FragmentManager fragmentManager = getActivity()
				.getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("xls Format View");
		builder.setView(agreeView);
		builder.setMessage("You must need to upload xls file like this format");
		builder.setPositiveButton("Continue",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Bundle bundle = new Bundle();
						bundle.putString("Class ID", classid);
						bundle.putString("Class Name", classname);
						bundle.putString("Student", "Upload");
						bundle.putString("Action", "Manage Students");
						FragmentAddStudents fragmentAddStudents = new FragmentAddStudents();
						fragmentAddStudents.setArguments(bundle);
						fragmentTransaction
								.replace(R.id.frame_layout, fragmentAddStudents)
								.addToBackStack(null).commit();
					}
				});
		builder.setNegativeButton("No,Thanks", null);
		builder.show();
	}

}
