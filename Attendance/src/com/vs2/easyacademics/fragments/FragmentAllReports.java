package com.vs2.easyacademics.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.ToastMsg;
import com.vs2.easyacademics.objects.Utility;

public class FragmentAllReports extends SherlockFragment {
	
	private View mView;
	private ActionBar mActionbar;
	private LinearLayout mLayoutStudentReports,mLayoutAttendanceReports,mLayoutAssignmentReports;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.fragment_all_reports, null);
		mActionbar = getSherlockActivity().getSupportActionBar();
		mActionbar.setTitle("Reports");
		initiGloble();
		return mView;
	}
	public void initiGloble() {
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		final Bundle bundle = new Bundle();
		final FragmentClassList fragmentClassList = new FragmentClassList();
		
		mLayoutStudentReports=(LinearLayout)mView.findViewById(R.id.layout_student_reports);
		mLayoutAttendanceReports=(LinearLayout)mView.findViewById(R.id.layout_attendance_reports);
		mLayoutAssignmentReports=(LinearLayout)mView.findViewById(R.id.layout_Assignment_reports);
		
		mLayoutStudentReports.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (Utility.isOnline(getActivity())) {
					
					bundle.putString("Action", "Student Report");
					fragmentClassList.setArguments(bundle);
					fragmentTransaction
							.replace(R.id.frame_layout, fragmentClassList)
							.addToBackStack(null).commit();
				} else {
					Log.e("No connection", "No internet connection found!");
					ToastMsg.showError(getActivity(),
							"No internet connection found!",
							ToastMsg.GRAVITY_CENTER);
				}
			}
		});
		mLayoutAttendanceReports.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (Utility.isOnline(getActivity())) {
					
					bundle.putString("Action", "Attendance Report");
					fragmentClassList.setArguments(bundle);
					fragmentTransaction
							.replace(R.id.frame_layout, fragmentClassList)
							.addToBackStack(null).commit();
				} else {
					Log.e("No connection", "No internet connection found!");
					ToastMsg.showError(getActivity(),
							"No internet connection found!",
							ToastMsg.GRAVITY_CENTER);
				}
			}
		});
		mLayoutAssignmentReports.setOnClickListener(new View.OnClickListener() {
	
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (Utility.isOnline(getActivity())) {
					
					bundle.putString("Action", "Assignment Report");
					fragmentClassList.setArguments(bundle);
					fragmentTransaction
							.replace(R.id.frame_layout, fragmentClassList)
							.addToBackStack(null).commit();
				} else {
					Log.e("No connection", "No internet connection found!");
					ToastMsg.showError(getActivity(),
							"No internet connection found!",
							ToastMsg.GRAVITY_CENTER);
				}
			}
		});
		
	}
	
}
