package com.vs2.easyacademics.objects;

public class StudentReportMaster {

	int attendanceId,lectureNo;
	String subjectName,time,attend;
	
	public StudentReportMaster(int attendanceId, String subjectName,
			int lectureNo, String time, String attend) {
		super();
		this.attendanceId = attendanceId;
		this.subjectName = subjectName;
		this.lectureNo = lectureNo;
		this.time = time;
		this.attend = attend;
	}
	
	public int getAttendanceId() {
		return attendanceId;
	}
	public void setAttendanceId(int attendanceId) {
		this.attendanceId = attendanceId;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public int getLectureNo() {
		return lectureNo;
	}
	public void setLectureNo(int lectureNo) {
		this.lectureNo = lectureNo;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getAttend() {
		return attend;
	}
	public void setAttend(String attend) {
		this.attend = attend;
	}
	
	

}
