package com.vs2.easyacademics.objects;

public class AssignmentReportMaster {
	
	int id,StudentId;
	String submitDate,RollNo,StudentName;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStudentId() {
		return StudentId;
	}
	public void setStudentId(int studentId) {
		StudentId = studentId;
	}
	public String getSubmitDate() {
		return submitDate;
	}
	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate;
	}
	public String getRollNo() {
		return RollNo;
	}
	public void setRollNo(String rollNo) {
		RollNo = rollNo;
	}
	public String getStudentName() {
		return StudentName;
	}
	public void setStudentName(String studentName) {
		StudentName = studentName;
	}
	public AssignmentReportMaster(int id, int studentId, String submitDate,
			String rollNo, String studentName) {
		super();
		this.id = id;
		StudentId = studentId;
		this.submitDate = submitDate;
		RollNo = rollNo;
		StudentName = studentName;
	}
	public AssignmentReportMaster(int id,String submitDate, String rollNo,
			String studentName) {
		super();
		this.id = id;
		this.submitDate = submitDate;
		RollNo = rollNo;
		StudentName = studentName;
	}
	
}
