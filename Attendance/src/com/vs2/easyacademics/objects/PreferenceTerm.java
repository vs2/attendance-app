package com.vs2.easyacademics.objects;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceTerm {
	

	public static final String PREFS_TERM_SETTINGS = "TermMaster_Setting";
	
	public static final String KEY_TERM_ID = "TermId";
	public static final String KEY_TERM_VALUE = "TermValue";
	
	public static void clearPreference(Context context){
		SharedPreferences sharedPrefs = context.getSharedPreferences(PREFS_TERM_SETTINGS, 0);
		SharedPreferences.Editor editor = sharedPrefs.edit();
		editor.clear();
		editor.commit();
	}
	
	public static void setBooleanPref(String prefKey,Context context,boolean status){
		SharedPreferences settings = context.getSharedPreferences(PREFS_TERM_SETTINGS, 0);
	      SharedPreferences.Editor editor = settings.edit();
	      editor.putBoolean(prefKey, status);
	      editor.commit();
	}
	
	public static boolean getBooleanPref(String prefKey,Context context){
		SharedPreferences settings = context.getSharedPreferences(PREFS_TERM_SETTINGS, 0);
	       return settings.getBoolean(prefKey, false);
	}
	
	public static void setIntPref(String prefKey,Context context,int value){
		SharedPreferences settings = context.getSharedPreferences(PREFS_TERM_SETTINGS, 0);
	      SharedPreferences.Editor editor = settings.edit();
	      editor.putInt(prefKey, value);
	      editor.commit();
	}
	
	public static int getIntPref(String prefKey,Context context){
		SharedPreferences settings = context.getSharedPreferences(PREFS_TERM_SETTINGS, 0);
	    return settings.getInt(prefKey, 40);
	}
	
	public static void setStringPref(String prefKey,Context context,String value){
		SharedPreferences settings = context.getSharedPreferences(PREFS_TERM_SETTINGS, 0);
	      SharedPreferences.Editor editor = settings.edit();
	      editor.putString(prefKey, value);
	      editor.commit();
	}
	
	public static String getStringPref(String prefKey,Context context){
		SharedPreferences settings = context.getSharedPreferences(PREFS_TERM_SETTINGS, 0);
	    return settings.getString(prefKey,null);
	}
	
	
	
}
