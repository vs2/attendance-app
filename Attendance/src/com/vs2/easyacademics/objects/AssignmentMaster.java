package com.vs2.easyacademics.objects;

public class AssignmentMaster {
	
	int id;
	String assignmentName,givenDate,submitDate;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAssignmentName() {
		return assignmentName;
	}
	public void setAssignmentName(String assignmentName) {
		this.assignmentName = assignmentName;
	}
	public String getGivenDate() {
		return givenDate;
	}
	public void setGivenDate(String givenDate) {
		this.givenDate = givenDate;
	}
	public String getSubmitDate() {
		return submitDate;
	}
	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate;
	}
	public AssignmentMaster(int id, String assignmentName, String givenDate,
			String submitDate) {
		super();
		this.id = id;
		this.assignmentName = assignmentName;
		this.givenDate = givenDate;
		this.submitDate = submitDate;
	}
	
	
	
}
