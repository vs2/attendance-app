package com.vs2.easyacademics.objects;

/**
 * Represents  DrawerMenu of the application.
  */

public class DrawerMenu {
	
	
	/**
	   * The HOME,NEW_BUDGET,ACTIVE_BUDGET,TRANSACTIONS,USEFUL_LINKS,SETTINGS,CALCULATOR,HELP and LOGOUT are DrawerMenu Items.
	   */
	public static final int HOME = 1;
	public static final int TAKE_ATTENDANCE = 2;
	public static final int ASSIGNMENTS = 3;
	public static final int STUDENT_REPORTS = 4;
	public static final int VIEW_REPORTS = 5;
	public static final int SETTINGS = 6;
	public static final int LOGOUT = 7;
	
	/**
	   * The type,title and iconResource of the DrawerMenu Items.
	   */
	private int type;
	private String title;
	private int iconResource;
	
	/**
	 * Default Constructor 
	 */
	public DrawerMenu(){
		super();
	}
	
	/**
	   * Creates a new DrawerMenu with the given type and title.
	  
	   */
	public DrawerMenu(int type,String title){
		this.type = type;
		this.title = title;
	}
	
	/**
	   * Creates a new DrawerMenu with the given type,title and iconResource.
	  
	   */

	public DrawerMenu(int type,String title,int iconResource){
		this.type = type;
		this.title = title;
		this.iconResource = iconResource;
	}
	
	/**
	   * Gets the type of this DrawerMenu.
	   * @return this DrawerMenu's type.
	   */
	public int getType() {
		return type;
	}
	
	/**
	   * Changes the type of this DrawerMenu.
	   * This may involve a lengthy legal process.
	   * @param type This DrawerMenu's new type.  
	   */
	public void setType(int type) {
		this.type = type;
	}
	
	/**
	   * Gets the title of this DrawerMenu.
	   * @return this DrawerMenu's title.
	   */
	public String getTitle() {
		return title;
	}
	
	/**
	   * Changes the title of this DrawerMenu.
	   * This may involve a lengthy legal process.
	   * @param title This DrawerMenu's new title.  
	   */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	   * Gets the iconResource of this DrawerMenu.
	   * @return this DrawerMenu's iconResource.
	   */
	public int getIconResource() {
		return iconResource;
	}
	
	/**
	   * Changes the iconResource of this DrawerMenu.
	   * This may involve a lengthy legal process.
	   * @param iconResource This DrawerMenu's new iconResource.  
	   */
	public void setIconResource(int iconResource) {
		this.iconResource = iconResource;
	}
	
	/*@Override
	public String toString(){
		return this.title;
	}*/
	
}
