package com.vs2.easyacademics.objects;

public class ClassMaster {
	
	int classid;
	int userid;
	String classname;
	int termid;
	int collegeid;
	
	public ClassMaster(int classid, String classname,int termid,int collegeid) {
		this.classid = classid;
		this.classname = classname;
		this.termid=termid;
		this.collegeid=collegeid;
	}
	
	
	public ClassMaster(String classname) {
		super();
		this.classname = classname;
	}


	public ClassMaster() {
		// TODO Auto-generated constructor stub
	}


	public ClassMaster(int classid, String classname) {
		this.classid = classid;
		this.classname = classname;
	}


	public int getClassid() {
		return classid;
	}
	public void setClassid(int classid) {
		this.classid = classid;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getClassname() {
		return classname;
	}
	public void setClassname(String classname) {
		this.classname = classname;
	}
	
	public int getTermid() {
		return termid;
	}
	public void setTermid(int termid) {
		this.termid = termid;
	}
	public int getCollegeId() {
		return collegeid;
	}
	public void setCollegeId(int collegeid) {
		this.collegeid= collegeid;
	}

}
