package com.vs2.easyacademics.objects;

public class AttendenceMaster {

	public AttendenceMaster(String studname, String attend) {
		super();
		this.studname = studname;
		this.attend = attend;
	}

	public AttendenceMaster(String studname, int enrolmentno) {
		super();
		this.studname = studname;
		this.enrolmentno = enrolmentno;
	}

	int id,aid, sid;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	String studname, lectureNo;
	int enrolmentno;
	String attend;
	String time;
	public String getLectureNo() {
		return lectureNo;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public void setLectureNo(String lectureNo) {
		this.lectureNo = lectureNo;
	}

	public AttendenceMaster(int id,int aid, int sid, String studname,
			String lectureNo, int enrolmentno, String attend,String time) {
		super();
		this.id = id;
		this.aid = aid;
		this.sid = sid;
		this.studname = studname;
		this.lectureNo = lectureNo;
		this.enrolmentno = enrolmentno;
		this.attend = attend;
		this.time = time;
	}

	public AttendenceMaster(int aid, int sid, String studname, int enrolmentno,
			String attend) {
		super();
		this.aid = aid;
		this.sid = sid;
		this.studname = studname;
		this.enrolmentno = enrolmentno;
		this.attend = attend;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public int getAid() {
		return aid;
	}

	public void setAid(int aid) {
		this.aid = aid;
	}

	public String getStudname() {
		return studname;
	}

	public void setStudname(String studname) {
		this.studname = studname;
	}

	public int getEnrolmentno() {
		return enrolmentno;
	}

	public void setEnrolmentno(int enrolmentno) {
		this.enrolmentno = enrolmentno;
	}

	public String getAttend() {
		return attend;
	}

	public void setAttend(String attend) {
		this.attend = attend;
	}

	public AttendenceMaster(String studname, int enrolmentno, String attend) {
		super();
		this.studname = studname;
		this.enrolmentno = enrolmentno;
		this.attend = attend;
	}

	public AttendenceMaster(String attend) {
		super();
		this.attend = attend;
	}

	public AttendenceMaster(int enrolmentno) {
		super();
		this.enrolmentno = enrolmentno;
	}

}
