package com.vs2.easyacademics.objects;

public class StudentMaster {
	
	int studid;
	int classid;
	String rollno;
	String studentname;
	String contactno;
	String address;
	public String getSelection() {
		return selection;
	}

	public void setSelection(String selection) {
		this.selection = selection;
	}

	String email;
	String selection;

	public StudentMaster() {
		// TODO Auto-generated constructor stub
	}
	
	public StudentMaster(String rollno, String studentname) {
		super();
		this.rollno= rollno;
		this.studentname = studentname;
	}

	

	public StudentMaster(int studid, String rollno, String studentname,
			String selection) {
		super();
		this.studid = studid;
		this.rollno = rollno;
		this.studentname = studentname;
		this.selection = selection;
	}

	public StudentMaster(String rollno, String studentname,
			String contactno, String address, String email) {
		super();
		this.rollno = rollno;
		this.studentname = studentname;
		this.contactno = contactno;
		this.address = address;
		this.email = email;
	}
	
	public StudentMaster(int studid, String rollno, String studentname,
			String contactno, String address, String email) {
		super();
		this.studid = studid;
		this.rollno = rollno;
		this.studentname = studentname;
		this.contactno = contactno;
		this.address = address;
		this.email = email;
	}

	public int getStudid() {
		return studid;
	}

	public void setStudid(int studid) {
		this.studid = studid;
	}

	public int getClassid() {
		return classid;
	}

	public void setClassid(int classid) {
		this.classid = classid;
	}
	public String getRollno() {
		return rollno;
	}

	public void setRollno(String rollno) {
		this.rollno = rollno;
	}

	public String getStudentname() {
		return studentname;
	}

	public void setStudentname(String studentname) {
		this.studentname = studentname;
	}

	public String getContactno() {
		return contactno;
	}

	public void setContactno(String contactno) {
		this.contactno = contactno;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}



		
}
