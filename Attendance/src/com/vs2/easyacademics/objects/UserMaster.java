package com.vs2.easyacademics.objects;

import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class UserMaster {

	int userid;
	int usertypeid;
	String username;
	String password;
	String email;
	String phoneno;
	int collegeid;
	int termid;
	String collegename;
	String secretcode;
	

	public UserMaster(int userid, int usertypeid, String username,
			String password, String email, String phoneno, int collegeid,
			int termid, String collegename, String secretcode) {
		super();
		this.userid = userid;
		this.usertypeid = usertypeid;
		this.username = username;
		this.password = password;
		this.email = email;
		this.phoneno = phoneno;
		this.collegeid = collegeid;
		this.termid = termid;
		this.collegename = collegename;
		this.secretcode = secretcode;
	}

	public String getSecretcode() {
		return secretcode;
	}

	public void setSecretcode(String secretcode) {
		this.secretcode = secretcode;
	}

	public UserMaster() {
		// TODO Auto-generated constructor stub
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getUsertypeid() {
		return usertypeid;
	}

	public void setUsertypeid(int usertypeid) {
		this.usertypeid = usertypeid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public int getCollegeid() {
		return collegeid;
	}

	public void setCollegeid(int collegeid) {
		this.collegeid = collegeid;
	}

	public int getTermid() {
		return termid;
	}

	public void setTermid(int termid) {
		this.termid = termid;
	}

	public String getCollegename() {
		return collegename;
	}

	public void setCollegename(String collegename) {
		this.collegename = collegename;
	}
	
	public static boolean saveUserMaster(UserMaster usermaster,Context context){
		if(usermaster == null)return false;
		
		try {
			PreferenceUser.setIntPref(PreferenceUser.KEY_USER_ID, context, usermaster.getUserid());
			PreferenceUser.setIntPref(PreferenceUser.KEY_USER_TYPE_ID, context, usermaster.getUsertypeid());
			PreferenceUser.setStringPref(PreferenceUser.KEY_USER_NAME, context, usermaster.getUsername());
			PreferenceUser.setStringPref(PreferenceUser.KEY_EMAIL, context, usermaster.getEmail());
			PreferenceUser.setStringPref(PreferenceUser.KEY_PASSWORD, context, usermaster.getPassword());
			PreferenceUser.setStringPref(PreferenceUser.KEY_PHONE_NO, context, usermaster.getPhoneno());
			PreferenceUser.setIntPref(PreferenceUser.KEY_COLLEGE_ID, context, usermaster.getCollegeid());
			PreferenceUser.setIntPref(PreferenceUser.KEY_TERM_ID, context, usermaster.getTermid());
			PreferenceUser.setStringPref(PreferenceUser.KEY_COLLEGE_NAME, context, usermaster.getCollegename());
			PreferenceUser.setStringPref(PreferenceUser.KEY_SECRET_CODE, context, usermaster.getSecretcode());
			
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Save user", "Error : " + e.toString());
		}
		return true;
	}
	
	public static UserMaster getUserMaster(JSONObject json){
		try {
			
			/**
			 * Using users object Changes the users of this User
			 */
			Log.e("User Master: ", json.getInt("Id")+json.getInt("UserTypeId")+json.getString("UserName"));
			UserMaster userMaster = new UserMaster();
			if(json.has("Id")){
				userMaster.setUserid(json.getInt("Id"));
			}
			if(json.has("UserTypeId")){
				userMaster.setUsertypeid(json.getInt("UserTypeId"));
			}
			if(json.has("UserName")){
				userMaster.setUsername(json.getString("UserName"));
			}
			if(json.has("Email")){
				userMaster.setEmail(json.getString("Email"));
			}
			
			if(json.has("Password")){
				userMaster.setPassword(json.getString("Password"));
			}
			if(json.has("PhoneNo")){
				userMaster.setPhoneno(json.getString("PhoneNo"));
			}
			if(json.has("CollegeId")){
				userMaster.setCollegeid(json.getInt("CollegeId"));
			}
			if(json.has("TermId")){
				userMaster.setTermid(json.getInt("TermId"));
			}
			if(json.has("CollegeName")){
				userMaster.setCollegename(json.getString("CollegeName"));
			}
			if(json.has("SecretCode")){
				userMaster.setSecretcode(json.getString("SecretCode"));
			}
			return userMaster;

		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		
	}
public static boolean exists(Context context) {
		
		if(PreferenceUser.getStringPref(PreferenceUser.KEY_USER_NAME, context) != null 
				&& PreferenceUser.getStringPref(PreferenceUser.KEY_USER_NAME, context).length() > 0){
			return true;
		}else{
			return false;
		}
	}
public static UserMaster getSavedUser(Context context) {
	try {
		
		UserMaster userMaster = new UserMaster();
		userMaster.setUserid(PreferenceUser.getIntPref(PreferenceUser.KEY_USER_ID, context));
		userMaster.setUsertypeid(PreferenceUser.getIntPref(PreferenceUser.KEY_USER_TYPE_ID, context));
		userMaster.setUsername(PreferenceUser.getStringPref(PreferenceUser.KEY_USER_NAME, context));
		userMaster.setEmail(PreferenceUser.getStringPref(PreferenceUser.KEY_EMAIL, context));
		userMaster.setPassword(PreferenceUser.getStringPref(PreferenceUser.KEY_PASSWORD, context));
		userMaster.setPhoneno(PreferenceUser.getStringPref(PreferenceUser.KEY_PHONE_NO, context));
		userMaster.setCollegeid(PreferenceUser.getIntPref(PreferenceUser.KEY_COLLEGE_ID, context));
		userMaster.setTermid(PreferenceUser.getIntPref(PreferenceUser.KEY_TERM_ID, context));
		userMaster.setCollegename(PreferenceUser.getStringPref(PreferenceUser.KEY_COLLEGE_NAME, context));
		userMaster.setSecretcode(PreferenceUser.getStringPref(PreferenceUser.KEY_SECRET_CODE, context));
						
		return userMaster;
	} catch (Exception e) {
		// TODO: handle exception
		Log.e("Get saved user", "Error : " + e.toString());
		return null;
	}
	
}

}
