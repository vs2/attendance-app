package com.vs2.easyacademics.objects;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;

public class Utility {
	
	public static String strRollNumbers;
	public static String strNotAttendRollNumbers;
	public static String strTeachersId;
	public static String strTeachersName;
	
	public static boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		return (cm == null || cm.getActiveNetworkInfo() == null) ? false : cm
				.getActiveNetworkInfo().isConnectedOrConnecting();
	}

	@SuppressLint("SimpleDateFormat")
	public static String getFormatedDate(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return dateFormat.format(date);
	}

	public static void alert(Context context, String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setMessage("" + message);
		builder.setNeutralButton("OK", null);
		builder.create().show();
	}
		

	public static String getFormatedDate(String dateTime) {
		long millisec = Long.parseLong(dateTime);
		Date now = new Date(millisec);
		SimpleDateFormat format = new SimpleDateFormat("MMM d, ''yy h:mm a",
				Locale.ENGLISH);
		return format.format(now);
	}
	
	// Make string for attendance

public static void AddRollNoInAttendance(String original,String no)
	{	
		if(original=="")
		{
			original=no+",";
		}
		else
		{			
			original=original+no+",";
			String [] arrayOriginal= original.split(",");
			for(int i=0;i<arrayOriginal.length;i++)
			{
				System.out.println(arrayOriginal[i]+":");
			}
			for(int i=0;i<arrayOriginal.length;i++)
			{
				for(int j=i;j<arrayOriginal.length;j++)
				{
					if(arrayOriginal[i].compareTo(arrayOriginal[j])>0)
					{
						String temp=arrayOriginal[i];
						arrayOriginal[i]=arrayOriginal[j];
						arrayOriginal[j]=temp;
					}
				}
			}
			original="";
			for(int i=0;i<arrayOriginal.length;i++)
			{
				original=original+arrayOriginal[i]+",";
				System.out.println(arrayOriginal[i]+":");
			}
		}
		Utility.strRollNumbers= original;
	}
	
//remove roll no 
	public static void RemoveRollNoInAttendance(String original,String no)
	{
		String [] arrayOriginal=null;
		if(original=="")
		{
			original="";
		}else
		{
			arrayOriginal= original.split(",");
			for(int i=0;i<arrayOriginal.length;i++)
			{
				//System.out.println(arrayOriginal[i]+":");
				if(arrayOriginal[i].compareTo(no)==0)
				{
					for(int j=i;j<arrayOriginal.length-1;j++)
					{
						arrayOriginal[j]=arrayOriginal[j+1];
					}			
				}
			}
		}
		original="";
		for(int i=0;i<arrayOriginal.length-1;i++)
		{
			original=original+arrayOriginal[i]+",";			
			//System.out.println(arrayOriginal[i]+":");
		}
		Utility.strRollNumbers= original;
	}
	
	public static void AddRollNoInNotAttendAttendance(String original,String no)
	{	
		if(original=="")
		{
			original=no+",";
		}
		else
		{			
			original=original+no+",";
			String [] arrayOriginal= original.split(",");
			for(int i=0;i<arrayOriginal.length;i++)
			{
				System.out.println(arrayOriginal[i]+":");
			}
			for(int i=0;i<arrayOriginal.length;i++)
			{
				for(int j=i;j<arrayOriginal.length;j++)
				{
					if(arrayOriginal[i].compareTo(arrayOriginal[j])>0)
					{
						String temp=arrayOriginal[i];
						arrayOriginal[i]=arrayOriginal[j];
						arrayOriginal[j]=temp;
					}
				}
			}
			original="";
			for(int i=0;i<arrayOriginal.length;i++)
			{
				original=original+arrayOriginal[i]+",";
				System.out.println(arrayOriginal[i]+":");
			}
		}
		Utility.strNotAttendRollNumbers= original;
	}
	
	public static void RemoveRollNoInNotAttendAttendance(String original,String no)
	{	
		String [] arrayOriginal=null;
		if(original=="")
		{
			original="";
		}else
		{
			arrayOriginal= original.split(",");
			for(int i=0;i<arrayOriginal.length;i++)
			{
				//System.out.println(arrayOriginal[i]+":");
				if(arrayOriginal[i].compareTo(no)==0)
				{
					for(int j=i;j<arrayOriginal.length-1;j++)
					{
						arrayOriginal[j]=arrayOriginal[j+1];
					}			
				}
			}
		}
		original="";
		for(int i=0;i<arrayOriginal.length-1;i++)
		{
			original=original+arrayOriginal[i]+",";			
			//System.out.println(arrayOriginal[i]+":");
		}
		Utility.strNotAttendRollNumbers= original;
	}
	
	public static void AddTeacherId(String original,String no)
	{	
		if(original=="")
		{
			original=no+",";
		}
		else
		{			
			original=original+no+",";
			String [] arrayOriginal= original.split(",");
			for(int i=0;i<arrayOriginal.length;i++)
			{
				System.out.println(arrayOriginal[i]+":");
			}
			for(int i=0;i<arrayOriginal.length;i++)
			{
				for(int j=i;j<arrayOriginal.length;j++)
				{
					if(arrayOriginal[i].compareTo(arrayOriginal[j])>0)
					{
						String temp=arrayOriginal[i];
						arrayOriginal[i]=arrayOriginal[j];
						arrayOriginal[j]=temp;
					}
				}
			}
			original="";
			for(int i=0;i<arrayOriginal.length;i++)
			{
				original=original+arrayOriginal[i]+",";
				System.out.println(arrayOriginal[i]+":");
			}
		}
		Utility.strTeachersId= original;
	}
	
//remove roll no 
	public static void RemoveTeacherId(String original,String no)
	{
		String [] arrayOriginal=null;
		if(original=="")
		{
			original="";
		}else
		{
			arrayOriginal= original.split(",");
			for(int i=0;i<arrayOriginal.length;i++)
			{
				//System.out.println(arrayOriginal[i]+":");
				if(arrayOriginal[i].compareTo(no)==0)
				{
					for(int j=i;j<arrayOriginal.length-1;j++)
					{
						arrayOriginal[j]=arrayOriginal[j+1];
					}			
				}
			}
		}
		original="";
		for(int i=0;i<arrayOriginal.length-1;i++)
		{
			original=original+arrayOriginal[i]+",";			
			//System.out.println(arrayOriginal[i]+":");
		}
		Utility.strTeachersId= original;
	}
	public static void AddTeacherName(String original,String no)
	{	
		if(original=="")
		{
			original=no+",";
		}
		else
		{			
			original=original+no+",";
			String [] arrayOriginal= original.split(",");
			for(int i=0;i<arrayOriginal.length;i++)
			{
				System.out.println(arrayOriginal[i]+":");
			}
			for(int i=0;i<arrayOriginal.length;i++)
			{
				for(int j=i;j<arrayOriginal.length;j++)
				{
					if(arrayOriginal[i].compareTo(arrayOriginal[j])>0)
					{
						String temp=arrayOriginal[i];
						arrayOriginal[i]=arrayOriginal[j];
						arrayOriginal[j]=temp;
					}
				}
			}
			original="";
			for(int i=0;i<arrayOriginal.length;i++)
			{
				original=original+arrayOriginal[i]+",";
				System.out.println(arrayOriginal[i]+":");
			}
		}
		Utility.strTeachersName= original;
	}
	
//remove roll no 
	public static void RemoveTeacherName(String original,String no)
	{
		String [] arrayOriginal=null;
		if(original=="")
		{
			original="";
		}else
		{
			arrayOriginal= original.split(",");
			for(int i=0;i<arrayOriginal.length;i++)
			{
				//System.out.println(arrayOriginal[i]+":");
				if(arrayOriginal[i].compareTo(no)==0)
				{
					for(int j=i;j<arrayOriginal.length-1;j++)
					{
						arrayOriginal[j]=arrayOriginal[j+1];
					}			
				}
			}
		}
		original="";
		for(int i=0;i<arrayOriginal.length-1;i++)
		{
			original=original+arrayOriginal[i]+",";			
			//System.out.println(arrayOriginal[i]+":");
		}
		Utility.strTeachersName= original;
	}
}
