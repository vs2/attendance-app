package com.vs2.easyacademics.objects;

public class ShowReport {
	
	String lectureNo,totalPresent,totalAbsent,bunked,joined;

	public String getLectureNo() {
		return lectureNo;
	}

	public void setLectureNo(String lectureNo) {
		this.lectureNo = lectureNo;
	}

	public String getTotalPresent() {
		return totalPresent;
	}

	public void setTotalPresent(String totalPresent) {
		this.totalPresent = totalPresent;
	}

	public String getTotalAbsent() {
		return totalAbsent;
	}

	public void setTotalAbsent(String totalAbsent) {
		this.totalAbsent = totalAbsent;
	}

	public String getBunked() {
		return bunked;
	}

	public void setBunked(String bunked) {
		this.bunked = bunked;
	}

	public String getJoined() {
		return joined;
	}

	public void setJoined(String joined) {
		this.joined = joined;
	}

	public ShowReport(String lectureNo, String totalPresent,
			String totalAbsent, String bunked, String joined) {
		super();
		this.lectureNo = lectureNo;
		this.totalPresent = totalPresent;
		this.totalAbsent = totalAbsent;
		this.bunked = bunked;
		this.joined = joined;
	}

	public ShowReport(String lectureNo, String totalPresent, String totalAbsent) {
		super();
		this.lectureNo = lectureNo;
		this.totalPresent = totalPresent;
		this.totalAbsent = totalAbsent;
	}
	
}
