package com.vs2.easyacademics.objects;

public class SubjectMasterDetails {
	int id,subid;
	int classid;
	String subjectname;
	
	public SubjectMasterDetails(String subjectname) {
		super();
		this.subjectname = subjectname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public SubjectMasterDetails(int subid, String subjectname) {
		super();
		this.subid = subid;
		this.subjectname = subjectname;
	}

	public SubjectMasterDetails(int id, int subid, String subjectname) {
		super();
		this.subid = subid;
		this.id = id;
		this.subjectname = subjectname;
	}

	public SubjectMasterDetails() {
		// TODO Auto-generated constructor stub
	}
	public int getSubid() {
		return subid;
	}
	public void setSubid(int subid) {
		this.subid = subid;
	}
	public int getClassid() {
		return classid;
	}
	public void setClassid(int classid) {
		this.classid = classid;
	}
	public String getSubjectname() {
		return subjectname;
	}
	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}
	
}
