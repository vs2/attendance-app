package com.vs2.easyacademics.objects;

public class FullReport {
	int studeId,rollNo;
	String studentName,totalLecture,attendLecture;
	
	
	public FullReport(int studeId, int rollNo, String studentName,
			String totalLecture, String attendLecture) {
		super();
		this.studeId = studeId;
		this.rollNo = rollNo;
		this.studentName = studentName;
		this.totalLecture = totalLecture;
		this.attendLecture = attendLecture;
	}
	public int getStudeId() {
		return studeId;
	}
	public void setStudeId(int studeId) {
		this.studeId = studeId;
	}
	public int getRollNo() {
		return rollNo;
	}
	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getTotalLecture() {
		return totalLecture;
	}
	public void setTotalLecture(String totalLecture) {
		this.totalLecture = totalLecture;
	}
	public String getAttendLecture() {
		return attendLecture;
	}
	public void setAttendLecture(String attendLecture) {
		this.attendLecture = attendLecture;
	}
	
	
}
