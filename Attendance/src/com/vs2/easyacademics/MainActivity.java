package com.vs2.easyacademics;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.ToastMsg;
import com.vs2.easyacademics.objects.UserMaster;
import com.vs2.easyacademics.objects.Utility;

public class MainActivity extends Activity {

	private Button mButtonLogin,mButtonRegistration,mButtonForgetPassword;
	private EditText mEdittextUname,mEdittextPass;
	ArrayList<UserMaster> arry_usermaster;
	int MYID;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		
		mButtonLogin=(Button)findViewById(R.id.button_login);
		mButtonRegistration=(Button)findViewById(R.id.button_registration);
		mButtonForgetPassword=(Button)findViewById(R.id.button_forgetpass);
		mEdittextUname=(EditText)findViewById(R.id.edittext_username);
		mEdittextPass=(EditText)findViewById(R.id.edittext_password);
		
		if(UserMaster.exists(MainActivity.this)){
			
			Global.sUserMaster = UserMaster.getSavedUser(MainActivity.this);
			startActivity(new Intent(MainActivity.this,HomeActivity.class));
			finish();
		}
		
		mButtonLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*Intent intent = new Intent(MainActivity.this,HomeActivity.class);
				startActivity(intent);*/
				if(notBlank(mEdittextUname) && notBlank(mEdittextPass)){

					if (Utility.isOnline(MainActivity.this)) {
						
						new LoginApi().execute(mEdittextUname.getText().toString().trim(),
								mEdittextPass.getText().toString().trim());
					} else {
						Log.e( "No connection", "No internet connection found!");
						ToastMsg.showError(MainActivity.this,"No internet connection found!", ToastMsg.GRAVITY_CENTER);
					}

				}			
			}
				
			});
		mButtonRegistration.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(),
                        RegistrationActivity.class);
                startActivity(i);
			}
		});
		mButtonForgetPassword.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(),
                        ForgetPasswordActivity.class);
                startActivity(i);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	public class LoginApi extends AsyncTask<String,Integer,JSONObject> {

		ProgressDialog pd;
		/**
		 * Things to be done before execution 
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			
			pd = new ProgressDialog(MainActivity.this);
			pd.setMessage("Logging please wait....");
			pd.setIndeterminate(false);
			pd.setCancelable(true);
			pd.show();

		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			JSONParser parser = new JSONParser();

			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("UserName", params[0]));
			postParams.add(new BasicNameValuePair("Password", params[1]));
			try {
				
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "user_sign_in.php",postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("LoginApi", "Error : " + e.toString());
				return null;
			}
		}

		
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("",result+"");
				if (result != null) {
					processLogin(result);
				} else {
					Log.e("Login", "Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}
	
	private void processLogin(JSONObject result){
		if(result.has("Status") && result.has("Message")){
			try {
				if(result.getInt("Status") == 1){
					Global.sUserMaster = UserMaster.getUserMaster(result.getJSONObject("Data"));					
					UserMaster.saveUserMaster(Global.sUserMaster,MainActivity.this);
					
					Intent intent = new Intent(MainActivity.this,HomeActivity.class);
					startActivity(intent);
					finish();
				}else{
					ToastMsg.showError(MainActivity.this,"Wrong password or username", ToastMsg.GRAVITY_CENTER);
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("Login","Login error");
			}
		}else{
			//Toast.makeText(MainActivity.this, "Unable to Login", Toast.LENGTH_LONG).show();
			ToastMsg.showError(MainActivity.this,"Unable to Login", ToastMsg.GRAVITY_CENTER);
		}
	}
	private boolean notBlank(EditText edit){
		if(edit.getText().length() > 0){
			edit.setError(null);
			return true;
		}else{
			edit.setError("Required " + edit.getHint());
			return false;
		}
	}
	
}
