package com.vs2.easyacademics;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.PreferenceUser;
import com.vs2.easyacademics.objects.ToastMsg;
import com.vs2.easyacademics.objects.UserMaster;
import com.vs2.easyacademics.objects.Utility;

public class RegistrationActivity extends Activity {

	private EditText mEdittextUsername,mEdittextPassword,mEdittextRePasswrod,mEdittextEmail,mEdittextCollegeName,mEdittextContactNo;
	private Button mButtonSubmit;
	String[] spinnerList;
	Spinner mSpinneUserTyper;
	String userType;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_registration);
		initGloble();
		
	}
	
	public void initGloble(){
		spinnerList = new String[3];
		spinnerList[0] = "Select Type";		
		spinnerList[1] = "Admin";
		spinnerList[2] = "Teacher";
		
		mSpinneUserTyper = (Spinner)findViewById(R.id.spinner_user_type);
		mEdittextUsername=(EditText)findViewById(R.id.edittext_username);
		mEdittextPassword=(EditText)findViewById(R.id.edittext_password);
		mEdittextRePasswrod=(EditText)findViewById(R.id.edittext_repassword);
		mEdittextEmail=(EditText)findViewById(R.id.edittext_emailid);
		mEdittextCollegeName=(EditText)findViewById(R.id.edittext_college_name);
		mEdittextContactNo=(EditText)findViewById(R.id.edittext_contactno);
		mButtonSubmit=(Button)findViewById(R.id.button_submit);
		
		ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(RegistrationActivity.this, R.layout.spinner_item, spinnerList);
		spinnerAdapter
				.setDropDownViewResource(android.R.layout.simple_list_item_1);
		mSpinneUserTyper.setAdapter(spinnerAdapter);
		
		mSpinneUserTyper.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if (mSpinneUserTyper.getSelectedItem().equals("Teacher")) {
					mEdittextCollegeName.setHint("Enter Secret Code");

				} else if (mSpinneUserTyper.getSelectedItem().equals("Admin")) {
					mEdittextCollegeName.setHint("College Name");
				}else{
					mEdittextCollegeName.setHint("First Select Type");
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		mButtonSubmit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Calendar c = Calendar.getInstance();
				int year = c.get(Calendar.YEAR);	
				if (mSpinneUserTyper.getSelectedItem().equals("Select Type")) {
					ToastMsg.showError(RegistrationActivity.this,
							"Please First Select your Sign Up Type...\n Thank You..", ToastMsg.GRAVITY_CENTER);
				}else{
				
				if(notBlank(mEdittextUsername) && notBlank(mEdittextPassword)
						&& notBlank(mEdittextRePasswrod) && notBlank(mEdittextEmail) && notBlank(mEdittextCollegeName)){
					
					if (Utility.isOnline(RegistrationActivity.this)) {
						
						if(mEdittextPassword.getText().toString().equals(mEdittextRePasswrod.getText().toString())){
							
							if(validEmail( mEdittextEmail.getText().toString())){
								new RegisterApi().execute(mEdittextUsername.getText().toString(),
										mEdittextCollegeName.getText().toString(),mSpinneUserTyper.getSelectedItemId()+"",
										mEdittextPassword.getText().toString(), mEdittextEmail.getText().toString(),
										mEdittextContactNo.getText().toString(),year+"");
							}else{
								ToastMsg.showError(RegistrationActivity.this,
										"Invalid Email", ToastMsg.GRAVITY_TOP);
							}
														
						
						}else{
							ToastMsg.showError(RegistrationActivity.this,
									"Password not Match", ToastMsg.GRAVITY_TOP);
						}
					} else {
						Log.e( "No connection", "No internet connection found!");
						ToastMsg.showError(RegistrationActivity.this,"No internet connection found!", ToastMsg.GRAVITY_CENTER);
					}
					
				}
				}
				
			}
		});
		
		mEdittextRePasswrod.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				
				  if (!hasFocus) {
					  if(mEdittextPassword.getText().toString().equals(mEdittextRePasswrod.getText().toString())){
						
						}else{
							ToastMsg.showError(RegistrationActivity.this,
									"Password not Match", ToastMsg.GRAVITY_TOP);
						}
				      }
				
			}
		});
		
		
	}
	 public boolean validEmail(String emailAddress)
     {
             
             if( Build.VERSION.SDK_INT >= 8 )
               {
                 return android.util.Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches();
               }

               Pattern pattern;
               Matcher matcher;
               String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)(\\.[A-Za-z]{2,})$";
               pattern = Pattern.compile(EMAIL_PATTERN);
               matcher = pattern.matcher(emailAddress);

               return matcher.matches();
     }
	public class RegisterApi extends AsyncTask<String,Integer,JSONObject> {

		ProgressDialog pd;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = new ProgressDialog(RegistrationActivity.this);
			pd.setMessage("Registering please wait....");
			pd.setIndeterminate(false);
			pd.setCancelable(false);
			pd.show();

		}

		@Override
		protected JSONObject doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			JSONParser parser = new JSONParser();
			List<NameValuePair> postParams = new ArrayList<NameValuePair>();
			postParams.add(new BasicNameValuePair("UserName", params[0]));
			postParams.add(new BasicNameValuePair("CollegeName", params[1]));
			postParams.add(new BasicNameValuePair("UserTypeId", params[2]));
			postParams.add(new BasicNameValuePair("Password", params[3]));
			postParams.add(new BasicNameValuePair("Email", params[4]));
			postParams.add(new BasicNameValuePair("PhoneNo", params[5]));
			postParams.add(new BasicNameValuePair("TermValue", params[6]));
		
			try {
				
				return parser.getJSONFromUrl(Global.SERVER_PATH
						+ "user_sign_up_and_create_college.php", postParams);

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("RegistrationApi", "Error : " + e.toString());
				return null;
			}
		}

		/**
		 * execution of result
		 */
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				Log.e("JSON",result+"");
				if (result != null) {
					processRegistration(result);
				} else {
					Log.e("Registration", "Server encontered problem try again later");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

	}
	
	private void processRegistration(JSONObject result){
		
		if(result.has("Status") && result.has("Message")){
			try {
				String message = result.getString("Message");
				if(result.getInt("Status") == 1){
					PreferenceUser.clearPreference(RegistrationActivity.this);	
					Global.sUserMaster = UserMaster.getUserMaster(result.getJSONObject("Data"));										
					UserMaster.saveUserMaster(Global.sUserMaster,RegistrationActivity.this);
					Intent intent = new Intent(RegistrationActivity.this,HomeActivity.class);
					startActivity(intent);
					finish();
				}else{
					ToastMsg.showError(RegistrationActivity.this,
							message, ToastMsg.GRAVITY_TOP);
					
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("Registration", "Registration error");
			}
		}else{
			ToastMsg.showError(RegistrationActivity.this,
					"Unable to Register", ToastMsg.GRAVITY_TOP);
		}
	}
	private boolean notBlank(EditText edit){
		if(edit.getText().length() > 0){
			edit.setError(null);
			return true;
		}else{
			edit.setError("Required " + edit.getHint());
			return false;
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration, menu);
		return true;
	}
}
