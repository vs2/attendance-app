package com.vs2.easyacademics.adapters;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.AttendenceMaster;

public class AttendanceListAdapter extends BaseAdapter {
	@SuppressWarnings("unused")
	private Context context;
	 private ArrayList<AttendenceMaster> mAttendenceMasterList;
	 private LayoutInflater mInflater;
	int counter1=0,counter2=0;
	public AttendanceListAdapter(Context context,ArrayList<AttendenceMaster>  entries) {
		
		// TODO Auto-generated constructor stub
		this.context=context;
		this.mAttendenceMasterList=entries;
		this.mInflater=LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mAttendenceMasterList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mAttendenceMasterList.get(position);
	}
	
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	
	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
       final ViewHolder holder;
        
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.attendance_list_item, null);
            holder = new ViewHolder();
            holder.BGLayout = (RelativeLayout) convertView.findViewById(R.id.layout_view);
            holder.textenrolmentno = (TextView) convertView.findViewById(R.id.text_attend_enrol_no);
            holder.textStudId = (TextView) convertView.findViewById(R.id.text_attend_stud_id);
            holder.textStudentName = (TextView) convertView.findViewById(R.id.text_attend_stud_name);
            holder.textAttend = (TextView) convertView.findViewById(R.id.text_attend_view);
            holder.textAttendId=(TextView)convertView.findViewById(R.id.text_attend_attend_id);
            convertView.setTag(holder);
        } else
        	
        holder = (ViewHolder) convertView.getTag();
        
        holder.textenrolmentno.setText(mAttendenceMasterList.get(position).getEnrolmentno()+") ");
        holder.textStudId.setText(mAttendenceMasterList.get(position).getSid()+"");
        holder.textAttendId.setText(mAttendenceMasterList.get(position).getId()+"");
        holder.textStudentName.setText(mAttendenceMasterList.get(position).getStudname());
        if(mAttendenceMasterList.get(position).getAttend().equals("Present")){
        	//holder.BGLayout.setBackgroundResource(R.color.absent);
       // 	holder.textenrolmentno.setTextColor(context.getResources().getColor(R.color.absent));
       // 	holder.textStudentName.setTextColor(context.getResources().getColor(R.color.absent));
      //  	holder.textAttend.setTextColor(context.getResources().getColor(R.color.absent));
        	holder.textAttend.setBackgroundResource(R.color.absent);
        }else{
        	//holder.BGLayout.setBackgroundResource(R.color.present);
     //   	holder.textenrolmentno.setTextColor(context.getResources().getColor(R.color.present));
      //  	holder.textStudentName.setTextColor(context.getResources().getColor(R.color.present));
      //  	holder.textAttend.setTextColor(context.getResources().getColor(R.color.present));
        	holder.textAttend.setBackgroundResource(R.color.present);
        }
        
        holder.textAttend.setText(""+mAttendenceMasterList.get(position).getAttend());
        
        
        return convertView;
    }
	
	private class ViewHolder {
		RelativeLayout BGLayout;
        TextView textStudentName;
        TextView textenrolmentno;
        TextView textAttend,textStudId,textAttendId;
       
               
    }
	

}
