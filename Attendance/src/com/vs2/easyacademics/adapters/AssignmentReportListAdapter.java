package com.vs2.easyacademics.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.AssignmentMaster;
import com.vs2.easyacademics.objects.AssignmentReportMaster;

public class AssignmentReportListAdapter extends BaseAdapter {
	@SuppressWarnings("unused")
	private Context context;
	 private ArrayList<AssignmentReportMaster> mAssignmentMasterList;
	 private LayoutInflater mInflater;
	
	public AssignmentReportListAdapter(Context context,ArrayList<AssignmentReportMaster>  entries) {
		
		// TODO Auto-generated constructor stub
		this.context=context;
		this.mAssignmentMasterList=entries;
		this.mInflater=LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mAssignmentMasterList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mAssignmentMasterList.get(position);
	}
	
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
       final ViewHolder holder;
        
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_assignments_report, null);
            holder = new ViewHolder();
            holder.textId = (TextView) convertView.findViewById(R.id.text_assignment_report_id);
            holder.textName = (TextView) convertView.findViewById(R.id.text_assignment_report_stud_name_no);
            holder.textSubmitDate = (TextView) convertView.findViewById(R.id.text_text_assignment_report_submit_date);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
 
        holder.textName.setText(mAssignmentMasterList.get(position).getRollNo()+") "+mAssignmentMasterList.get(position).getStudentName());
        holder.textSubmitDate.setText("    Submited Date: "+mAssignmentMasterList.get(position).getSubmitDate());
        holder.textId.setText(""+mAssignmentMasterList.get(position).getId());
       
        return convertView;
    }
	
	private class ViewHolder {
        TextView textId,textName,textSubmitDate;
       
               
    }
	

}
