package com.vs2.easyacademics.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.AssignmentMaster;

public class AssignmentListAdapter extends BaseAdapter {
	@SuppressWarnings("unused")
	private Context context;
	 private ArrayList<AssignmentMaster> mAssignmentMasterList;
	 private LayoutInflater mInflater;
	
	public AssignmentListAdapter(Context context,ArrayList<AssignmentMaster>  entries) {
		
		// TODO Auto-generated constructor stub
		this.context=context;
		this.mAssignmentMasterList=entries;
		this.mInflater=LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mAssignmentMasterList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mAssignmentMasterList.get(position);
	}
	
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
       final ViewHolder holder;
        
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_assignments, null);
            holder = new ViewHolder();
            holder.textName = (TextView) convertView.findViewById(R.id.text_assignment_name);
            holder.textId = (TextView) convertView.findViewById(R.id.text_assignment_id);
            holder.textGivenDate = (TextView) convertView.findViewById(R.id.text_assignment_given_date);
            holder.textSubmitDate = (TextView) convertView.findViewById(R.id.text_assignment_submit_date);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
 
        holder.textName.setText(mAssignmentMasterList.get(position).getAssignmentName());
        holder.textId.setText(mAssignmentMasterList.get(position).getId()+"");
        holder.textGivenDate.setText("Given Date: "+mAssignmentMasterList.get(position).getGivenDate());
        holder.textSubmitDate.setText("Submit Date: "+mAssignmentMasterList.get(position).getSubmitDate());
       
        return convertView;
    }
	
	private class ViewHolder {
        TextView textName,textId,textGivenDate,textSubmitDate;
       
               
    }
	

}
