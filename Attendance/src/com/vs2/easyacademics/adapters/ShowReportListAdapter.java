package com.vs2.easyacademics.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.ShowReport;
import com.vs2.easyacademics.objects.SubjectMaster;

public class ShowReportListAdapter extends BaseAdapter {
	@SuppressWarnings("unused")
	private Context context;
	 private ArrayList<ShowReport> mShowReportList;
	 private LayoutInflater mInflater;
	
	public ShowReportListAdapter(Context context,ArrayList<ShowReport>  entries) {
		
		// TODO Auto-generated constructor stub
		this.context=context;
		this.mShowReportList=entries;
		this.mInflater=LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mShowReportList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mShowReportList.get(position);
	}
	
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
       final ViewHolder holder;
        
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.report_view_diff_list_items, null);
            holder = new ViewHolder();
                      
            holder.textLecture=(TextView)convertView.findViewById(R.id.text_lecture_no);
            holder.textBunked=(TextView)convertView.findViewById(R.id.text_bunked_no);
            holder.textJoint=(TextView)convertView.findViewById(R.id.text_join_no);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
       
        	 holder.textLecture.setText("Lecture No."+mShowReportList.get(position).getLectureNo()+"   P-"+mShowReportList.get(position).getTotalPresent()+"   A-"+mShowReportList.get(position).getTotalAbsent());
             holder.textBunked.setText("Bunked No.: "+mShowReportList.get(position).getBunked());
             holder.textJoint.setText("Newly Joined No.: "+mShowReportList.get(position).getJoined()+"");
      
       
       
        return convertView;
    }
	
	private class ViewHolder {
        TextView textLecture,textBunked,textJoint;
       
               
    }
	

}
