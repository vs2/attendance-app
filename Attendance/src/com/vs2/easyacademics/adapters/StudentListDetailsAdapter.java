package com.vs2.easyacademics.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.StudentMaster;

public class StudentListDetailsAdapter extends BaseAdapter {
	@SuppressWarnings("unused")
	private Context context;
	 private ArrayList<StudentMaster> mStudentMasterList;
	 private LayoutInflater mInflater;
	
	public StudentListDetailsAdapter(Context context,ArrayList<StudentMaster>  entries) {
		
		// TODO Auto-generated constructor stub
		this.context=context;
		this.mStudentMasterList=entries;
		this.mInflater=LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mStudentMasterList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mStudentMasterList.get(position);
	}
	
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
       final ViewHolder holder;
        
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.student_details_list_item, null);
            holder = new ViewHolder();
            holder.textStudentId = (TextView) convertView.findViewById(R.id.text_sid);
            holder.textStudentName = (TextView) convertView.findViewById(R.id.text_student_name);
            holder.textenrolmentno = (TextView) convertView.findViewById(R.id.text_enrolment_no);
            holder.textStudentEmail = (TextView) convertView.findViewById(R.id.text_studentemail);
            holder.textStudentContactNo = (TextView) convertView.findViewById(R.id.text_studentcontactno);
            holder.textStudentAddress = (TextView) convertView.findViewById(R.id.text_studentaddress);
            holder.imageViewCall = (ImageView) convertView.findViewById(R.id.button_call);
            holder.imageViewEmail = (ImageView) convertView.findViewById(R.id.button_email);
            convertView.setTag(holder);
        } else
        	
        holder = (ViewHolder) convertView.getTag();
        holder.textStudentId.setText(mStudentMasterList.get(position).getStudid()+"");
        holder.textStudentName.setText(mStudentMasterList.get(position).getStudentname());
        holder.textenrolmentno.setText(mStudentMasterList.get(position).getRollno()+"");
        holder.textStudentEmail.setText(mStudentMasterList.get(position).getEmail());
              
        if(mStudentMasterList.get(position).getEmail().equals("")){
        	holder.imageViewEmail.setVisibility(View.GONE);
        	holder.textStudentEmail.setVisibility(View.GONE);
        }else{
        	holder.imageViewEmail.setVisibility(View.VISIBLE);
        	holder.textStudentEmail.setVisibility(View.VISIBLE);
        }
        
        holder.textStudentContactNo.setText(mStudentMasterList.get(position).getContactno());
        if(mStudentMasterList.get(position).getContactno().equals("")){
        	holder.imageViewCall.setVisibility(View.GONE);
        	holder.textStudentContactNo.setVisibility(View.GONE);
        }else{
        	holder.imageViewCall.setVisibility(View.VISIBLE);
        	holder.textStudentContactNo.setVisibility(View.VISIBLE);
        }
        
        holder.textStudentAddress.setText(mStudentMasterList.get(position).getAddress());
        if(mStudentMasterList.get(position).getContactno().equals("")){
        	holder.textStudentAddress.setVisibility(View.GONE);
        }else{
        	holder.textStudentAddress.setVisibility(View.VISIBLE);
        }
        
        holder.imageViewCall.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse("tel:"+mStudentMasterList.get(position).getContactno()));
				callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
				context.startActivity(callIntent);
			}
		});
        holder.imageViewEmail.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent contactEmailIntent = new Intent(
                        Intent.ACTION_SENDTO, Uri.fromParts("mailto",
                        		mStudentMasterList.get(position).getEmail(), null));
        contactEmailIntent.putExtra(
                        Intent.EXTRA_SUBJECT,"Information");
        context.startActivity(Intent.createChooser(
                        contactEmailIntent," Choose by mail"));
			}
		});
       
        return convertView;
    }
	
	private class ViewHolder {
        TextView textStudentId;
        TextView textStudentName;
        TextView textenrolmentno;
        TextView textStudentEmail;
        TextView textStudentContactNo;
        TextView textStudentAddress;
        ImageView imageViewCall,imageViewEmail;
    }
	

}
