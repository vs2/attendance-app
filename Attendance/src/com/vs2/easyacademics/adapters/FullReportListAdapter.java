package com.vs2.easyacademics.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.FullReport;
import com.vs2.easyacademics.objects.ShowReport;
import com.vs2.easyacademics.objects.SubjectMaster;

public class FullReportListAdapter extends BaseAdapter {
	@SuppressWarnings("unused")
	private Context context;
	 private ArrayList<FullReport> mFullReportList;
	 private LayoutInflater mInflater;
	
	public FullReportListAdapter(Context context,ArrayList<FullReport>  entries) {
		
		// TODO Auto-generated constructor stub
		this.context=context;
		this.mFullReportList=entries;
		this.mInflater=LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mFullReportList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mFullReportList.get(position);
	}
	
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
       final ViewHolder holder;
        
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.full_report_list_item, null);
            holder = new ViewHolder();
                      
            holder.textNoName=(TextView)convertView.findViewById(R.id.text_no_name);
            holder.textDetails=(TextView)convertView.findViewById(R.id.text_details);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
       
        	 holder.textNoName.setText(""+mFullReportList.get(position).getRollNo()+") "+mFullReportList.get(position).getStudentName());
             holder.textDetails.setText(""+mFullReportList.get(position).getAttendLecture()+"/"+mFullReportList.get(position).getTotalLecture());
      
        return convertView;
    }
	
	private class ViewHolder {
        TextView textNoName,textDetails;
       
               
    }
	

}
