package com.vs2.easyacademics.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.SubjectMaster;

public class SubjcetListAdapter extends BaseAdapter {
	@SuppressWarnings("unused")
	private Context context;
	 private ArrayList<SubjectMaster> mSubjectMasterList;
	 private LayoutInflater mInflater;
	
	public SubjcetListAdapter(Context context,ArrayList<SubjectMaster>  entries) {
		
		// TODO Auto-generated constructor stub
		this.context=context;
		this.mSubjectMasterList=entries;
		this.mInflater=LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mSubjectMasterList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mSubjectMasterList.get(position);
	}
	
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
       final ViewHolder holder;
        
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.class_list_item, null);
            holder = new ViewHolder();
            holder.textClassName = (TextView) convertView.findViewById(R.id.text_classname);
            holder.textClassid = (TextView) convertView.findViewById(R.id.text_classid);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
 
        holder.textClassName.setText(mSubjectMasterList.get(position).getSubjectname());
        holder.textClassid.setText(mSubjectMasterList.get(position).getSubid()+"");
       
        return convertView;
    }
	
	private class ViewHolder {
        TextView textClassName;
        TextView textClassid;
       
               
    }
	

}
