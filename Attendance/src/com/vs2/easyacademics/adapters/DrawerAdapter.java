package com.vs2.easyacademics.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.DrawerMenu;


/**
 * Represents DrawerAdapter
 */
public class DrawerAdapter extends BaseAdapter {

	@SuppressWarnings("unused")
	private Context context;
	private ArrayList<DrawerMenu> mMenus;
	private LayoutInflater mInflater;

	 /**
	   * Creates a new DrawerAdapter with DrawerMenu.
	  
	   */
	public DrawerAdapter(Context context, ArrayList<DrawerMenu> menus) {
		super();
		this.context = context;
		this.mMenus = menus;
		this.mInflater = LayoutInflater.from(context);
	}

	/**
	 * Get Size of ArrayList<DrawerMenu>
	 * @return size
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mMenus.size();
	}

	/**
	 * get items of position
	 * @return Object
	 */
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mMenus.get(position);
	}
	/**
	 * get items Id of position
	 * @return position
	 */
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	/**
	 * Get View
	 * @param position,convertView,parent
	 * @return convertView
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.menu_list_item, null);
			holder = new ViewHolder();
			holder.textViewItem = (TextView) convertView
					.findViewById(R.id.text_title);
			holder.iconItem = (ImageView) convertView
					.findViewById(R.id.image_icon);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.textViewItem.setText(""+mMenus.get(position).getTitle());
		holder.iconItem.setBackgroundResource(mMenus.get(position).getIconResource());
		return convertView;
	}
	/**
	 * Declare TextView and ImageView
	 */
	public class ViewHolder {
		TextView textViewItem;
		ImageView iconItem;
	}

}
