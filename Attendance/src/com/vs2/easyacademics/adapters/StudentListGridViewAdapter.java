package com.vs2.easyacademics.adapters;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vs2.easyacademics.R;
import com.vs2.easyacademics.fragments.FragmentTakeAttendance;
import com.vs2.easyacademics.objects.StudentMaster;


@SuppressLint("ResourceAsColor")
public class StudentListGridViewAdapter extends BaseAdapter {

	private ArrayList<StudentMaster> mStudentMasterList;
	private Context context;
	private LayoutInflater mInflater;

	public StudentListGridViewAdapter(Context context,ArrayList<StudentMaster> StudentMasterList) {
		super();
		this.mStudentMasterList = StudentMasterList;
		this.context = context;
		this.mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mStudentMasterList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mStudentMasterList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.stude_list_for_attendance, null);
			holder = new ViewHolder();
			holder.textId = (TextView) convertView
					.findViewById(R.id.text_identifier);
			holder.textRollNo = (TextView) convertView
					.findViewById(R.id.text_no);
			holder.textName = (TextView) convertView
					.findViewById(R.id.text_name);
			holder.rlayout = (RelativeLayout) convertView
					.findViewById(R.id.rlayout);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		try {
			holder.textId.setText(mStudentMasterList.get(position).getStudid()+"");
			holder.textRollNo.setText(mStudentMasterList.get(position).getRollno()+"");
			holder.textName.setText(mStudentMasterList.get(position).getStudentname());
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		if (mStudentMasterList.get(position).getSelection().equals("false")) {
            holder.rlayout.setBackgroundResource(android.R.color.transparent);
        } else {
            holder.rlayout.setBackgroundResource(FragmentTakeAttendance.color);
        }
		return convertView;
	}

	public class ViewHolder {
		TextView textRollNo,textName,textId;
		RelativeLayout rlayout;
	}

}
