package com.vs2.easyacademics;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.vs2.easyacademics.R;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.JSONParser;
import com.vs2.easyacademics.objects.ToastMsg;
import com.vs2.easyacademics.objects.Utility;

public class ForgetPasswordActivity extends Activity {

	private EditText mEditTextUsername,mEditTextEmail;
	private Button mButtonGetPass;
	private TextView mTextViewYourPass;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forget_password);
		initGloble();
	}
public void initGloble(){
	mEditTextEmail=(EditText)findViewById(R.id.edittext_forget_email);
	mEditTextUsername=(EditText)findViewById(R.id.edittext_forget_username);
	mButtonGetPass=(Button)findViewById(R.id.button_getpass);
	mTextViewYourPass=(TextView)findViewById(R.id.textview_yourpass);
	mButtonGetPass.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(notBlank(mEditTextUsername) && notBlank(mEditTextEmail)){
				mTextViewYourPass.setVisibility(View.GONE);
				
				if (Utility.isOnline(ForgetPasswordActivity.this)) {
					
					if(validEmail(mEditTextEmail.getText().toString())){
						new GetingPasswordApi().execute(mEditTextUsername.getText().toString(),mEditTextEmail.getText().toString());
					}else{
						ToastMsg.showError(ForgetPasswordActivity.this,"Invalid Email", ToastMsg.GRAVITY_CENTER);
					}
					
					
				} else {
					Log.e( "No connection", "No internet connection found!");
					ToastMsg.showError(ForgetPasswordActivity.this,"No internet connection found!", ToastMsg.GRAVITY_CENTER);
				}
					
			}
		}
	});
}
public boolean validEmail(String emailAddress)
{
        
        if( Build.VERSION.SDK_INT >= 8 )
          {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches();
          }

          Pattern pattern;
          Matcher matcher;
          String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)(\\.[A-Za-z]{2,})$";
          pattern = Pattern.compile(EMAIL_PATTERN);
          matcher = pattern.matcher(emailAddress);

          return matcher.matches();
}
public class GetingPasswordApi extends AsyncTask<String,Integer,JSONObject> {

	ProgressDialog pd;
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		
		pd = new ProgressDialog(ForgetPasswordActivity.this);
		pd.setMessage("Please wait....");
		pd.setIndeterminate(false);
		pd.setCancelable(true);
		pd.show();

	}

	@Override
	protected JSONObject doInBackground(String... params) {
		// TODO Auto-generated method stub
		JSONParser parser = new JSONParser();

		List<NameValuePair> postParams = new ArrayList<NameValuePair>();
		postParams.add(new BasicNameValuePair("UserName", params[0]));
		postParams.add(new BasicNameValuePair("Email", params[1]));
		try {
			
			return parser.getJSONFromUrl(Global.SERVER_PATH
					+ "get_password.php",postParams);

		} catch (Exception e) {
			// TODO: handle exception
			Log.e("GetingPasswordApi", "Error : " + e.toString());
			return null;
		}
	}
	
	@Override
	protected void onPostExecute(JSONObject result) {
		// TODO Auto-generated method stub
		try {
			pd.dismiss();
			Log.e("",result+"");
			if (result != null) {
				processGeting(result);
			} else {
				Log.e("GetingPasswordApi", "Server encontered problem try again later");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.onPostExecute(result);
	}

}

private void processGeting(JSONObject result){
	if(result.has("Status") && result.has("Message")){
		try {
			String Message=result.getString("Message");
			if(result.getInt("Status") == 1){
				mTextViewYourPass.setVisibility(View.VISIBLE);
				mTextViewYourPass.setText("Your Password is: "+result.getJSONObject("Data").getString("Password"));					
				ToastMsg.showSuccess(ForgetPasswordActivity.this,
						"Your Password is: "+result.getJSONObject("Data").getString("Password"), ToastMsg.GRAVITY_CENTER);
			}else{
				ToastMsg.showError(ForgetPasswordActivity.this,Message, ToastMsg.GRAVITY_CENTER);
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e("GetingPasswordApi","GetingPasswordApi error");
		}
	}else{
		ToastMsg.showError(ForgetPasswordActivity.this, "Unable to Geting Password", ToastMsg.GRAVITY_CENTER);
	}
}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.forget_password, menu);
		return true;
	}
	private boolean notBlank(EditText edit){
		if(edit.getText().length() > 0){
			edit.setError(null);
			return true;
		}else{
			edit.setError("Required " + edit.getHint());
			return false;
		}
	}
}
