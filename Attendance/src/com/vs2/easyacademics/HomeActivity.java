package com.vs2.easyacademics;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.vs2.easyacademics.R;
import com.vs2.easyacademics.adapters.DrawerAdapter;
import com.vs2.easyacademics.fragments.FragmentAdmin;
import com.vs2.easyacademics.fragments.FragmentAllReports;
import com.vs2.easyacademics.fragments.FragmentClassList;
import com.vs2.easyacademics.fragments.FragmentClassProfile;
import com.vs2.easyacademics.fragments.FragmentSetting;
import com.vs2.easyacademics.objects.DrawerMenu;
import com.vs2.easyacademics.objects.Global;
import com.vs2.easyacademics.objects.PreferenceUser;
import com.vs2.easyacademics.objects.ToastMsg;
import com.vs2.easyacademics.objects.Utility;

public class HomeActivity extends SherlockFragmentActivity {

	private ActionBar mActionBar;
	public static DrawerLayout mDrawerLayout;
	private ListView mListView;
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerAdapter mAdapter;
	private ArrayList<DrawerMenu> mMenus;
	private int selectedMenu = 0;

	public static RelativeLayout mRLayoutAdd;
	private AdView mAdView;
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		Log.e("Home","Home");
		initGlobals(savedInstanceState);
		//removeAddView();
		setupAd();
	}
	public void removeAddView(){
		WindowManager wm = (WindowManager) HomeActivity.this.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		int width = display.getWidth();  // deprecated
		int height = display.getHeight();
		
		if(height<500){
			HomeActivity.mRLayoutAdd.setVisibility(View.VISIBLE);
		}else{
			LayoutParams params = HomeActivity.mDrawerLayout.getLayoutParams();
			((MarginLayoutParams) params).setMargins(0, 0, 0, 0); //left, top, right, bottom
			HomeActivity.mDrawerLayout.setLayoutParams(params);
		}
		Log.e("display","width: "+width+" height: "+height);
		
	}
	private void setupAd() {
		mRLayoutAdd = (RelativeLayout) findViewById(R.id.layout_ad);
		mAdView = new AdView(this);
		mAdView.setAdSize(AdSize.BANNER);
		mAdView.setAdUnitId(getString(R.string.admob_string));
		mAdView.setAdListener(new AdListener() {
			@Override
			public void onAdClosed() {
				// TODO Auto-generated method stub
				Log.e("Main Activity", "ad closed");
				super.onAdClosed();
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				// TODO Auto-generated method stub
				Log.e("Main Activity", "ad onAdFailedToLoad");
				super.onAdFailedToLoad(errorCode);
			}

			@Override
			public void onAdLeftApplication() {
				// TODO Auto-generated method stub
				Log.e("Main Activity", "ad onAdLeftApplication");
				super.onAdLeftApplication();
			}

			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				Log.e("Main Activity", "ad onAdLoaded");
				super.onAdLoaded();
			}

			@Override
			public void onAdOpened() {
				// TODO Auto-generated method stub
				Log.e("Main Activity", "ad onAdOpened");
				super.onAdOpened();
			}
		});
		mRLayoutAdd.addView(mAdView);
		AdRequest adRequest = new AdRequest.Builder().build();
		mAdView.loadAd(adRequest);
	}

	
	private void initGlobals(Bundle savedInstanceState) {
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setHomeButtonEnabled(true);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		mListView = (ListView) findViewById(R.id.left_drawer);
		setMenus();

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.bars, R.string.open_menu, R.string.close_menu) {
			@Override
			public void onDrawerOpened(View drawerView) {
				// TODO Auto-generated method stub
				supportInvalidateOptionsMenu();
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				// TODO Auto-generated method stub
				supportInvalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerToggle.setDrawerIndicatorEnabled(true);
		if (savedInstanceState != null) {
			selectedMenu = savedInstanceState.getInt("selectedMenu");
			loadFragment(selectedMenu, true, false);
			mAdapter.notifyDataSetChanged();
		} else {
			profileFragment();

		}

	}

	private void profileFragment() {
		
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		if(Global.sUserMaster.getUsertypeid()==1)
		{
			//admin
			fragmentTransaction.replace(R.id.frame_layout, new FragmentAdmin()).commit();
			
		}
		else
		{
			//teacher
			//fragmentTransaction.replace(R.id.frame_layout, new FragmentClassProfile()).commit();
			FragmentClassProfile fragmentClassProfile=new FragmentClassProfile();
			Bundle bundle=new Bundle();
			bundle.putString("Action", "Default");
			fragmentClassProfile.setArguments(bundle);
    		fragmentTransaction.replace(R.id.frame_layout, fragmentClassProfile).commit();
    		
		}
		
		selectedMenu = 0;
		mAdapter.notifyDataSetChanged();
	}

	private void toggleDrawer() {
		if (!mDrawerLayout.isDrawerOpen(mListView)) {
			mDrawerLayout.openDrawer(mListView);
		} else {
			mDrawerLayout.closeDrawer(mListView);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		try {
			outState.putInt("selectedMenu", selectedMenu);
		} catch (Exception e) {
			// TODO: handle exception
		}
		super.onSaveInstanceState(outState);
	}

	private void loadFragment(int position, boolean reload, boolean toggle) {

		DrawerMenu menu = (DrawerMenu) mAdapter.getItem(position);
		if (toggle) {
			toggleDrawer();
		}

		FragmentManager fragmentManager = getSupportFragmentManager();
		final FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		Bundle bundle = new Bundle();
		FragmentClassList fragmentClassList = new FragmentClassList();
		switch (menu.getType()) {
		case DrawerMenu.HOME:
			
			if (Utility.isOnline(HomeActivity.this)) {
				removeAddView();
				profileFragment();
				
				break;
			} else {
				Log.e("No connection", "No internet connection found!");
				ToastMsg.showError(HomeActivity.this,
						"No internet connection found!",
						ToastMsg.GRAVITY_CENTER);
				break;
			}

		case DrawerMenu.TAKE_ATTENDANCE:
			if (Utility.isOnline(HomeActivity.this)) {
				removeAddView();
				bundle.putString("Action", "Take Attendance");
				fragmentClassList.setArguments(bundle);				
				fragmentTransaction
						.replace(R.id.frame_layout, fragmentClassList);
				fragmentTransaction
						.addToBackStack(null);
				fragmentTransaction.commit();
				break;
			} else {
				Log.e("No connection", "No internet connection found!");
				ToastMsg.showError(HomeActivity.this,
						"No internet connection found!",
						ToastMsg.GRAVITY_CENTER);
				break;
			}

		case DrawerMenu.ASSIGNMENTS:

			if (Utility.isOnline(HomeActivity.this)) {
				removeAddView();
				bundle.putString("Action", "Assignments");
				fragmentClassList.setArguments(bundle);
				fragmentTransaction
						.replace(R.id.frame_layout, fragmentClassList)
						.addToBackStack(null).commit();
				break;
			} else {
				Log.e("No connection", "No internet connection found!");
				ToastMsg.showError(HomeActivity.this,
						"No internet connection found!",
						ToastMsg.GRAVITY_CENTER);
				break;
			}
		case DrawerMenu.STUDENT_REPORTS:
			if (Utility.isOnline(HomeActivity.this)) {
				removeAddView();
				bundle.putString("Action", "Student Report");
				fragmentClassList.setArguments(bundle);
				fragmentTransaction
						.replace(R.id.frame_layout, fragmentClassList)
						.addToBackStack(null).commit();
				break;
			} else {
				Log.e("No connection", "No internet connection found!");
				ToastMsg.showError(HomeActivity.this,
						"No internet connection found!",
						ToastMsg.GRAVITY_CENTER);
				break;
			}

		case DrawerMenu.VIEW_REPORTS:

			if (Utility.isOnline(HomeActivity.this)) {
				removeAddView();
				/*FragmentAllReports fragmentAllReports = new FragmentAllReports();
				bundle.putString("Action", "Reports");
				fragmentClassList.setArguments(bundle);
				fragmentTransaction
						.replace(R.id.frame_layout, fragmentAllReports)
						.addToBackStack(null).commit();*/
				bundle.putString("Action", "Attendance Report");
				fragmentClassList.setArguments(bundle);
				fragmentTransaction
						.replace(R.id.frame_layout, fragmentClassList)
						.addToBackStack(null).commit();
				break;
			} else {
				Log.e("No connection", "No internet connection found!");
				ToastMsg.showError(HomeActivity.this,
						"No internet connection found!",
						ToastMsg.GRAVITY_CENTER);
				break;
			}

		case DrawerMenu.SETTINGS:
			if (Utility.isOnline(HomeActivity.this)) {
				removeAddView();
				FragmentSetting fragmentSetting = new FragmentSetting();
				bundle.putString("Action", "Change Password");
				bundle.putString("Back", "Home");
				fragmentSetting.setArguments(bundle);
				fragmentTransaction
						.replace(R.id.frame_layout, fragmentSetting)
						.addToBackStack(null).commit();
				
				break;
			} else {
				Log.e("No connection", "No internet connection found!");
				ToastMsg.showError(HomeActivity.this,
						"No internet connection found!",
						ToastMsg.GRAVITY_CENTER);
				break;
			}

		case DrawerMenu.LOGOUT:
			PreferenceUser.clearPreference(HomeActivity.this);
			startActivity(new Intent(getApplicationContext(),
					MainActivity.class));
			finish();
			break;

		}

	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (mDrawerToggle != null) {
			mDrawerToggle.syncState();
		}
		super.onPostCreate(savedInstanceState);
	}

	private void setMenus() {

		mMenus = new ArrayList<DrawerMenu>();
		mMenus.add(new DrawerMenu(DrawerMenu.HOME, "Home", R.drawable.home));
		mMenus.add(new DrawerMenu(DrawerMenu.TAKE_ATTENDANCE,
				"Take Attendance", R.drawable.take_attendance));
		mMenus.add(new DrawerMenu(DrawerMenu.ASSIGNMENTS, "Assignments",
				R.drawable.assignments));
		mMenus.add(new DrawerMenu(DrawerMenu.STUDENT_REPORTS,
				"Students Reports", R.drawable.student_reports));
		mMenus.add(new DrawerMenu(DrawerMenu.VIEW_REPORTS, "Attendance Reports",
				R.drawable.attendance_reports));
		mMenus.add(new DrawerMenu(DrawerMenu.SETTINGS, "Settings",
				R.drawable.settings));
		mMenus.add(new DrawerMenu(DrawerMenu.LOGOUT, "Log out",
				R.drawable.log_out));
		mAdapter = new DrawerAdapter(HomeActivity.this, mMenus);
		mListView.setAdapter(mAdapter);

		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// Log.e("Drawel click", "Drawel click"+" "+position);
				loadFragment(position, false, true);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater menuInflater = this.getSupportMenuInflater();
		menuInflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == 16908332) {
			toggleDrawer();
		}
		return super.onOptionsItemSelected(item);
	}
}
